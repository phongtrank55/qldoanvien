﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Common.ConstraintConfig
{
    public class MonthlyReportExpireDateConfiguration
    {
        public const string PLUGIN_NAME = "ExpiredMonthlyReport";
        public const char SEPARATE_CHARACTER = '|';
        public class Key
        {
            public const string DUE_DATE = "DueDate";
            public const string ENABLED_CUT_OFF_DATE = "EnabledCutOffDate";
            public const string CUT_OFF_DATE = "CutOffDate"; // số ngày sau ngày hết hạn
            public const string DEDUCATE_POINT = "DeducatePoint";
            public const string ENABLED_DEDUCATE_EVERY_EXPIRE_DAY = "EnabledDecuteEveryExpireDay";
        }

        public class Value
        {
            public const string DUE_DATE_DAY_END_MONTH = "END"; //NGÀY CUỐI THÁNG; > 28
            public const string DUE_DATE_MONTH_THIS = "THIS"; //Tháng này
            public const string DUE_DATE_MONTH_NEXT = "NEXT"; //tháng sau
           
        }

    }
}
