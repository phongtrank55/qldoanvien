﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Common
{
    //Phân biệt loại đơn vị
    public class ConstraintUnitType
    {
        public const int FACULTY = 1;
        public const int UNIVERSITY = 0;
    }
}
