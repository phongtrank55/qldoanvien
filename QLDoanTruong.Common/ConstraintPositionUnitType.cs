﻿
namespace QLDoanTruong.Common
{
    public class ConstraintPositionUnitType
    {
        public const int UNIVERSITY = 0;
        public const int FACULTY = 1;
        public const int CLASS = 2;
    }
}
