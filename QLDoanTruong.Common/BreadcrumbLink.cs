﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Common
{
    public class BreadcrumbLink
    {
        public string Link { get; set; }
        public string Title { get; set; }
        
    }
}
