﻿
namespace QLDoanTruong.Common
{
    //Phân biệt các loại đoàn viên
    public class ConstraintUnionMemberType
    {
        public const int STUDENT = 0;
        public const int LECTURE = 1;
        public const int STAFF = 2;

    }
}
