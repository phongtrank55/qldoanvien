﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Common
{
    public class ConstraintName
    {
        public const string HOST = "";
        public const string AVATAR_DEFAULT = "default.PNG";

        public const string MENU_DIRECTORY_PATH = "~/Uploads/Menus/";
        public const string AVATAR_DIRECTORY_PATH = "~/Uploads/Avatars/";
        public const string DOCUMENT_ACTIVITY_DIRECTORY_PATH = "~/Uploads/DocumentActivities/";
        public const string PROOF_FILE_DIRECTORY_PATH = "~/Uploads/Proofs/";
        public const string ACHIEVEMENT_FILE_DIRECTORY_PATH = "~/Uploads/Achievements/";
        public const string NEW_THUMB_DIRECTORY_PATH = "~/Uploads/NewThumbs/";
        public const string IMPORT_DIRECTORY_PATH = "~/Uploads/Imports/";
        public const int ACHIEVEMENT_FILE = 3;
        public const int DOCUMENT_FILE = 1;
        public const int PROOF_FILE = 2;
        public const int NEW_ATTACHMENT = 4;

        public const int LAST_DAY_TO_SEND_REPORT = 28;
    }
}
