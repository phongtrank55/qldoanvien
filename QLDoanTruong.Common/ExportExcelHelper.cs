﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
//using NPOI.XWPF.UserModel;

namespace QLDoanTruong.Common
{
    public class ExportExcelHelper
    {

        public static void SetFontCell(ref ICell cell, XSSFWorkbook workbook, int fontHeight = 10, bool isBold=false)
        {
            var defaultFont = workbook.CreateFont();
            defaultFont.FontHeight = fontHeight;
            defaultFont.FontName = "Arial";
            defaultFont.IsBold = isBold;
            defaultFont.Color = IndexedColors.Black.Index;
            cell.CellStyle.SetFont(defaultFont);
        }
        public static void SetStyleCellContentTable(ref ICell cell, XSSFWorkbook workbook, HorizontalAlignment horizontalAlignment = HorizontalAlignment.Left)
        {
            var style = workbook.CreateCellStyle();
            style.BorderBottom = BorderStyle.Thin;
            style.BorderTop = BorderStyle.Thin;
            style.BorderLeft = BorderStyle.Thin;
            style.BorderRight = BorderStyle.Thin;
            style.Alignment = horizontalAlignment;
            //font
            var defaultFont = workbook.CreateFont();
            defaultFont.FontHeight = 10;
            defaultFont.FontName = "Arial";
            defaultFont.Color = IndexedColors.Black.Index;
            style.SetFont(defaultFont);

            cell.CellStyle = style;

        }
        public static void SetStyleCellHeaderTable(ref ICell cell, XSSFWorkbook workbook)
        {
            var style = workbook.CreateCellStyle();
            style.BorderBottom = BorderStyle.Thin;
            style.BorderTop = BorderStyle.Thin;
            style.BorderLeft = BorderStyle.Thin;
            style.BorderRight = BorderStyle.Thin;
            style.Alignment = HorizontalAlignment.Center;
            //font
            //font
            var defaultFont = workbook.CreateFont();
            defaultFont.FontHeight = 10;
            defaultFont.FontName = "Arial";
            defaultFont.IsBold = true;
            defaultFont.Color = IndexedColors.Black.Index;
            style.SetFont(defaultFont);

            cell.CellStyle = style;

        }
    }
}
