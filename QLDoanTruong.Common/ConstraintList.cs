﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Common
{
    public class Item
    {
        public int ID { get; set; }
        public string Name { get; set; }

    }
    public class CommonList
    {
        /// <summary>
        /// QL Đoàn viên
        /// </summary>
        public static List<Item> ListMemberType = new List<Item>(){
            new Item(){ID=ConstraintUnionMemberType.STUDENT, Name = "Sinh viên"},
            new Item(){ID=ConstraintUnionMemberType.LECTURE, Name = "Giảng viên"},
            new Item(){ID=ConstraintUnionMemberType.STAFF, Name = "Cán bộ Đoàn trường"}
        };

        /// <summary>
        /// MonthlyReport
        /// </summary>
        public static List<Item> Months = new List<Item>()
        {
            new Item(){ID = 1 , Name="Tháng 1"},
            new Item(){ID = 2 , Name="Tháng 2"},
            new Item(){ID = 3 , Name="Tháng 3"},
            new Item(){ID = 4 , Name="Tháng 4"},
            new Item(){ID = 5 , Name="Tháng 5"},
            new Item(){ID = 6 , Name="Tháng 6"},
            new Item(){ID = 7 , Name="Tháng 7"},
            new Item(){ID = 8 , Name="Tháng 8"},
            new Item(){ID = 9 , Name="Tháng 9"},
            new Item(){ID = 10 , Name="Tháng 10"},
            new Item(){ID = 11 , Name="Tháng 11"},
            new Item(){ID = 12 , Name="Tháng 12"}
        };

        public static List<Item> GetYears(bool isBirthDate=false)
        {
            var res = new List<Item>();
            int i = isBirthDate ? 1960 : 2018;
            for (; i <= DateTime.Now.Year+2; i++)
                res.Add(new Item() { ID = i, Name = i.ToString() });
            return res;

        }

        public static List<Item> Days = new List<Item>()
        {
            new Item(){ID = 1 , Name="1"},
            new Item(){ID = 2 , Name="2"},
            new Item(){ID = 3 , Name="3"},
            new Item(){ID = 4 , Name="4"},
            new Item(){ID = 5 , Name="5"},
            new Item(){ID = 6 , Name="6"},
            new Item(){ID = 7 , Name="7"},
            new Item(){ID = 8 , Name="8"},
            new Item(){ID = 9 , Name="9"},
            new Item(){ID = 10 , Name="10"},
            new Item(){ID = 11 , Name="11"},
            new Item(){ID = 12 , Name="12"},
            new Item(){ID = 13 , Name="13"},
            new Item(){ID = 14 , Name="14"},
            new Item(){ID = 15 , Name="15"},
            new Item(){ID = 16 , Name="16"},
            new Item(){ID = 17 , Name="17"},
            new Item(){ID = 18 , Name="18"},
            new Item(){ID = 19 , Name="19"},
            new Item(){ID = 20 , Name="20"},
            new Item(){ID = 21 , Name="21"},
            new Item(){ID = 22 , Name="22"},
            new Item(){ID = 23 , Name="23"},
            new Item(){ID = 24 , Name="24"},
            new Item(){ID = 25 , Name="25"},
            new Item(){ID = 26 , Name="26"},
            new Item(){ID = 27 , Name="27"},
            new Item(){ID = 28 , Name="28"},
            new Item(){ID = 29 , Name="29"},
            new Item(){ID = 30 , Name="30"},
            new Item(){ID = 31 , Name="31"}
        };

        //khen thưởng
        public static List<Item> ListUnionType = new List<Item>()
        {
            new Item(){ID=ConstraintUnionType.FACULTY, Name="Liên chi đoàn"},
            new Item(){ID=ConstraintUnionType.CLASS, Name="Chi đoàn"},
            new Item(){ID=ConstraintUnionType.MEMBER, Name="Đoàn viên"},
        };
        //tùy chọn khi import
        public static List<Item> ListImportOption = new List<Item>()
        {
            new Item(){ID=ConstrainImportOptionType.OVERRIDE, Name="Ghi đè thông tin đoàn viên đã có"},
            new Item(){ID=ConstrainImportOptionType.SKIP_EXISTS, Name="Bỏ qua đoàn viên đã có"}
        };
        //ql chi doan
        public static List<Item> ListUnionClassType = new List<Item>()
        {
            new Item(){ID=ConstraintUnionClassType.STUDENT, Name="Sinh viên"},
            new Item(){ID=ConstraintUnionClassType.LECTURE, Name="Cán bộ"},
          
        };
    
    }
}
