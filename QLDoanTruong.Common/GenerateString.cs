﻿using System;
using System.Text;

namespace QLDoanTruong.Common
{
    public class GenerateString
    {
        private readonly static Random rand = new Random();
        private static char[] set = new char[]
        {
            'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k',
            'l', 'x', 'c', 'v', 'b', 'n', 'm', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 'A', 'S', 'D', 'F',
            'G', 'H', 'J', 'K', 'Q', 'W', 'E', 'R', 'T', 'Y', 'I', 'U', 'O', 'P',
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 
            
        };

        private static int lengthSet = set.Length;
        public static string GenerateToken(int length=10)
        {

            StringBuilder result = new StringBuilder(); 
            for (int i = 0; i < length; i++)
            {
                result.Append(set[rand.Next(0, lengthSet)]);
            }
         
            return result.ToString();
        }
    }
}
