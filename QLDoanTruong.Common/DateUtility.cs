﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QLDoanTruong.Common
{
    public class DateRange
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }
    public class DateUtility
    {
        /// <summary>
        /// dd/MM/yyyy - dd/MM/yyyy
        /// </summary>
        public static DateRange GetDateRange(string dateRangeString)
        {
            //Xoa dau cach
            dateRangeString = Regex.Replace(dateRangeString, @"\s+", "");
            
            var range = dateRangeString.Split('-');
            return new DateRange() {
                StartDate=DateTime.ParseExact(range[0], "dd/MM/yyyy", null),
                EndDate = DateTime.ParseExact(range[1], "dd/MM/yyyy", null),
            };
        }

        public static DateTime GetDateFromVietNamese(string date){
            
            return DateTime.ParseExact(date, "dd/MM/yyyy", null);

        }

        public static string GetDayOfWeekVietnamese(DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Monday: return "Thứ 2"; 
                case DayOfWeek.Tuesday: return "Thứ 3";
                case DayOfWeek.Wednesday: return "Thứ 4";
                case DayOfWeek.Thursday: return "Thứ 5";
                case DayOfWeek.Friday: return "Thứ 6";
                case DayOfWeek.Saturday: return "Thứ 7";
                default: return "Chủ nhật";
            }
        }
             
    }
}
