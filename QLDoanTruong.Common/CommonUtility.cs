﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Common
{
    public class CommonUtility
    {
        public static Byte[] PdfSharpConvert(String html)
        {
            Byte[] res = null;
            using (MemoryStream ms = new MemoryStream())
            {
                var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html, PdfSharp.PageSize.A4);
                pdf.Save(ms);
                res = ms.ToArray();
            }
            return res;
        }
        public static void SendMail(string toEmailAddress, string subject, string content)
        {
            var fromEmailAddress = "qldoantruong@gmail.com";
            var fromEmailDisplayName = "Quản lý đoàn viên - ĐHVinh";
            var fromEmailPassword = "qldoantruong123";
            var smtpHost = "smtp.gmail.com";
            var smtpPort = 587;

            bool enabledSsl = true;

            string body = content;
            MailMessage message = new MailMessage(new MailAddress(fromEmailAddress, fromEmailDisplayName), new MailAddress(toEmailAddress));
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;

            var client = new SmtpClient();
            client.Credentials = new NetworkCredential(fromEmailAddress, fromEmailPassword);
            client.Host = smtpHost;
            client.EnableSsl = enabledSsl;
            client.Port = smtpPort;
            client.Send(message);
        }
    }
}
