﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using QLDoanTruong.Model;

namespace QLDoanTruong.Service
{
    public class GroupUserService:IService<GroupUser>
    {
        private YouthUnionUniversityDbContext context;

        public GroupUserService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public GroupUser Add(GroupUser entity)
        {
            var groupUser = context.GroupUsers.Add(entity);
            context.SaveChanges();
            return groupUser;
        }

        public void Update(GroupUser entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            GroupUser groupUser = context.GroupUsers.Find(id);
            Delete(groupUser);

        }

        public void Delete(GroupUser entity)
        {
            context.GroupUsers.Remove(entity);
            context.SaveChanges();
        }

        public List<GroupUser> GetAll()
        {
            return context.GroupUsers.ToList();
        }

        public GroupUser GetSingleById(long? id)
        {
            return context.GroupUsers.Find(id);
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
