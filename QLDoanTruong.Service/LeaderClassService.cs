﻿using QLDoanTruong.Model;
using QLDoanTruong.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;


namespace QLDoanTruong.Service
{
    public class LeaderClassService : IService<LeaderClass>
    {
        private YouthUnionUniversityDbContext context;

        public LeaderClassService()
        {
            context = new YouthUnionUniversityDbContext();
        }

        public LeaderClass Add(LeaderClass entity)
        {
            var Leader = context.LeaderClasses.Add(entity);
            context.SaveChanges();
            return Leader;
        }

        public void Update(LeaderClass entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            LeaderClass Leader = context.LeaderClasses.Find(id);
            Delete(Leader);

        }

        public void Delete(LeaderClass entity)
        {
            context.LeaderClasses.Remove(entity);
            context.SaveChanges();
        }

        public List<LeaderClass> GetAll()
        {
            return context.LeaderClasses.ToList();
        }

        public LeaderClass GetSingleById(long? id)
        {
            return context.LeaderClasses.Find(id);
        }

        public List<LeaderClassViewModel> GetViewBySchoolYearAndClass(long schoolYearID, long classID)
        {
            return context.Positions
                        .Where(x => x.UnitType == QLDoanTruong.Common.ConstraintPositionUnitType.CLASS)
                        .Select(x => new LeaderClassViewModel()
                        {
                            PositionName = x.Name,
                            PositionType = x.Type,
                            ID = x.LeaderClasses.Where(m => m.ClassID == classID && m.SchoolYearID == schoolYearID)
                                        .Select(m => m.ID),
                            Member = x.LeaderClasses.Where(m => m.ClassID == classID && m.SchoolYearID == schoolYearID)
                                            .Select(m => m.YouthUnionMember.Lastname + " " + m.YouthUnionMember.Firstname + " - " + m.YouthUnionMember.YouthUnionMemberCode),


                        }).ToList();

        }

        public List<Position> GetPositionForClass(long memberID, long schoolYearID)
        {
            //xEM các chức vụ hiện tại của người này trong lớp

            var currentPosition = context.LeaderClasses
                                .Where(x => x.YouthUnionMemberID == memberID
                                             && x.SchoolYearID == schoolYearID)
                                .Select(x => x.Position);
            //var y = currentPosition.ToList();
            var classID = context.YouthUnionMembers.FirstOrDefault(x=>x.ID==memberID).YouthUnionClassID;
            if (currentPosition.Count() == 0)
            {
                return context.Positions
                            .Where(x => x.UnitType == QLDoanTruong.Common.ConstraintPositionUnitType.CLASS
                                   && x.Amount > context.LeaderClasses.Count(m => m.ClassID == classID
                                                                     && m.SchoolYearID == schoolYearID
                                                                     && m.PositionID == x.ID))
                             .ToList();
            }

            var currentTypes = currentPosition.Select(x => x.Type);
            return context.Positions.Where(x => x.UnitType == QLDoanTruong.Common.ConstraintPositionUnitType.CLASS
                                && !currentTypes.Contains(x.Type)
                                && x.Amount > context.LeaderClasses.Count(m => m.ClassID == classID
                                                                && m.SchoolYearID == schoolYearID
                                                                && m.PositionID == x.ID))
                                .ToList();

        }
        public void Dispose()
        {
            context.Dispose();
        }


    }

}
