﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class AwardLevelService : IService<AwardLevel>
    {
        private YouthUnionUniversityDbContext context;

        public AwardLevelService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public AwardLevel Add(AwardLevel entity)
        {
            var awardType = context.AwardLevels.Add(entity);
            context.SaveChanges();
            return awardType;
        }

        public void Update(AwardLevel entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            AwardLevel awardType = context.AwardLevels.Find(id);
            if (awardType != null)
                Delete(awardType);

        }

        public void Delete(AwardLevel entity)
        {
            context.AwardLevels.Remove(entity);
            context.SaveChanges();
        }

        public List<AwardLevel> GetAll()
        {
            return context.AwardLevels.ToList();
        }

        public AwardLevel GetSingleById(long? id)
        {
            return context.AwardLevels.Find(id);
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }

}
