﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class TempMonthlyReportDetailService : IService<TempMonthlyReportDetail>
    {
        private YouthUnionUniversityDbContext context;

        public TempMonthlyReportDetailService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public TempMonthlyReportDetail Add(TempMonthlyReportDetail entity)
        {

            entity.UpdatedDate = DateTime.Now;
            var tempMonthlyReport = context.TempMonthlyReportDetails.Add(entity);
            context.SaveChanges();
            return tempMonthlyReport;
        }

        public void Update(TempMonthlyReportDetail entity)
        {
            if (entity.FileUploadProof == null)
            {
                var file = context.MonthlyReportDetails.FirstOrDefault(x => x.ActivityID == entity.ActivityID && x.MonthReportID == entity.MonthReportID).FileUploadProof;
                entity.FileUploadProof = file;
            }
            entity.UpdatedDate = DateTime.Now;
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        /// <summary>
        /// Chuyển dữ liệu bên LCĐ sang dữ liệu bên đoàn trường
        /// </summary>
        /// <param search="reportID"></param>
        public void SendReport(long reportID)
        {
            //XoÁ Các báo cáo đã có

            context.MonthlyReportDetails.RemoveRange(context.MonthlyReportDetails.Where(x => x.MonthReportID == reportID));
            var details = context.TempMonthlyReportDetails
                        .Where(x => x.MonthReportID == reportID)
                        .Select(x => new
                        {
                            MonthReportID = x.MonthReportID,
                            ActivityID = x.ActivityID,
                            PointSelf = x.PointSelf,
                            UpdatedDate = x.UpdatedDate,
                            DateActive = x.DateActive,
                            FileUploadProof = x.FileUploadProof,
                            OutsideLinkProof = x.OutsideLinkProof,

                        }).ToList().Select(x => new MonthlyReportDetail()
                        {
                            MonthReportID = x.MonthReportID,
                            ActivityID = x.ActivityID,
                            PointSelf = x.PointSelf,
                            UpdatedDate = x.UpdatedDate,
                            DateActive = x.DateActive,
                            FileUploadProof = x.FileUploadProof,
                            OutsideLinkProof = x.OutsideLinkProof,

                            PointSubmit = 0,
                            Submited = false

                        });

            context.MonthlyReportDetails.AddRange(details);
            var report = context.MonthlyReports.Find(reportID);
            report.Submited = false;
            //report.Rank = null;
            var now = DateTime.Now;
            report.SentDate = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0);

            context.SaveChanges();
        }

        public void Delete(long id)
        {
            TempMonthlyReportDetail tempMonthlyReport = context.TempMonthlyReportDetails.Find(id);
            Delete(tempMonthlyReport);
        }

        public void DeleteActivityInReport(long? activityID, long? reportID)
        {
            var detail = context.TempMonthlyReportDetails
                                .FirstOrDefault(x => x.MonthReportID == reportID && x.ActivityID == activityID);
            context.TempMonthlyReportDetails.Remove(detail);
            context.SaveChanges();
        }
        /// <summary>
        
        /// </summary>
        /// <param search="reportID"></param>
        public void DeleteByReport(long? reportID)
        {
            var report = context.TempMonthlyReportDetails.Where(x => x.MonthlyReport.ID == reportID);
            if (report.Count() > 0)
            {
                context.TempMonthlyReportDetails.RemoveRange(report);
                context.SaveChanges();
            }
        }

        public void Delete(TempMonthlyReportDetail entity)
        {
            context.TempMonthlyReportDetails.Remove(entity);
            context.SaveChanges();
        }

        public List<TempMonthlyReportDetail> GetAll()
        {
            return context.TempMonthlyReportDetails.ToList();
        }

        public List<TempMonthlyReportDetail> GetAllByMonthlyReport(long? reportID)
        {
            if (reportID == null) return null;
            return context.TempMonthlyReportDetails.Where(x => x.MonthReportID == reportID).ToList();
        }

        public TempMonthlyReportDetail GetSingleById(long? id)
        {
            return context.TempMonthlyReportDetails.Find(id);
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }

}
