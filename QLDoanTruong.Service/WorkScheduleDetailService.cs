﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using QLDoanTruong.Model.ViewModel;
using System.Text.RegularExpressions;

namespace QLDoanTruong.Service
{
    public class WorkScheduleDetailService : IService<WorkScheduleDetail>
    {
        private YouthUnionUniversityDbContext context;

        public WorkScheduleDetailService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public WorkScheduleDetail Add(WorkScheduleDetail entity)
        {
            var detailSchedule = context.WorkScheduleDetails.Add(entity);
            context.SaveChanges();
            //Thay đổi ngày cập nhật
            using (var scheduleService = new WorkScheduleService())
            {
                var schedule = scheduleService.GetSingleById(entity.WorkScheduleID);
                schedule.ModifiedDate = DateTime.Now;
                scheduleService.Update(schedule);
            }

            return detailSchedule;
        }
        public WorkScheduleDetail Add(WorkScheduleDetailViewModel detailViewModel)
        {
            //1:bch; 2:ĐV; 3: khác
            if (detailViewModel.ParticipantOther != null)
            {
                detailViewModel.Participants.Add("3::" + detailViewModel.ParticipantOther);
            }
            if (detailViewModel.ChairManOther != null)
            {
                detailViewModel.Chairman.Add("3::" + detailViewModel.ChairManOther);
            }
            var entity = new WorkScheduleDetail()
            {
                IsImportant = detailViewModel.IsImportant,
                Date = detailViewModel.Date,
                Time = detailViewModel.Time,
                Content = detailViewModel.Content,
                Location = detailViewModel.Location,
                WorkScheduleID = detailViewModel.WorkScheduleID,
                ChairMan = string.Join("||", detailViewModel.Chairman),
                Participants = string.Join("||", detailViewModel.Participants),
                DayOfWeek = QLDoanTruong.Common.DateUtility.GetDayOfWeekVietnamese(detailViewModel.Date)
            };

            var detailSchedule = context.WorkScheduleDetails.Add(entity);
            context.SaveChanges();
            //Thay đổi ngày cập nhật
            using (var scheduleService = new WorkScheduleService())
            {
                var schedule = scheduleService.GetSingleById(entity.WorkScheduleID);
                schedule.ModifiedDate = DateTime.Now;
                scheduleService.Update(schedule);
            }

            return detailSchedule;
        }

        public void Update(WorkScheduleDetail entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            WorkScheduleDetail detailSchedule = context.WorkScheduleDetails.Find(id);
            Delete(detailSchedule);

        }

        public void Delete(WorkScheduleDetail entity)
        {
            context.WorkScheduleDetails.Remove(entity);
            context.SaveChanges();
            using (var scheduleService = new WorkScheduleService())
            {
                var schedule = scheduleService.GetSingleById(entity.WorkScheduleID);
                schedule.ModifiedDate = DateTime.Now;
                scheduleService.Update(schedule);
            }
        }

        public List<WorkScheduleDetail> GetAll()
        {
            return context.WorkScheduleDetails.OrderBy(x => new { x.Date, x.Time }).ToList();
        }

        public List<WorkScheduleDetail> GetEveryDay(long? scheduleID)
        {
            return context.WorkScheduleDetails.Where(x => x.WorkScheduleID == scheduleID && x.IsEveryDay).ToList();
        }
        public List<WorkScheduleDetailViewModel> GetAllViewByShedule(long? scheduleID)
        {
            if (scheduleID == null) return null;
            var scheduleDetail = context.WorkScheduleDetails.Where(x => x.WorkScheduleID == scheduleID && !x.IsEveryDay).OrderBy(x => new { x.Date, x.Time }).ToList();
            var result = new List<WorkScheduleDetailViewModel>();
            //Chuyển các thành phần tham dự và chủ trì
            foreach (var detail in scheduleDetail)
            {
                var detailView = new WorkScheduleDetailViewModel()
                {
                    IsDayLong = detail.IsDayLong,
                    IsImportant = detail.IsImportant,
                    Date = detail.Date,
                    Time = detail.Time,
                    ID = detail.ID,
                    DayOfWeek = detail.DayOfWeek,
                    Content = detail.Content,
                    Location = detail.Location,
                    WorkScheduleID = detail.WorkScheduleID,

                };
                if (!detail.IsDayLong)
                {
                    //tách chuỗi
                    //thành phần tham dự
                    var participants = detail.Participants.Split(';');
                    if (participants[0] != string.Empty)
                    {
                        foreach (var part in participants)
                        {
                            var item = Regex.Split(part, @"::");
                            if (item[0] == "1")
                            {
                                var unitID = int.Parse(item[1]);
                                var unitName = context.Units.Where(x => x.ID == unitID).Select(x => x.Name).FirstOrDefault();
                                if(unitName!=null)
                                    detailView.Participants.Add("Ban chấp hành " + unitName);
                                
                            }
                            else if (item[0] == "2")
                            {
                                var memberID = int.Parse(item[1]);
                                var member = context.YouthUnionMembers.FirstOrDefault(x => x.ID == memberID);
                                if(member!=null)
                                    detailView.Participants.Add(member.Lastname + " " + member.Firstname);
                            }
                            else
                            {
                                detailView.ParticipantOther = item[1];
                            }

                        }
                    }
                    //chairman
                    var chairmans = Regex.Split(detail.ChairMan, @"\|\|");
                    if (chairmans[0] != string.Empty)
                    {
                        foreach (var part in chairmans)
                        {
                            var item = Regex.Split(part, @"::");
                            if (item[0] == "1")
                            {
                                var unitID = int.Parse(item[1]);
                                var unitName = context.Units.Where(x => x.ID == unitID).Select(x => x.Name).FirstOrDefault();
                                if (unitName != null)
                                    detailView.Chairman.Add("Ban chấp hành " + unitName);
                            }
                            else if (item[0] == "2")
                            {
                                var memberID = int.Parse(item[1]);
                                var member = context.YouthUnionMembers.FirstOrDefault(x => x.ID == memberID);
                                if (member != null)
                                detailView.Chairman.Add(member.Lastname + " " + member.Firstname);
                            }
                            else
                            {
                                detailView.ChairManOther = item[1];
                            }
                        }
                    }
                }
                result.Add(detailView);
            }
            return result;
        }

        public List<WorkScheduleDisplayViewModel> GetDisplayBySchedule(long? scheduleID)
        {
            if (scheduleID == null) return null;
            var result = new List<WorkScheduleDisplayViewModel>();
            var scheduleDetail = context.WorkScheduleDetails.Where(x => x.WorkScheduleID == scheduleID && !x.IsEveryDay).OrderBy(x => new { x.Date, x.Time });
            var dates = scheduleDetail.GroupBy(x => new { x.DayOfWeek, x.Date }).Select(x => new { Date = x.Key.Date, DayOfWeek = x.Key.DayOfWeek, Count = x.Count() }).OrderBy(x=>x.Date);
            foreach (var date in dates)
            {
                var view = new WorkScheduleDisplayViewModel();
                view.Date = date.Date;
                view.DayOfWeek = date.DayOfWeek;
                view.Count = date.Count;
                result.Add(view);
            }
            //var y = dates.ToList();
            int indexResult = 0, indexItemResult = 1;
            
            //add detail

            foreach (var detail in scheduleDetail)
            {

                var detailView = new WorkScheduleDisplayViewModel.DetailView()
                {
                    IsDayLong = detail.IsDayLong,
                    IsImportant = detail.IsImportant,
                    Time = detail.Time,
                    ID = detail.ID,
                    Content = detail.Content,
                    Location = detail.Location,
                    WorkScheduleID = detail.WorkScheduleID

                };
                if (!detail.IsDayLong)
                {
                    //tách chuỗi
                    //thành phần tham dự
                    var participants = Regex.Split(detail.Participants, @"\|\|");
                    if (participants[0] != string.Empty)
                    {
                        foreach (var part in participants)
                        {
                            var item = Regex.Split(part, @"::");
                            if (item[0] == "1")
                            {
                                var unitID = int.Parse(item[1]);
                                var unitName = context.Units.Where(x => x.ID == unitID).Select(x => x.Name).FirstOrDefault();
                                if (unitName != null)
                                    detailView.Participants.Add("Ban chấp hành " + unitName);
                            }
                            else if (item[0] == "2")
                            {
                                var memberID = int.Parse(item[1]);
                                var member = context.YouthUnionMembers.FirstOrDefault(x => x.ID == memberID);
                                if (member != null)
                                    detailView.Participants.Add(member.Lastname + " " + member.Firstname);
                            }
                            else
                            {
                                detailView.Participants.Add(item[1]);
                            }

                        }
                    }
                    //chairman
                    var chairmans = Regex.Split(detail.ChairMan, @"\|\|");
                    if (chairmans[0] != string.Empty)
                    {
                        foreach (var part in chairmans)
                        {
                            var item = Regex.Split(part, @"::");
                            if (item[0] == "1")
                            {
                                var unitID = int.Parse(item[1]);
                                var unitName = context.Units.Where(x => x.ID == unitID).Select(x => x.Name).FirstOrDefault();
                                if (unitName != null)
                                    detailView.Chairman.Add("Ban chấp hành " + unitName);
                            }
                            else if (item[0] == "2")
                            {
                                var memberID = int.Parse(item[1]);
                                var member = context.YouthUnionMembers.FirstOrDefault(x => x.ID == memberID);
                                if (member != null)
                                detailView.Chairman.Add(member.Lastname + " " + member.Firstname);
                            }
                            else
                            {
                                detailView.Chairman.Add(item[1]);
                            }
                        }
                    }

                }
                result[indexResult].Details.Add(detailView);
                if (indexItemResult == result[indexResult].Count)
                {
                    indexResult++;
                    indexItemResult = 1;

                }
                else
                {
                    indexItemResult++;
                }
            }
            return result;
        }

        public WorkScheduleDetail GetSingleById(long? id)
        {
            return context.WorkScheduleDetails.Find(id);
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }

}
