﻿using System;
using System.Collections.Generic;
using System.Linq;
using QLDoanTruong.Model;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Data.Entity.Infrastructure;
using QLDoanTruong.Model.ViewModel;
using QLDoanTruong.Common;

namespace QLDoanTruong.Service
{
    public class YouthUnionMemberService : IService<YouthUnionMember>
    {
        private YouthUnionUniversityDbContext context;
        public YouthUnionMemberService()
        {
            context = new YouthUnionUniversityDbContext();
        }

        public YouthUnionMember Add(YouthUnionMember entity)
        {
            entity.CreatedDate = DateTime.Now;
            try
            {
                var member = context.YouthUnionMembers.Add(entity);
                context.SaveChanges();
                return member;
            }
            catch(Exception e)
            {
                var u = context.Users.Find(entity.ID);
                if (u != null)
                {
                    context.Users.Remove(u);
                    context.SaveChanges();
                }
            }
            return null;
        }

        public void Update(YouthUnionMember entity)
        {
            //Cần chú ý khi đổi mã sv khi mã là tên đăng nhập
            //var previousEntity = GetSingleById(entity.ID);

            //using (var memberService = new UserService())
            //{
            //}
            ////no tracking
            //((IObjectContextAdapter)context).ObjectContext.Detach(previousEntity);
            var currentEntity = context.YouthUnionMembers.AsNoTracking().FirstOrDefault(x => x.ID == entity.ID);
            entity.CreatedDate = currentEntity.CreatedDate;
            if (entity.Photo == null)
                entity.Photo = currentEntity.Photo;
            entity.ModifiedDate = DateTime.Now;

            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void UpdateBasicInfo(YouthUnionMember entity)
        {
            var previousEntity = context.YouthUnionMembers.AsNoTracking().FirstOrDefault(x => x.ID == entity.ID);
            
            entity.CreatedDate = previousEntity.CreatedDate;
            if (entity.Photo == null)
                entity.Photo = previousEntity.Photo;
            entity.ModifiedDate = DateTime.Now;
            entity.YouthUnionMemberCode = previousEntity.YouthUnionMemberCode;
            entity.Type = previousEntity.Type;
            //entity.User =  previousEntity.User;
            //So sánh đổi lớp
            if (entity.YouthUnionClassID != previousEntity.YouthUnionClassID)
            {
                //Xóa cán bộ lớp
                var leaderClass = context.LeaderClasses
                                   .Where(x => x.ClassID == previousEntity.YouthUnionClassID
                                                && x.YouthUnionMemberID == entity.ID);
                if (leaderClass.Count() > 0)
                {
                    context.LeaderClasses.RemoveRange(leaderClass);
                    
                }

                //so sánh có đổi khoa hay không 
                if (previousEntity.YouthUnionClassID != null && entity.YouthUnionClassID != null)
                {
                    var previousFaultyID = previousEntity.YouthUnionClass.FacultyID;
                    var currentFacultyID = context.YouthUnionClasses
                                                    .FirstOrDefault(x => x.ID == entity.YouthUnionClassID).FacultyID;
                    if (currentFacultyID != previousFaultyID)
                    {
                        //Xóa cán bộ khoa
                        var leaderFaculty = context.LeaderUnits
                                          .Where(x => x.UnitID == previousFaultyID
                                                       && x.YouthUnionMemberID == entity.ID);
                        if (leaderFaculty.Count() > 0)
                        {
                            context.LeaderUnits.RemoveRange(leaderFaculty);
                    
                        }
                    }
                }
            }

            //context.YouthUnionMembers.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(long id)
        {
            YouthUnionMember member = context.YouthUnionMembers.Find(id);
            // context.YouthUnionMembers.Remove(member);
            Delete(member);
        }

        public void Delete(YouthUnionMember entity)
        {
            //Xóa bí thư chi đoàn
            //using (var classService = new YouthUnionClassService())
            //{

            //    var c = classService.GetSingleById(entity.YouthUnionClassID);

            //    if (c != null && c.SecretaryID == entity.ID)
            //    {
            //        c.SecretaryID = null;
            //        classService.Update(c);
            //    }
            //}

            //Xóa leader            
            var leader = context.LeaderUnits.Where(x => x.YouthUnionMemberID == entity.ID);
            if (leader != null)
            {
                context.LeaderUnits.RemoveRange(leader);
            }


            //Xóa khen thưởng
            var achievements = context.Achievements
                        .Where(x => x.YouthUnionID == entity.ID
                                    && x.Type == ConstraintUnionType.MEMBER);
            context.Achievements.RemoveRange(achievements);
            context.Users.Remove(context.Users.Find(entity.ID));
            context.SaveChanges();
            //(new UserService()).Delete(entity.ID);
            //context.YouthUnionMembers.Remove(entity);

        }
        //public List<YouthUnionMember> GetLeaderClass(long? classID, int?typePosition)
        //{
        //    if (classID == null && typePosition == null)
        //        return context.YouthUnionMembers.Where(x => x.PositionID != null).ToList();
        //    if (classID == null)
        //        return context.YouthUnionMembers.Where(x => x.PositionID != null && x.Position.Type == typePosition).ToList();
        //    if (typePosition == null)
        //        return context.YouthUnionMembers.Where(x => x.PositionID != null && x.YouthUnionClassID==classID).ToList();
        //    return context.YouthUnionMembers.Where(x => x.PositionID != null && x.YouthUnionClassID == classID && x.Position.Type == typePosition).ToList();
        //}


        public List<YouthUnionMember> GetAll()
        {
            return context.YouthUnionMembers.OrderBy(x => x.Firstname).ToList();
        }
        /// <summary>
        /// Get member for add leader
        /// </summary>
        /// <returns></returns>
        public List<YouthUnionMember> GetMemberNotLeaderFaculty(long facultyID, string search)
        {
            return context.YouthUnionMembers.Where(x => x.YouthUnionClass.FacultyID == facultyID &&
                        !context.LeaderUnits.Any(l => l.UnitID == facultyID && l.YouthUnionMemberID == x.ID)
                        && ((x.Lastname + " " + x.Firstname).Contains(search) || x.YouthUnionMemberCode.Contains(search)))
                    .OrderBy(x => x.Firstname).ToList();
        }
        /// <summary>
        /// Lấy danh sách cán bộ của một nguời
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetMaxPosition(long id)
        {
            var positions = GetAllPossition(id);
            return positions.Select(x => x.Name).FirstOrDefault();

        }
        public List<Position> GetAllPossition(long id)
        {
            return context.LeaderClasses.Where(x => x.YouthUnionMemberID == id).Select(x => x.Position)
                        .Union(context.LeaderUnits.Where(m => m.YouthUnionMemberID == id).Select(m => m.Position))
                        .OrderBy(x => new { x.Level, x.UnitType }).ToList();
        }

        public string GetFullPosition(long id)
        {
            var positions = GetAllPossition(id);
            return string.Join(", ", positions.Select(x => x.Name).ToList());
        }

        public bool ExistedMemberCode(string memberCode)
        {
            return context.YouthUnionMembers.FirstOrDefault(m => m.YouthUnionMemberCode == memberCode) != null;
        }
        public bool ExistedEmail(string email, long? memberID = null)
        {
            if (memberID == null)
                return context.YouthUnionMembers.FirstOrDefault(m => m.Email == email) != null;
            return context.YouthUnionMembers.FirstOrDefault(m => m.Email == email && m.ID != memberID) != null;
        }
        public YouthUnionMember GetSingleById(long? id)
        {
            return context.YouthUnionMembers.AsNoTracking().FirstOrDefault(x=>x.ID==id);
        }

        public YouthUnionMember GetSingleByMemberCode(string memberCode)
        {
            return context.YouthUnionMembers.FirstOrDefault(x => x.YouthUnionMemberCode == memberCode);
        }


        public List<YouthUnionMember> GetByClass(long classID)
        {
            return context.YouthUnionMembers.Where(x => x.YouthUnionClassID == classID).OrderBy(x => x.Firstname).ToList();
        }

        public List<YouthUnionMember> GetByFaculty(long facultyID)
        {
            return context.YouthUnionMembers.Where(x => x.YouthUnionClass.FacultyID == facultyID).OrderBy(x => x.Firstname).ToList();

        }

        public List<YouthUnionMember> Find(Expression<Func<YouthUnionMember, bool>> condition)
        {
            return context.YouthUnionMembers.Where(condition).ToList();
        }

        public List<YouthUnionMember> FindBySearchViewModel(SearchMemberViewModel searchForm)
        {
            searchForm.Fullname = searchForm.Fullname == null ? string.Empty : searchForm.Fullname.Trim();
            searchForm.MemberCode = searchForm.MemberCode == null ? string.Empty : searchForm.MemberCode.Trim();
            searchForm.Phone = searchForm.Phone == null ? searchForm.Phone = string.Empty : searchForm.Phone.Trim();
            //điều kiện textbox
            Expression<Func<YouthUnionMember, bool>> condition = x => x.YouthUnionMemberCode.Contains(searchForm.MemberCode)
                                                                    && (x.Lastname + " " + x.Firstname).Contains(searchForm.Fullname)
                                                                    && ((x.Phone.ToString() ?? string.Empty).Contains(searchForm.Phone));

            IQueryable<YouthUnionMember> result = null;

            if (searchForm.Type == ConstraintUnionMemberType.STAFF)
            {
                result = context.YouthUnionMembers.Where(x => x.Type == searchForm.Type).OrderBy(x => x.Firstname);
            }
            else
            {
                if (searchForm.Type == ConstraintUnionMemberType.STUDENT)
                {
                    if (searchForm.ClassID != null)
                        result = context.YouthUnionMembers.Where(x => x.YouthUnionClassID == searchForm.ClassID).OrderBy(x => x.Firstname);

                    else if (searchForm.FacultyID != null)
                        result = context.YouthUnionMembers.Where(x => x.YouthUnionClass.FacultyID == searchForm.FacultyID).OrderBy(x => x.Firstname);
                    //Theo năm học
                    if (searchForm.AcademicYearID != null)
                    {
                        if (result != null)
                            result = result.Where(x => x.YouthUnionClass.AcademicYearID == searchForm.AcademicYearID);
                        else
                            result = context.YouthUnionMembers.Where(x => x.YouthUnionClass.AcademicYearID == searchForm.AcademicYearID).OrderBy(x => x.Firstname);
                    }
                }
                else if (searchForm.Type == ConstraintUnionMemberType.LECTURE)
                // tìm gảng viên
                {
                    //Tìm CĐ CB
                    if (searchForm.FacultyID != null)
                    {
                        var classes = context.YouthUnionClasses.FirstOrDefault(x => x.FacultyID == searchForm.FacultyID && x.Type == ConstraintUnionClassType.LECTURE);
                        result = context.YouthUnionMembers.Where(x => x.YouthUnionClassID == classes.ID).OrderBy(x => x.Firstname);

                    }
                }
                if (result != null)
                    result = result.Where(x => x.Type == searchForm.Type);
                else
                    result = context.YouthUnionMembers.Where(x => x.Type == searchForm.Type).OrderBy(x => x.Firstname);
            }
            // theo tôn giáo
            if (searchForm.ReligionID != null)
            {

                result = result.Where(x => x.ReligionID == searchForm.ReligionID);
            }
            //theo các ô text
            result = result.Where(condition);
            return result.ToList();
        }

        public int GetTypeMemberByID(long id)
        {
            return context.YouthUnionMembers.AsNoTracking().FirstOrDefault(x => x.ID == id).Type;
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }

}
