﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class WorkScheduleService : IService<WorkSchedule>
    {
        private YouthUnionUniversityDbContext context;

        public WorkScheduleService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public WorkSchedule Add(WorkSchedule entity)
        {
            entity.CreatedDate = entity.ModifiedDate = DateTime.Now;
            var workSchedule = context.WorkSchedules.Add(entity);
            context.SaveChanges();
            return workSchedule;
        }

        //public WorkSchedule Add(int week)
        //{
        //  var workSchedule = context.WorkSchedules()
        //}

        public void Update(WorkSchedule entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            WorkSchedule workSchedule = context.WorkSchedules.Find(id);
            Delete(workSchedule);

        }

        public void Delete(WorkSchedule entity)
        {
            context.WorkSchedules.Remove(entity);
            context.SaveChanges();
        }

        public List<WorkSchedule> GetAll()
        {
            return context.WorkSchedules.OrderByDescending(x => new { x.SchoolYearID, x.Week }).ToList();
        }

        public WorkSchedule GetSingleById(long? id)
        {
            return context.WorkSchedules.Find(id);
        }

        public int GetLastWeekHasSchedule(int schoolYearID)
        {
            try
            {
                var week = context.WorkSchedules.Where(x => x.SchoolYearID == schoolYearID).Max(x => x.Week);

                return week;
            }
            catch
            {
                return 0;
            }

        }
        public WorkSchedule GetByWeek(int week, int schoolYearID)
        {
            return context.WorkSchedules.FirstOrDefault(x => x.Week == week && x.SchoolYearID == schoolYearID);
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }

}
