﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using QLDoanTruong.Common.ConstraintConfig;

namespace QLDoanTruong.Service
{
    public class MonthlyReportExpireDateService : IService<MonthlyReportExpireDate>
    {
        private YouthUnionUniversityDbContext context;

        public MonthlyReportExpireDateService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public MonthlyReportExpireDate Add(MonthlyReportExpireDate entity)
        {
            var expire = context.MonthlyReportExpireDates.Add(entity);
            context.SaveChanges();
            return expire;
        }

        public void Update(MonthlyReportExpireDate entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            MonthlyReportExpireDate award = context.MonthlyReportExpireDates.Find(id);
            Delete(award);

        }

        public void Delete(MonthlyReportExpireDate entity)
        {
            context.MonthlyReportExpireDates.Remove(entity);
            context.SaveChanges();
        }

        public List<MonthlyReportExpireDate> GetAll()
        {
            return context.MonthlyReportExpireDates.ToList();
        }


        public MonthlyReportExpireDate GetSingleById(long? id)
        {
            return context.MonthlyReportExpireDates.Find(id);
        }

        public MonthlyReportExpireDate GetSingleByMonthAndYear(int month, int year)
        {
            var res = context.MonthlyReportExpireDates.AsNoTracking().FirstOrDefault(x => x.Month == month && x.Year == year);
            if (res == null)
            {
                res = new MonthlyReportExpireDate();

                int dueDateMonth, dueDateYear, dueDateDay;

                //lấy cài đặt mặc định
                var defaultConfig = GetDefaultConfig();
                var cfg_dueDate = defaultConfig.FirstOrDefault(x => x.Key == MonthlyReportExpireDateConfiguration.Key.DUE_DATE).Value;
                //23|59|28|THIS
                var paramDueDate = cfg_dueDate.Split(MonthlyReportExpireDateConfiguration.SEPARATE_CHARACTER);
                //Xét tháng
                if (paramDueDate[3].Equals(MonthlyReportExpireDateConfiguration.Value.DUE_DATE_MONTH_THIS, StringComparison.CurrentCultureIgnoreCase)) //nếu là tháng này
                {
                    dueDateMonth = month;
                    dueDateYear = year;
                }
                else //tháng sau
                {
                    dueDateMonth = month == 12 ? 1 : month + 1; // nếu tháng này là 12
                    dueDateYear = month == 12 ? year + 1 : year;
                }
                //Xét ngày
                if (paramDueDate[2].Equals(MonthlyReportExpireDateConfiguration.Value.DUE_DATE_DAY_END_MONTH, StringComparison.CurrentCultureIgnoreCase)) //nếu là ngày cuối tháng
                {
                    dueDateDay = DateTime.DaysInMonth(dueDateYear, dueDateMonth);
                }
                else
                {
                    dueDateDay = int.Parse(paramDueDate[2]);
                }

                res.DueDate = new DateTime(dueDateYear, dueDateMonth, dueDateDay, int.Parse(paramDueDate[0]), int.Parse(paramDueDate[1]), 0);

                res.IsEnabledCutOffDate = defaultConfig.FirstOrDefault(x => x.Key == MonthlyReportExpireDateConfiguration.Key.ENABLED_CUT_OFF_DATE).Value.Equals("true", StringComparison.CurrentCultureIgnoreCase);

                res.CutOffDate = res.DueDate.AddDays(float.Parse(defaultConfig.FirstOrDefault(x => x.Key == MonthlyReportExpireDateConfiguration.Key.CUT_OFF_DATE).Value));

                res.DeducatePoint = float.Parse(defaultConfig.FirstOrDefault(x => x.Key == MonthlyReportExpireDateConfiguration.Key.DEDUCATE_POINT).Value);

                res.IsEnabledDeducateEveryExpireDay = defaultConfig.FirstOrDefault(x => x.Key == MonthlyReportExpireDateConfiguration.Key.ENABLED_DEDUCATE_EVERY_EXPIRE_DAY).Value.Equals("true", StringComparison.CurrentCultureIgnoreCase);

                res.Month = month;
                res.Year = year;

                res = Add(res);
                
            }

            return res;

        }

        public List<Config> GetDefaultConfig()
        {
            using (var cfgService = new ConfigService())
            {
                var defaultConfig = cfgService.GetByPlugin(MonthlyReportExpireDateConfiguration.PLUGIN_NAME);
                if (defaultConfig == null)
                {
                    defaultConfig = new List<Config>();
                }
                var cfg_dueDate = defaultConfig.FirstOrDefault(x => x.Key == MonthlyReportExpireDateConfiguration.Key.DUE_DATE);
                if (cfg_dueDate == null)
                {
                    cfg_dueDate = cfgService.Add(new Config()
                    {
                        Plugin = MonthlyReportExpireDateConfiguration.PLUGIN_NAME,
                        Key = MonthlyReportExpireDateConfiguration.Key.DUE_DATE,
                        //Gía trị mặc định 23h59 ngày 28 tháng này
                        Value = "23" + MonthlyReportExpireDateConfiguration.SEPARATE_CHARACTER
                                + "59" + MonthlyReportExpireDateConfiguration.SEPARATE_CHARACTER
                                + "28" + MonthlyReportExpireDateConfiguration.SEPARATE_CHARACTER
                                + MonthlyReportExpireDateConfiguration.Value.DUE_DATE_MONTH_THIS,
                        Description = "Hạn nộp"
                    });
                    defaultConfig.Add(cfg_dueDate);
                }

                var cfg_enabledCutOffDate = defaultConfig.FirstOrDefault(x => x.Key == MonthlyReportExpireDateConfiguration.Key.ENABLED_CUT_OFF_DATE);
                if (cfg_enabledCutOffDate == null)
                {
                    cfg_enabledCutOffDate = cfgService.Add(new Config()
                    {
                        Plugin = MonthlyReportExpireDateConfiguration.PLUGIN_NAME,
                        Key = MonthlyReportExpireDateConfiguration.Key.ENABLED_CUT_OFF_DATE,
                        Value = "True",
                        Description = "Cho phép nộp muộn"
                    });
                    defaultConfig.Add(cfg_enabledCutOffDate);
                }

                //var cfg_isEnabledCutOffDate = cfg_enabledCutOffDate.Value.Equals("true", StringComparison.CurrentCultureIgnoreCase);
                var cfg_cutOffDate = defaultConfig.FirstOrDefault(x => x.Key == MonthlyReportExpireDateConfiguration.Key.CUT_OFF_DATE);
                if (cfg_cutOffDate == null)
                {
                    cfg_cutOffDate = cfgService.Add(new Config()
                    {
                        Plugin = MonthlyReportExpireDateConfiguration.PLUGIN_NAME,
                        Key = MonthlyReportExpireDateConfiguration.Key.CUT_OFF_DATE,
                        Value = "7",
                        Description = "Nộp muộn sau bao nhiêu ngày"
                    });
                    defaultConfig.Add(cfg_cutOffDate);
                }

                var cfg_deducatePoint = defaultConfig.FirstOrDefault(x => x.Key == MonthlyReportExpireDateConfiguration.Key.DEDUCATE_POINT);
                if (cfg_deducatePoint == null)
                {
                    cfg_deducatePoint = cfgService.Add(new Config()
                    {
                        Plugin = MonthlyReportExpireDateConfiguration.PLUGIN_NAME,
                        Key = MonthlyReportExpireDateConfiguration.Key.DEDUCATE_POINT,
                        Value = "10",
                        Description = "Điểm trừ nộp muộn"
                    });
                    defaultConfig.Add(cfg_deducatePoint);
                }

                var cfg_enabledDeducateEveryDay = defaultConfig.FirstOrDefault(x => x.Key == MonthlyReportExpireDateConfiguration.Key.ENABLED_DEDUCATE_EVERY_EXPIRE_DAY);
                if (cfg_enabledDeducateEveryDay == null)
                {
                    cfg_enabledDeducateEveryDay = cfgService.Add(new Config()
                    {
                        Plugin = MonthlyReportExpireDateConfiguration.PLUGIN_NAME,
                        Key = MonthlyReportExpireDateConfiguration.Key.ENABLED_DEDUCATE_EVERY_EXPIRE_DAY,
                        Value = "false",
                        Description = "Trừ điểm mỗi ngày"
                    });
                    defaultConfig.Add(cfg_enabledDeducateEveryDay);
                }



                return defaultConfig;
            }
        }


        public void Dispose()
        {
            context.Dispose();
        }

    }

}
