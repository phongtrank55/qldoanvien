﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class ConfigService : IService<Config>
    {
        private YouthUnionUniversityDbContext context;

        public ConfigService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public Config Add(Config entity)
        {
            var award = context.Configs.Add(entity);
            context.SaveChanges();
            return award;
        }

        public void Update(Config entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            Config award = context.Configs.Find(id);
            Delete(award);

        }

        public void Delete(Config entity)
        {
            context.Configs.Remove(entity);
            context.SaveChanges();
        }

        public List<Config> GetAll()
        {
            return context.Configs.ToList();
        }
        public Config GetSingleById(long? id)
        {
            return context.Configs.Find(id);
        }
        public String GetValueByKey(string key)
        {
            var obj = context.Configs.FirstOrDefault(x => x.Key == key);
            if (obj == null) return null;
            return obj.Value;
        }

        public List<Config> GetByPlugin(string pluginName)
        {
            return context.Configs.Where(x => x.Plugin.ToLower() == pluginName.ToLower()).ToList();
        }
        public void Dispose()
        {
            context.Dispose();
        }

    }

}
