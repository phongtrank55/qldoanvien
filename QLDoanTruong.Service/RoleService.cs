﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class RoleService : IService<Role>
    {
        private YouthUnionUniversityDbContext context;

        public RoleService()
        {
            context = new YouthUnionUniversityDbContext();
        }

        public Role Add(Role entity)
        {
            var role = context.Roles.Add(entity);
            context.SaveChanges();
            return role;
        }

        public void Update(Role entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            Role role = context.Roles.Find(id);
            Delete(role);

        }

        public void Delete(Role entity)
        {
            context.Roles.Remove(entity);
            context.SaveChanges();
        }

        public List<Role> GetAll()
        {
            return context.Roles.OrderBy(x=>new {x.FunctionID, x.Order}).ToList();
        }
        public List<Role> GetByFunction(int functionID)
        {
            return context.Roles.Where(x => x.FunctionID == functionID).OrderBy(x=>x.Order).ToList();
        }
        public Role GetSingleById(long? id)
        {
            return context.Roles.Find(id);
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }

}
