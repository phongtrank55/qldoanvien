﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace QLDoanTruong.Service
{
    interface IService<TEntity> : IDisposable where TEntity : class
    {

        TEntity Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(long id);

        void Delete(TEntity entity);

        List<TEntity> GetAll();

        TEntity GetSingleById(long? id);
    }
}
