﻿using QLDoanTruong.Model;
using QLDoanTruong.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using QLDoanTruong.Common;

namespace QLDoanTruong.Service
{
    public class AchievementService : IService<Achievement>
    {
        private YouthUnionUniversityDbContext context;

        public AchievementService()
        {
            context = new YouthUnionUniversityDbContext();
        }

        public Achievement Add(Achievement entity)
        {
            var achievement = context.Achievements.Add(entity);
            context.SaveChanges();
            return achievement;
        }

        public void Update(Achievement entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(long id)
        {
            Achievement achievement = context.Achievements.Find(id);
            Delete(achievement);
        }

        public void Delete(Achievement entity)
        {
            context.Achievements.Remove(entity);
            context.SaveChanges();
        }

        public List<Achievement> GetAll()
        {
            return context.Achievements.ToList();
        }
        public List<AchievementViewModel> GetAllView()
        {
            var result = context.Achievements.Select(x => new AchievementViewModel()
            {
                ID = x.ID,

                Type = x.Type,
                SchoolYearName = x.SchoolYear.Name,
                AwardName = x.Award.Name,
                Document = x.Document,
                AwardDescription = x.Award.Description,
                YouthUnionID = x.YouthUnionID
            }).ToList();
            try
            {
                foreach (var item in result)
                {
                    if (item.Type == ConstraintUnionType.FACULTY)//lcđ
                    {
                        item.YouthUnionName = context.Units.Find(item.YouthUnionID).Name;
                    }
                    else if (item.Type == ConstraintUnionType.CLASS)//cđ
                    {
                        item.YouthUnionName = context.YouthUnionClasses.Find(item.YouthUnionID).Name;
                    }
                    else //đv
                    {
                        var mem = context.YouthUnionMembers.Find(item.YouthUnionID);
                        item.YouthUnionName = mem.Lastname + " " + mem.Firstname;
                    }
                }
                return result;
            }
            catch { return null; }
        }
        /// <summary>
        /// LẤy thành tích của đối tượng
        /// </summary>
        /// <param search="classType"></param>
        /// <param search="id"></param>
        /// <returns></returns>
        public List<AchievementViewModel> GetViewByUnionID(int type, long? unionID)
        {
            if (unionID == null) return null;
            var result = context.Achievements.Where(x => x.YouthUnionID == unionID && x.Type == type).Select(x => new AchievementViewModel()
            {
                ID = x.ID,
                SchoolYearName = x.SchoolYear.Name,
                AwardName = x.Award.Name,
                Document = x.Document,
                AwardDescription = x.Award.Description,
                AwardLevelName = x.Award.AwardLevel.Name
            }).ToList();

            foreach (var item in result)
            {
                if (item.Type == ConstraintUnionType.FACULTY)//lcđ
                {
                    item.YouthUnionName = context.Units.Find(unionID).Name;
                }
                else if (item.Type == ConstraintUnionType.CLASS)//cđ
                {
                    item.YouthUnionName = context.YouthUnionClasses.Find(unionID).Name;
                }
                else //đv
                {
                    var mem = context.YouthUnionMembers.Find(unionID);
                    item.YouthUnionName = mem.Lastname + " " + mem.Firstname;
                }
            }

            return result;
        }
        public Achievement GetSingleById(long? id)
        {
            return context.Achievements.Find(id);
        }
        public List<AchievementViewModel> GetViewByAward(int? awardLevelID, int? awardID, int? schoolYearID)
        {
            
            IEnumerable<Achievement> query;

            if (schoolYearID == null)
            {
                query = context.Achievements;
            }
            else
            {
                query = context.Achievements.Where(x => x.AwardID == awardID);
            }
            
            if (awardID != null)
            {
                query = query.Where(x => x.AwardID == awardID);
            }
            
            if(awardLevelID!=null)
            {
                query = query.Where(x => x.Award.AwardLevelID == awardLevelID);
            }

                var result = query.Select(x => new AchievementViewModel()
                          {
                              ID = x.ID,
                              Type = x.Type,
                              SchoolYearName = x.SchoolYear.Name,
                              AwardName = x.Award.Name,
                              Document = x.Document,
                              AwardDescription = x.Award.Description,
                              YouthUnionID = x.YouthUnionID
                          }).ToList();

            
            foreach (var item in result)
            {
                if (item.Type == ConstraintUnionType.FACULTY)//lcđ
                {
                    item.YouthUnionName = context.Units.Find(item.YouthUnionID).Name;
                }
                else if (item.Type == ConstraintUnionType.CLASS)//cđ
                {
                    item.YouthUnionName = context.YouthUnionClasses.Find(item.YouthUnionID).Name;
                }
                else //đv
                {
                    var mem = context.YouthUnionMembers.Find(item.YouthUnionID);
                    item.YouthUnionName = mem.Lastname + " " + mem.Firstname;
                }
            }
            return result;
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }

}
