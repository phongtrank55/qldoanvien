﻿using QLDoanTruong.Model;
using QLDoanTruong.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class LeaderUnitService : IService<LeaderUnit>
    {
        private YouthUnionUniversityDbContext context;

        public LeaderUnitService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public LeaderUnit Add(LeaderUnit entity)
        {
            var Leader = context.LeaderUnits.Add(entity);
            context.SaveChanges();
            return Leader;
        }

        public void Update(LeaderUnit entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            LeaderUnit Leader = context.LeaderUnits.Find(id);
            Delete(Leader);

        }

        public void Delete(LeaderUnit entity)
        {
            context.LeaderUnits.Remove(entity);
            context.SaveChanges();
        }

        public List<LeaderUnit> GetAll()
        {
            return context.LeaderUnits.ToList();
        }

        public LeaderUnit GetSingleById(long? id)
        {
            return context.LeaderUnits.Find(id);
        }

        public List<LeaderUnitViewModel> GetAllViewByUnitID(long? unitID)
        {
            return context.LeaderUnits
                .Where(x => x.UnitID == unitID)
                .Select(x => new LeaderUnitViewModel() {
                    MemberID = x.YouthUnionMemberID,
                    ID=x.ID,
                    YouthUnionMemberCode = x.YouthUnionMember.YouthUnionMemberCode,
                    Firstname = x.YouthUnionMember.Firstname,
                    PositionID = x.PositionID,
                    Lastname = x.YouthUnionMember.Lastname,
                    DateOfBirth = x.YouthUnionMember.DateOfBirth,
                    YouthUnionClass = x.YouthUnionMember.YouthUnionClass.Name,
                    Email = x.YouthUnionMember.Email,
                    Position = x.Position.Name,
                    Phone = x.YouthUnionMember.Phone
                }).OrderBy(m => m.Position).ToList();

        }
        public List<LeaderUnitViewModel> GetAllView()
        {
            return context.LeaderUnits
                .Select(x => new LeaderUnitViewModel()
                {
                    MemberID = x.YouthUnionMemberID,
                    ID = x.ID,
                    Firstname = x.YouthUnionMember.Firstname,
                    Lastname = x.YouthUnionMember.Lastname,
                    Email = x.YouthUnionMember.Email,
                    Position = x.Position.Name,
                    PositionID=x.PositionID,
                    UnitName = x.Unit.Name,
                }).OrderBy(m =>m.Position).ToList();

        }
        public void Dispose()
        {
            context.Dispose();
        }

        
    }

}
