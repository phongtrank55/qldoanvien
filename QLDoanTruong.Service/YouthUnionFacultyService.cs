﻿using System.Collections.Generic;
using System.Linq;
using QLDoanTruong.Model;
using System.Data.Entity;
using QLDoanTruong.Model.ViewModel;
using QLDoanTruong.Common;

namespace QLDoanTruong.Service
{
    public class YouthUnionFacultyService : IService<Unit>
    {
        private YouthUnionUniversityDbContext context;

        public YouthUnionFacultyService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public Unit Add(Unit entity)
        {
            entity.Type = QLDoanTruong.Common.ConstraintUnitType.FACULTY; //lcđ
            var faculty = context.Units.Add(entity);
            context.SaveChanges();

            //thêm chi đoàn cán bộ
            using (var classService = new YouthUnionClassService())
            {
                var leadClass = new YouthUnionClass()
                {
                    FacultyID = entity.ID,
                    Type = QLDoanTruong.Common.ConstraintUnionClassType.LECTURE, //CĐ cán bộ
                    Name = "Chi đoàn Cán bộ " + entity.Name,
                    YouthUnionClassCode = "CB" + entity.UnitCode,

                };
                classService.Add(leadClass);
            }
            return faculty;
        }

        public void Update(Unit entity)
        {
            entity.Type = QLDoanTruong.Common.ConstraintUnitType.FACULTY;
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public YouthUnionFacultyViewModel GetSingleViewById(long? id)
        {

            if (id == null) return null;
            return context.Units
                .Where(x => x.ID == id && x.Type == 1)
                .Select(x => new YouthUnionFacultyViewModel()
                {
                    ID = x.ID,
                    YouthUnionFaculityCode = x.UnitCode,
                    Name = x.Name,
                    ClassAmount = context.YouthUnionClasses.Count(m => m.FacultyID == x.ID),
                    UnionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.FacultyID == x.ID),
                    MaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.FacultyID == x.ID && m.Gender == 1),
                    FemaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.FacultyID == x.ID && m.Gender == 0),
                    ReligionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.FacultyID == x.ID && m.Religion.ID != 1),
                    EthnicAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.FacultyID == x.ID && m.Race.ID != 1),
                })
                .FirstOrDefault();
        }
        public List<YouthUnionFacultyViewModel> GetAllView()
        {

            return context.Units.Where(x => x.Type == 1)
                .Select(x => new YouthUnionFacultyViewModel()
                {
                    ID = x.ID,
                    YouthUnionFaculityCode = x.UnitCode,
                    Name = x.Name,
                    ClassAmount = context.YouthUnionClasses.Count(m => m.FacultyID == x.ID),
                    UnionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.FacultyID == x.ID),
                    MaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.FacultyID == x.ID && m.Gender == 1),
                    FemaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.FacultyID == x.ID && m.Gender == 0),
                    ReligionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.FacultyID == x.ID && m.Religion.ID != 1),
                    EthnicAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.FacultyID == x.ID && m.Race.ID != 1),
                }).ToList();
        }
        public void Delete(long id)
        {

            Unit faculty = GetSingleById(id);

            Delete(faculty);

        }

        public void Delete(Unit entity)
        {
            //Xóa leader
            //Xóa khen thưởng
            context.Achievements.RemoveRange(
                    context.Achievements
                        .Where(x => x.YouthUnionID == entity.ID
                                    && x.Type == ConstraintUnionType.FACULTY));

            //Xóa lớp
            using (var classService = new YouthUnionClassService())
            {
                var classes = context.YouthUnionClasses.Where(x => x.FacultyID == entity.ID).Select(x=>x.ID);
                foreach (var c in classes)
                {
                    classService.Delete(c);
                }
            }

            context.Units.Remove(entity);
            context.SaveChanges();
            //(new UnitService()).Delete(entity.ID);

        }
        public int GetIDByName(string name)
        {
            var faculty = context.Units.FirstOrDefault(x => x.Name == name
                                    && x.Type == QLDoanTruong.Common.ConstraintUnitType.FACULTY);
            return faculty != null ? faculty.ID : 0;
        }
        public List<Unit> GetAll()
        {
            return context.Units.Where(x => x.Type == QLDoanTruong.Common.ConstraintUnitType.FACULTY).ToList();
        }

        public Unit GetSingleById(long? id)
        {
            if (id == null) return null;
            return context.Units.Where(x => x.Type == QLDoanTruong.Common.ConstraintUnitType.FACULTY && x.ID == id).FirstOrDefault();
        }

        public bool ExistedFacultyCode(string facultyCode, int? facultyID=null)
        {
            if (facultyID == null)
            {
                return context.Units.FirstOrDefault(x => x.UnitCode == facultyCode) != null;
            }
            //Nếu trùng nhưng k phải khoa đang xét
            return context.Units.FirstOrDefault(x => x.UnitCode == facultyCode && x.ID != facultyID) != null;
        }
        public bool ExistedEmail(string email, int? facultyID=null)
        {
            if (facultyID == null)
                return context.Units.FirstOrDefault(m => m.Email == email) != null;
            return context.Units.FirstOrDefault(m => m.Email == email && m.ID != facultyID) != null;
        }
       
        public void Dispose()
        {
            context.Dispose();
        }

    }

}
