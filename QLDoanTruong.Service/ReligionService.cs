﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class ReligionService : IService<Religion>
    {
        private YouthUnionUniversityDbContext context;

        public ReligionService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public Religion Add(Religion entity)
        {
            var religion = context.Religions.Add(entity);
            context.SaveChanges();
            return religion;
        }
        public Religion GetPopular()
        {
            var res = context.Religions.AsNoTracking().FirstOrDefault(x => x.IsPopular == true);
            if (res == null)
            {
                var defaultRace = "Không";
                try
                {
                    res = context.Religions.Add(new Religion()
                    {
                        Name = defaultRace,
                        IsPopular = true
                    });

                }
                catch { }
            }
            return res;
        }
        public void Update(Religion entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            Religion religion = context.Religions.Find(id);
            Delete(religion);

        }

        public void Delete(Religion entity)
        {
            context.Religions.Remove(entity);
            context.SaveChanges();
        }

        public List<Religion> GetAll()
        {
            return context.Religions.ToList();
        }

        public Religion GetSingleById(long? id)
        {
            return context.Religions.Find(id);
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }

}
