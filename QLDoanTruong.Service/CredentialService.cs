﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using QLDoanTruong.Model.ViewModel;

namespace QLDoanTruong.Service
{
    public class CredentialService : IService<Credential>
    {
        private YouthUnionUniversityDbContext context;

        public CredentialService()
        {
            context = new YouthUnionUniversityDbContext();
        }
         

        public Credential Add(Credential entity)
        {
            var credential = context.Credentials.Add(entity);
            context.SaveChanges();
            return credential;
        }

        public void Update(Credential entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            Credential credential = context.Credentials.Find(id);
            Delete(credential);

        }

        public void Delete(Credential entity)
        {
            context.Credentials.Remove(entity);
            context.SaveChanges();
        }

        public List<Credential> GetAll()
        {
            return context.Credentials.ToList();
        }

        public List<CredentialViewModel> GetAllView()
        {
            var groupUsers = context.GroupUsers.Select(x => x.ID);
            var functions = context.Functions.OrderBy(x => x.Order);

            var result = new List<CredentialViewModel>();
            
            foreach (var function in functions)
            {
                var item = new CredentialViewModel();
                item.FunctionName = function.Name;
                item.ID = function.ID;

                var roles = function.Roles.OrderBy(x => x.Order);
                foreach(var role in roles)
                {
                    //các  quyền thuộc chức năng   
                    var rolesItemResult = new CredentialViewModel.RoleItem();
//                    rolesItemResult.Order = role.Order;
                    rolesItemResult.RoleName=role.Name;
                    rolesItemResult.RoleID = role.ID;

                    foreach(var groupID in groupUsers)
                    {
                        rolesItemResult.Values.Add(context.Credentials
                                                                .FirstOrDefault(x => x.GroupUserID == groupID 
                                                                    && x.RoleID == role.ID) != null);
                    }
                
                    item.Roles.Add(rolesItemResult);
                }

                result.Add(item);
            }
            return result;
        }
        public Credential GetSingleById(long? id)
        {
            return context.Credentials.Find(id);
        }

        public Credential GetSingle(string roleID, int groupID)
        {
            return context.Credentials.FirstOrDefault(x => x.RoleID == roleID && x.GroupUserID == groupID);
        }
        public void Dispose()
        {
            context.Dispose();
        }

    }

}
