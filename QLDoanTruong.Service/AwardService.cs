﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class AwardService : IService<Award>
    {
        private YouthUnionUniversityDbContext context;

        public AwardService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public Award Add(Award entity)
        {
            var award = context.Awards.Add(entity);
            context.SaveChanges();
            return award;
        }

        public void Update(Award entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            Award award = context.Awards.Find(id);
            Delete(award);

        }

        public void Delete(Award entity)
        {
            context.Awards.Remove(entity);
            context.SaveChanges();
        }

        public List<Award> GetAll()
        {
            return context.Awards.ToList();
        }

        public List<Award> GetByAwardLevelAndType(int? awardLevelID, int? type)
        {
            if (awardLevelID == null && type == null)
                return GetAll();
            else if (type == null)
                return context.Awards.Where(x => x.AwardLevelID == awardLevelID).ToList();
            return context.Awards.Where(x => x.Type == type).ToList();
        }
        public List<Award> GetByAwardLevel(int? awardLevelID)
        {
            if (awardLevelID==null)
                return GetAll();
            return context.Awards.Where(x => x.AwardLevelID==awardLevelID).ToList();
        }

        public Award GetSingleById(long? id)
        {
            return context.Awards.Find(id);
        }
        public void Dispose()
        {
            context.Dispose();
        }

    }

}
