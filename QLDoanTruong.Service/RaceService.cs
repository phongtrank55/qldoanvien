﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class RaceService : IService<Race>
    {
        private YouthUnionUniversityDbContext context;

        public RaceService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public Race Add(Race entity)
        {
            var race = context.Races.Add(entity);
            context.SaveChanges();
            return race;
        }

        public void Update(Race entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            Race race = context.Races.Find(id);
            Delete(race);

        }

        public Race GetPopular()
        {
            var res = context.Races.AsNoTracking().FirstOrDefault(x => x.IsPopular == true);
            if (res == null)
            {
                var defaultRace = "Kinh";
                try
                {
                    res = context.Races.Add(new Race(){
                        Name=defaultRace, IsPopular=true
                    });

                }
                catch { }
            }
            return res;
        }
        public void Delete(Race entity)
        {
            context.Races.Remove(entity);
            context.SaveChanges();
        }

        public List<Race> GetAll()
        {
            return context.Races.ToList();
        }

        public Race GetSingleById(long? id)
        {
            return context.Races.Find(id);
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }

}
