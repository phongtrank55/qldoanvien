﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class AcademicYearService : IService<AcademicYear>
    {
        private YouthUnionUniversityDbContext context;

        public AcademicYearService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public AcademicYear Add(AcademicYear entity)
        {
            var AcademicYear = context.AcademicYears.Add(entity);
            context.SaveChanges();
            return AcademicYear;
        }

        public void Update(AcademicYear entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            AcademicYear AcademicYear = context.AcademicYears.Find(id);
            Delete(AcademicYear);

        }

        public void Delete(AcademicYear entity)
        {
            context.AcademicYears.Remove(entity);
            context.SaveChanges();
        }
        public AcademicYear GetFirst() {
            return context.AcademicYears.FirstOrDefault();
        }
        public List<AcademicYear> GetAll()
        {
            return context.AcademicYears.ToList();
        }

        public AcademicYear GetSingleById(long? id)
        {
            return context.AcademicYears.Find(id);
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }

}
