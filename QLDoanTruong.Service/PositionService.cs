﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using QLDoanTruong.Common;

namespace QLDoanTruong.Service
{
    public class PositionService : IService<Position>
    {
        private YouthUnionUniversityDbContext context;

        public PositionService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public Position Add(Position entity)
        {
            var position = context.Positions.Add(entity);
            context.SaveChanges();
            return position;
        }

        public void Update(Position entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            Position position = context.Positions.Find(id);
            Delete(position);

        }

        public void Delete(Position entity)
        {
            context.Positions.Remove(entity);
            context.SaveChanges();
        }

        public List<Position> GetAll()
        {
            return context.Positions.ToList();
        }

        public List<Position> GetUnionOfFaculty()
        {
            return context.Positions.Where(x => x.Type == ConstraintPositionType.UNION && x.UnitType==ConstraintPositionUnitType.FACULTY).ToList();
        }
        //lấy chức vụ đoÀN
        
        public List<Position> GetAllOfClass() {
            return context.Positions.Where(x => x.UnitType == ConstraintPositionUnitType.CLASS).ToList();
        }
        public List<Position> GetUnionOfUniversity()
        {
            return context.Positions.Where(x => x.Type == ConstraintPositionType.UNION && x.UnitType == ConstraintPositionUnitType.UNIVERSITY).ToList();
        }

        public Position GetSingleById(long? id)
        {
            return context.Positions.FirstOrDefault(x => x.ID == id);
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }

}
