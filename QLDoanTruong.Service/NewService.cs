﻿using System.Collections.Generic;
using System.Linq;
using QLDoanTruong.Model;
using System.Data.Entity;
using QLDoanTruong.Common;
using System;

namespace QLDoanTruong.Service
{
    public class NewService : IService<New>
    {
        private YouthUnionUniversityDbContext context = new YouthUnionUniversityDbContext();

        public New Add(New entity)
        {
            entity.CreatedDate = entity.ModifiedDate = DateTime.Now;
            var _new = context.News.Add(entity);

            context.SaveChanges();
            return _new;
        }
        public void Update(New entity)
        {
            entity.ModifiedDate = DateTime.Now;
            var currentEntity = context.News.AsNoTracking().FirstOrDefault(x => x.ID == entity.ID);
            entity.CreatedDate = currentEntity.CreatedDate;
            //Nếu k cập nhật ảnh mới thì để nguyên ảnh cũ
            if (entity.Image == null)
                entity.Image = currentEntity.Image;
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }
        public bool UpdateOutstanding(long? id, bool isOutstanding)
        {
            if (id == null) return false;
            var _new = context.News.Find(id);
            if (_new == null) return false;
            _new.IsOutStanding = isOutstanding;
            context.SaveChanges();
            return true;
        }

        public void Delete(long id)
        {
            New _new = context.News.Find(id);
            Delete(_new);

        }

        public void Delete(New entity)
        {
            context.News.Remove(entity);
            context.SaveChanges();
        }

        public List<New> GetAll()
        {
            return context.News.OrderByDescending(x=>x.ModifiedDate).ToList();
        }
        
        public New GetSingleById(long? id)
        {
            return context.News.Find(id);
        }

        public List<New> GetLastestNews(int top = 5)
        {
            return context.News.OrderByDescending(x => x.CreatedDate).Take(top).ToList();
        }

        public List<New> GetFeaturedNews(int top = 5)
        {
            return context.News.Where(x=>x.IsOutStanding==true).OrderByDescending(x => x.CreatedDate).Take(top).ToList();
        }
        
        public void Dispose()
        {
            context.Dispose();
        }

    }
}
