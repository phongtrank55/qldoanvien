﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class UnitService : IService<Unit>
    {
        private YouthUnionUniversityDbContext context;

        public UnitService()
        {
            context = new YouthUnionUniversityDbContext();
        }
        public Unit Add(Unit entity)
        {
            var unit = context.Units.Add(entity);
            context.SaveChanges();
            return unit;
        }

        public void Update(Unit entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            Unit unit = context.Units.Find(id);
            Delete(unit);

        }

        public void Delete(Unit entity)
        {
            context.Units.Remove(entity);
            context.SaveChanges();
        }

        public List<Unit> GetAll()
        {
            return context.Units.ToList();
        }

        public Unit GetSingleById(long? id)
        {
            return context.Units.Find(id);
        }
        public Unit GetUnionUniversity()
        {
            var unitUniversity = context.Units.FirstOrDefault(m => m.Type == QLDoanTruong.Common.ConstraintUnitType.UNIVERSITY);
            if(unitUniversity==null)
            {
                //create
                return Add(new Unit()
                    {
                        UnitCode = "ĐT",
                        Name = "Đoàn trường",
                        Type = QLDoanTruong.Common.ConstraintUnitType.UNIVERSITY
                    });
                
            }
            return unitUniversity;

        }
        public void Dispose()
        {
            context.Dispose();
        }
    }

}
