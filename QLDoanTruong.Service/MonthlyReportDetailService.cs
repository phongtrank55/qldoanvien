﻿using QLDoanTruong.Model;
using QLDoanTruong.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class MonthlyReportDetailService : IService<MonthlyReportDetail>
    {
        private YouthUnionUniversityDbContext context;

        public MonthlyReportDetailService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public MonthlyReportDetail Add(MonthlyReportDetail entity)
        {
            //thay đổi cập nhật cho báo cáo này
            context.MonthlyReports.Find(entity.MonthReportID).ModifiedDate = DateTime.Now;

            entity.UpdatedDate = DateTime.Today;
            var monthlyReport = context.MonthlyReportDetails.Add(entity);
            context.SaveChanges();
            return monthlyReport;
        }

        public void Update(MonthlyReportDetail entity)
        {
            entity.UpdatedDate = DateTime.Today;
            if (entity.FileUploadProof == null)
            {
                var file = context.MonthlyReportDetails.FirstOrDefault(x => x.ActivityID == entity.ActivityID && x.MonthReportID == entity.MonthReportID).FileUploadProof;
                entity.FileUploadProof = file;
            }
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            MonthlyReportDetail monthlyReport = context.MonthlyReportDetails.Find(id);
            Delete(monthlyReport);

        }

        public void Delete(MonthlyReportDetail entity)
        {
            context.MonthlyReportDetails.Remove(entity);
            context.SaveChanges();
        }

        public List<MonthlyReportDetail> GetAll()
        {
            return context.MonthlyReportDetails.ToList();
        }

        public List<MonthlyReportDetail> GetAllByMonthlyReport(long? reportID)
        {
            if (reportID == null) return null;
            return context.MonthlyReportDetails.Where(x => x.MonthReportID == reportID).ToList();
        }

        public void SubmitPoint(long activityID, long reportID, float? point)
        {
            var detail = context.MonthlyReportDetails
                            .FirstOrDefault(x => x.MonthReportID == reportID && x.ActivityID == activityID);
            detail.Submited = true;
            detail.PointSubmit = point;
            //update đã cập nhật cho Báo cáo
            var report = context.MonthlyReports.Find(reportID);
            report.Submited = true;
            context.SaveChanges();
        }


        public MonthlyReportDetail GetSingleById(long? id)
        {
            return context.MonthlyReportDetails.Find(id);
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }

}
