﻿using System.Collections.Generic;
using System.Linq;
using QLDoanTruong.Model;
using System.Data.Entity;
using QLDoanTruong.Common;

namespace QLDoanTruong.Service
{
    public class FunctionService : IService<Function>
    {
        private YouthUnionUniversityDbContext context = new YouthUnionUniversityDbContext();


        public Function Add(Function entity)
        {
            var menu = context.Functions.Add(entity);
            context.SaveChanges();
            return menu;
        }
        public void Update(Function entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            Function menu = context.Functions.Find(id);
            Delete(menu);

        }

        public void Delete(Function entity)
        {
            context.Functions.Remove(entity);
            context.SaveChanges();
        }

        public List<Function> GetAll()
        {
            return context.Functions.ToList();
        }
        public List<Function> GetMenusByUserName(string username)
        {
            var groupID = context.Users.FirstOrDefault(x => x.Username == username).GroupUserID;
            return context.Credentials.Where(x => x.GroupUserID == groupID)
                            .Select(x => x.Role.Function)
                            .Distinct()
                            .OrderBy(x => x.Order)
                            .ToList();
            
        }
        public Function GetSingleById(long? id)
        {
            return context.Functions.Find(id);
        }

        public void Dispose()
        {
            context.Dispose();
        }

    }
}
