﻿using QLDoanTruong.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class SchoolYearService : IService<SchoolYear>
    {
        private YouthUnionUniversityDbContext context;

        public SchoolYearService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public SchoolYear Add(SchoolYear entity)
        {
            var schoolYear = context.SchoolYears.Add(entity);
            context.SaveChanges();
            return schoolYear;
        }

        public void Update(SchoolYear entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            SchoolYear schoolYear = context.SchoolYears.Find(id);
            Delete(schoolYear);

        }

        public void Delete(SchoolYear entity)
        {
            context.SchoolYears.Remove(entity);
            context.SaveChanges();
        }

        public List<SchoolYear> GetAll()
        {
            return context.SchoolYears.ToList();
        }

        public SchoolYear GetSingleById(long? id)
        {
            return context.SchoolYears.Find(id);
        }

        public SchoolYear GetCurrent()
        {
            var schoolYear = context.SchoolYears.FirstOrDefault(x=>x.StartDate<=DateTime.Today && x.EndDate >=DateTime.Today);
            //Xét chưa có thi tạo mới
            return schoolYear;
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }

}
