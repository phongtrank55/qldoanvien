﻿using QLDoanTruong.Model;
using QLDoanTruong.Common;
using QLDoanTruong.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QLDoanTruong.Service
{
    public class ActivityService : IService<Activity>
    {
        private YouthUnionUniversityDbContext context;

        public ActivityService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public Activity Add(Activity entity)
        {
            //Tìm mã
            //Mã là UnitCode_Year_Month_Day_Number
            string unitCode = context.Units.AsNoTracking().FirstOrDefault(x => x.ID == entity.UnitID).UnitCode;
            DateTime today = DateTime.Today;
            //lấy mã hoạt động cuối cùng đc thêm trong ngày
            var lastActivity = context.Activities.AsNoTracking()
                                .OrderByDescending(x => x.ID)
                                .FirstOrDefault(x => x.CreatedDate == today);
            var lastCode = lastActivity == null ? null : lastActivity.ActivityCode;
            int num = 1;
            if (lastCode != null)
            {
                num = int.Parse(lastCode.Split('_')[1]) + 1;

            }
            //add
            entity.ActivityCode =String.Format("{0}{1:00}{2:00}{3:00}_{4:00}", unitCode, today.Year, today.Month, today.Day, num);
            entity.CreatedDate = today;
            var activity = context.Activities.Add(entity);
            context.SaveChanges();
            return activity;
        }

        public void Update(Activity entity)
        {
            var currentActivity = context.Activities.AsNoTracking().FirstOrDefault(x => x.ID == entity.ID);
            if (entity.Document == null)
            {
                var file = currentActivity.Document;
                //entity.CreatedDate = previousEntity.CreatedDate;
                //Nếu k cập nhật ảnh mới thì để nguyên ảnh cũ

                entity.Document = file;
            }
            entity.CreatedDate = currentActivity.CreatedDate;
            entity.ModifiedDate = DateTime.Today;
            entity.ActivityCode = currentActivity.ActivityCode;

            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            Activity activity = context.Activities.Find(id);
            Delete(activity);

        }

        public void Delete(Activity entity)
        {
            context.Activities.Remove(entity);
            context.SaveChanges();
        }

        public List<Activity> GetAll()
        {
            return context.Activities.ToList();
        }

        public Activity GetSingleById(long? id)
        {
            return context.Activities.Find(id);
        }

        /// <summary>
        /// Tìm hoạt động mà chưa có trong báo cáo tháng này do khoa phụ trách hoặc do trường
        /// Trong khoảng thời gian hiện tại
        /// </summary>
        /// <param search="facultyId"></param>
        /// <returns></returns>
        public List<Activity> GetActivityToReportByFaculty(long currentReportID)
        {

            var currentReport = context.MonthlyReports.Find(currentReportID);
            //lấy ngày cuối cùng của tháng trước làm cận
            //var endDate = dateReportCurrent.AddDays(QLDoanTruong.Common.ConstraintName.LAST_DAY_TO_SEND_REPORT - dateReportCurrent.Day);
            //var startDate = endDate.AddMonths(-1);
            //Xét từ 1 đến 31
            var startDate = new DateTime(currentReport.Year, currentReport.Month, 1);
            var endDate = new DateTime(currentReport.Year, currentReport.Month, DateTime.DaysInMonth(currentReport.Year, currentReport.Month));

            var detailCurrentReport = context.TempMonthlyReportDetails.Where(x => x.MonthReportID == currentReportID);
            return context.Activities
                    .Where(x => (x.UnitID == currentReport.FacultyID || x.Unit.Type == QLDoanTruong.Common.ConstraintUnitType.UNIVERSITY)
                                && (x.EndDate >= startDate && x.EndDate <= endDate
                                                        || x.StartDate >= startDate && x.StartDate <= endDate)
                                        && !detailCurrentReport.Any(r => r.ActivityID == x.ID))
                   .OrderBy(x => x.StartDate).ToList();
        }


        /// <summary>
        /// Tìm các hoạt động trong khoảng thời gian
        /// </summary>
        /// <param search="startDate"></param>
        /// <param search="endDate"></param>
        /// <param search="unitID"></param>
        /// <returns></returns>
        public List<ActivityViewModel> GetViewInDateRangeByFacultyAndUniversity(DateTime startDate, DateTime endDate, int facultyID)
        {
            return context.Activities
                    .Where(x => (x.UnitID == facultyID || x.Unit.Type == QLDoanTruong.Common.ConstraintUnitType.UNIVERSITY)
                                && (x.EndDate >= startDate && x.EndDate <= endDate
                                                        || x.StartDate >= startDate && x.StartDate <= endDate))
                   .OrderBy(x => x.StartDate)
                   .Select(x => new ActivityViewModel()
                   {
                       UnitType = x.Unit.Type,
                       ID = x.ID,
                       Name = x.Name,
                       ActivityCode = x.ActivityCode,
                       Document = x.Document,
                       StartDate = x.StartDate,
                       EndDate = x.EndDate,
                       YouthUnionStaff = x.YouthUnionMemberStaff,
                       UnitName = x.Unit.Name,
                       Score = x.Score,
                       Content = x.Content,
                       IsEnd = DbFunctions.TruncateTime(x.EndDate) < DbFunctions.TruncateTime(DateTime.Now)
                   }).ToList();
        }



        public List<ActivityViewModel> GetViewInDateRange(DateTime startDate, DateTime endDate, int? unitID)
        {

            if (unitID == null)
            {
                return context.Activities
                    .Where(x => x.EndDate >= startDate && x.EndDate <= endDate
                        || x.StartDate >= startDate && x.StartDate <= endDate)
                    .OrderBy(x => x.StartDate)
                    .Select(x => new ActivityViewModel()
                    {
                        UnitType = x.Unit.Type,

                        ID = x.ID,
                        Name = x.Name,
                        ActivityCode = x.ActivityCode,
                        Document = x.Document,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        YouthUnionStaff = x.YouthUnionMemberStaff,
                        UnitName = x.Unit.Name,
                        Score = x.Score,
                        Content = x.Content,
                        IsEnd = DbFunctions.TruncateTime(x.EndDate) < DbFunctions.TruncateTime(DateTime.Now)
                        //IsEnd = x.EndDate < DateTime.Now
                    }).ToList();
            }
            return context.Activities
                    .Where(x => x.UnitID == unitID && (x.EndDate >= startDate && x.EndDate <= endDate
                                                        || x.StartDate >= startDate && x.StartDate <= endDate))
                   .OrderBy(x => x.StartDate)
                   .Select(x => new ActivityViewModel()
                   {
                       ID = x.ID,
                       UnitType = x.Unit.Type,

                       Name = x.Name,
                       ActivityCode = x.ActivityCode,
                       Document = x.Document,
                       StartDate = x.StartDate,
                       EndDate = x.EndDate,
                       YouthUnionStaff = x.YouthUnionMemberStaff,
                       UnitName = x.Unit.Name,
                       Score = x.Score,
                       Content = x.Content,
                       IsEnd = DbFunctions.TruncateTime(x.EndDate) < DbFunctions.TruncateTime(DateTime.Now)
                   }).ToList();
        }



        public ActivityViewModel GetSingleViewByID(int? id)
        {
            if (id == null) return null;
            return context.Activities
                    .Where(x => x.ID == id)
                   .Select(x => new ActivityViewModel
                   {
                       ID = x.ID,
                       Name = x.Name,
                       ActivityCode = x.ActivityCode,
                       UnitType = x.Unit.Type,

                       Document = x.Document,
                       StartDate = x.StartDate,
                       EndDate = x.EndDate,
                       YouthUnionStaff = x.YouthUnionMemberStaff,
                       UnitName = x.Unit.Name,
                       Score = x.Score,
                       Content = x.Content,
                       IsEnd = DbFunctions.TruncateTime(x.EndDate) < DbFunctions.TruncateTime(DateTime.Now)
                   }).FirstOrDefault();
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }

}
