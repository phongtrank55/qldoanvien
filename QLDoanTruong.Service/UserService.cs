﻿using System.Collections.Generic;
using System.Linq;
using QLDoanTruong.Model;
using System.Data.Entity;
using QLDoanTruong.Common;
using QLDoanTruong.Model.ViewModel;
using System;

namespace QLDoanTruong.Service
{
    public class UserService : IService<User>
    {
        private YouthUnionUniversityDbContext context = new YouthUnionUniversityDbContext();

        public User Add(User entity)
        {
            var u = context.Users.FirstOrDefault(x => x.Username == entity.Username);
            if (u != null)
            {
                context.Users.Remove(u);
            }

            entity.CreatedDate = DateTime.Now;
            entity.Status = true;
            entity.Password = MyEncryption.GetMd5Hash(entity.Username + entity.Password);
            var user = context.Users.Add(entity);

            context.SaveChanges();
            return user;
        }
        public void Update(User entity)
        {
            entity.ModifiedDate = DateTime.Now;
            entity.Password = MyEncryption.GetMd5Hash(entity.Username + entity.Password);
            var currentEntity = context.Users.AsNoTracking().FirstOrDefault(x => x.ID == entity.ID);
            entity.CreatedDate = currentEntity.CreatedDate;
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }
        public void UpdateToken(int userID, string token)
        {
            var user = context.Users.Find(userID);
            if (user != null)
            {
                user.TokenForgetPass = token;
                context.Entry(user).State = EntityState.Modified;

                context.SaveChanges();
            }
        }

        public User GetSingleByToken(string token)
        {
            return context.Users.FirstOrDefault(u => u.TokenForgetPass == token);
        }
        public bool ExistedToken(string token)
        {
            return GetSingleByToken(token) != null;
        }
        public bool ExistedUsername(string username)
        {
            return context.Users.FirstOrDefault(x=>x.Username== username) != null;
        }

        public void Delete(long id)
        {
            User user = context.Users.Find(id);
            Delete(user);

        }

        public void Delete(User entity)
        {
            new YouthUnionMemberService().Delete(entity.ID);
            //context.Users.Remove(entity);
            //context.SaveChanges();
        }

        public List<User> GetAll()
        {
            return context.Users.ToList();
        }

        public List<AccountViewModel> GetViewByGroup(int? groupID, string txtSearch)
        {
            if (txtSearch == null) txtSearch = string.Empty;
            
            IQueryable<User> result = null;

            if (groupID != null)
            {
                result = context.Users.Where(x => x.GroupUserID == groupID);
            }
            if (result == null)
                result = context.Users
                        .Where(x => x.Username.Contains(txtSearch) || (x.YouthUnionMember.Lastname + " " + x.YouthUnionMember.Firstname).Contains(txtSearch));
            else
                result = result.Where(x => x.Username.Contains(txtSearch) || (x.YouthUnionMember.Lastname + " " + x.YouthUnionMember.Firstname).Contains(txtSearch));
            
            var r = result.Select(x => new AccountViewModel()
            {
                ID = x.ID,
                Username = x.Username,
                Email = x.YouthUnionMember.Email,
                DateOfBirth = x.YouthUnionMember.DateOfBirth,
                Lastname = x.YouthUnionMember.Lastname,
                Firstname = x.YouthUnionMember.Firstname,
                Gender = x.YouthUnionMember.Gender,
                Status = x.Status,
                GroupUserID=x.GroupUserID,
                GroupName = x.GroupUser.Name
            }).OrderBy(x => x.Firstname).ToList();
            return r;
        }

        public User GetSingleById(long? id)
        {
            return context.Users.Find(id);
        }
        public AccountViewModel GetSingleViewById(long? id)
        {
            var user = context.Users.Find(id);
            if (user == null) return null;
            return new AccountViewModel()
            {
                Username = user.Username,
                GroupUserID = user.GroupUserID,
                ID = user.YouthUnionMember.ID,
                YouthUnionClassID = user.YouthUnionMember.YouthUnionClassID,
                YouthUnionMemberCode = user.YouthUnionMember.YouthUnionMemberCode,
                Lastname = user.YouthUnionMember.Lastname,
                Firstname = user.YouthUnionMember.Firstname,
                Gender = user.YouthUnionMember.Gender,
                DateOfBirth = user.YouthUnionMember.DateOfBirth,
                Email = user.YouthUnionMember.Email,
                FacultyID = user.YouthUnionMember.YouthUnionClass.FacultyID,
                Type = user.YouthUnionMember.Type

            };

        }

        public User GetByLogin(string username, string password)
        {
            string passHash = MyEncryption.GetMd5Hash(username + password);
            var user = context.Users.FirstOrDefault(x => x.Username == username && x.Password == passHash);
            return user;
        }

        public List<string> GetPermissionByUsername(string username)
        {
            var groupID = context.Users.FirstOrDefault(x => x.Username == username).GroupUserID;
            return context.Credentials.Where(x => x.GroupUserID == groupID).Select(x => x.RoleID).ToList();
        }

        public long GetIDByUsername(string username)
        {
            return context.Users.FirstOrDefault(x => x.Username == username).ID;
        }

        public Unit GetFacultyByUsername(string username)
        {
            var mem = context.Users.FirstOrDefault(x => x.Username == username).YouthUnionMember;
            if(mem.YouthUnionClassID==null) return null;
            return mem.YouthUnionClass.Unit;
        }
        public YouthUnionClass GetClassByUsername(string username)
        {
            var mem = context.Users.FirstOrDefault(x => x.Username == username).YouthUnionMember;

            return mem.YouthUnionClass ;
        }
        public void UpdateEmailByUsername(string username, string email)
        {
            var mem = context.Users.FirstOrDefault(x => x.Username == username).YouthUnionMember;
            mem.Email = email;
            //context.Entry(mem).State = EntityState.Modified;

            context.SaveChanges();
            
        }
        public string GetAvatarByUsername(string username)
        {
            return context.Users.FirstOrDefault(x => x.Username == username).YouthUnionMember.Photo;
        }
        public int GetGroupUserByUsername(string username)
        {
            return context.Users.FirstOrDefault(x => x.Username == username).GroupUserID;
        }
        public User GetSingleByEmail(string email)
        {
            return context.Users.FirstOrDefault(x => x.YouthUnionMember.Email == email);
        }
        
        public void Dispose()
        {
            context.Dispose();
        }


    }
}
