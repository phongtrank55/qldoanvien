﻿using QLDoanTruong.Model;
using QLDoanTruong.Model.ViewModel;
using QLDoanTruong.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace QLDoanTruong.Service
{
    public class MonthlyReportService : IService<MonthlyReport>
    {
        private YouthUnionUniversityDbContext context;

        public MonthlyReportService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public MonthlyReport Add(MonthlyReport entity)
        {
            ////tìm tháng để thêm báo cáo
            //DateTime now = DateTime.Today.Day <= ConstraintName.LAST_DAY_TO_SEND_REPORT ? DateTime.Today : DateTime.Today.AddMonths(1);
            //entity.Month = now.Month;
            //entity.Year = now.Year;
            entity.CreatedDate = DateTime.Today;
            entity.ModifiedDate = DateTime.Today;
            entity.MonthlyReportCode = string.Format("BC_{0}_{1:00}_{2:0000}", context.Units.Find(entity.FacultyID).UnitCode, entity.Month, entity.Year);
            var monthlyReport = context.MonthlyReports.Add(entity);


            context.SaveChanges();
            //new TempMonthlyReportDetailService().DeleteByFaculty(monthlyReport.FacultyID);
            return monthlyReport;
        }

        public void Update(MonthlyReport entity)
        {
            var currentCreatetedDate = context.MonthlyReports.Find(entity.ID).CreatedDate;
            entity.CreatedDate = currentCreatetedDate;
            entity.ModifiedDate = DateTime.Today;
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            MonthlyReport monthlyReport = context.MonthlyReports.Find(id);
            Delete(monthlyReport);

        }

        public void Delete(MonthlyReport entity)
        {
            context.MonthlyReports.Remove(entity);
            context.SaveChanges();
        }

        public List<MonthlyReport> GetAll()
        {
            return context.MonthlyReports.ToList();
        }

        //
        public MonthlyReport GetSingleByFacultyAndMonth(int facultyID, int month, int year)
        {
            return context.MonthlyReports.FirstOrDefault(x => x.FacultyID == facultyID && x.Month == month && x.Year == x.Year);
        }

        public List<MonthlyReportViewModel> GetSentViewByMonth(int month, int year)
        {
            MonthlyReportExpireDate expireDate = null;
            using (var expireService = new MonthlyReportExpireDateService())
            {
                expireDate = expireService.GetSingleByMonthAndYear(month, year);
            }
            Expression<Func<MonthlyReport, bool>> condition = null;
            if (expireDate.IsEnabledCutOffDate)
            {
                condition = x => x.Year == year && x.Month == month && x.SentDate != null && x.SentDate <= expireDate.CutOffDate;
            }
            else
            {
                condition = x => x.Year == year && x.Month == month && x.SentDate != null && x.SentDate <= expireDate.DueDate;
            }

            var data = context.MonthlyReports
                 .Where(condition)
                 .Select(x => new
             {

                 MonthlyReportCode = x.MonthlyReportCode,
                 ID = x.ID,
                 FacultyID = x.FacultyID,
                 FacultyName = x.Unit.Name,
                 Submited = x.Submited,
                 Month = x.Month,
                 Year = x.Year,
                 SentDate = x.SentDate,
                 PointSelfSum = context.MonthlyReportDetails.Where(m => m.MonthReportID == x.ID).Sum(m => m.PointSelf),
                 PointSubmitSum = context.MonthlyReportDetails.Where(m => m.MonthReportID == x.ID).Sum(m => m.PointSubmit) + x.OtherPoint,
                 Rank = x.Rank
             }).ToList();

            var result = data.Select(x => new MonthlyReportViewModel()
            {
                MonthlyReportCode = x.MonthlyReportCode,
                ID = x.ID,
                Month = x.Month,
                Year = x.Year,
                FacultyID = x.FacultyID,
                FacultyName = x.FacultyName,
                Submited = x.Submited,
                ExpireDayAmount = x.SentDate.Value - expireDate.DueDate,
                DeducatePoint = x.SentDate.Value <= expireDate.DueDate ? 0 : (expireDate.IsEnabledDeducateEveryExpireDay && (x.SentDate.Value - expireDate.DueDate).TotalMinutes > 0 ? ((x.SentDate.Value - expireDate.DueDate).Days + 1) * expireDate.DeducatePoint : expireDate.DeducatePoint),
                PointSubmitSum = x.PointSubmitSum,
                PointSelfSum = x.PointSelfSum,
                SentDate = x.SentDate,
                Rank = x.Rank

            }).OrderByDescending(x => x.ID).ToList();


            ////So sánh 
            foreach (var item in result)
            {
                if (item.Submited)
                {
                    //so sánh tháng trước
                    //tìm tháng trước
                    int preMonth = item.Month, preYear = item.Year;
                    if (item.Month == 1)
                    {
                        preMonth = 12; preYear--;
                    }
                    else
                    {
                        preMonth--;
                    }
                    item.ComparePreviousMonth = item.PointSum - GetSumPointFacultyAndMonth(item.FacultyID, preMonth, preYear);
                }
            }
            return result;
        }

        /// <summary>
        /// Hiển thị ngoài trang chủ
        /// </summary>
        /// <param name="id"></param>
        public List<MonthlyReportViewModel> GetDisplayViewAllFaculty(int month, int year)
        {
            var unitSent = GetSentViewByMonth(month, year).OrderByDescending(x => x.Rank);
            var unitIDSent = unitSent.Select(y => y.FacultyID);
            //Tìm các khoa chưa có trong danh sách
            var unitNotSent = context.Units.Where(x => x.Type != QLDoanTruong.Common.ConstraintUnitType.UNIVERSITY
                                    && !unitIDSent.Contains(x.ID)).ToList();


            var result = new Stack<MonthlyReportViewModel>();
            //Chưa gửi thì thêm tên khoa

            foreach (var faculty in unitNotSent)
            {
                result.Push(new MonthlyReportViewModel()
                    {
                        Rank = null,
                        PointSubmitSum = null,
                        FacultyName = faculty.Name
                    });
            }

            foreach (var faculty in unitSent)
            {
                result.Push(faculty);
            }

            return result.ToList();
        }

        public void UpdateRank(int month, int year)
        {

            //tìm báo cáo đã xác nhận trong tháng
            var reportInThisMonth = context.MonthlyReports.Where(x => x.Year == year && x.Month == month);
            //Tính tổng điểm cuẢ các báo cáo ấy
            if (reportInThisMonth.Count() > 0)
            {
                //Điều kiện xét xếp hạng
                MonthlyReportExpireDate expireDate = null;
                using (var expireService = new MonthlyReportExpireDateService())
                {
                    expireDate = expireService.GetSingleByMonthAndYear(month, year);
                }
                //điều kiện lấy báo cáo
                Expression<Func<MonthlyReport, bool>> condition = null;
                //var sumPointSubmits = new List<dynamic>();
                if (expireDate.IsEnabledCutOffDate)
                {
                    condition = x => x.Submited && x.SentDate <= expireDate.CutOffDate;
                }
                else
                {
                    condition = x => x.Submited && x.SentDate <= expireDate.DueDate;
                }


                /////lấy tổng điểm của các báo cáo
                //var sumPointSubmits = reportInThisMonth
                //    .Where(condition)
                //    .Select(x => new
                //{
                //    ReportID = x.ID,
                //    PointSum = x.OtherPoint
                //                + x.MonthlyReportDetails.Sum(m => m.PointSubmit)
                //                - (x.SentDate.Value <= expireDate.DueDate ? 0 : (expireDate.IsEnabledDeducateEveryExpireDay && (x.SentDate.Value - expireDate.DueDate).TotalMinutes > 0 ? ((x.SentDate.Value - expireDate.DueDate).Days + 1) * expireDate.DeducatePoint : expireDate.DeducatePoint))
                //});

                ///lấy tổng điểm của các báo cáo
                  var sumPointSubmits = reportInThisMonth
                    .Where(condition).ToList()
                    .Select(x => new
                    {   
                        ReportID = x.ID,
                        PointSum = x.OtherPoint
                                   + x.MonthlyReportDetails.Sum(m => m.PointSubmit)
                                   - (x.SentDate.Value <= expireDate.DueDate ? 0 : ((expireDate.IsEnabledDeducateEveryExpireDay && (x.SentDate.Value - expireDate.DueDate).TotalMinutes > 0) ? ((x.SentDate.Value - expireDate.DueDate).Days + 1) * expireDate.DeducatePoint : expireDate.DeducatePoint))
                    });

                //các con điểm không trùng
                var uniquePoints = sumPointSubmits.Select(x => x.PointSum).Distinct();

                //Tính rank

                foreach (var rep in reportInThisMonth)
                {
                    //báo cáo đc xét xếp hạng
                    var selectedReport = sumPointSubmits.FirstOrDefault(x => x.ReportID == rep.ID);
                    if (selectedReport != null)
                    {
                        rep.Rank = uniquePoints.Count(x => x > selectedReport.PointSum) + 1;
                        ////tính điểm trừ
                        //var expireDayAmount = rep.SentDate.Value - expireDate.DueDate;
                        //float deducatePoint = 0;
                        //if (expireDayAmount.TotalMinutes > 0)
                        //    deducatePoint = expireDate.IsEnabledDeducateEveryExpireDay ? (expireDayAmount.Days + 1) * expireDate.DeducatePoint : expireDate.DeducatePoint;

                        //var sumPointSubmitOfRep = rep.MonthlyReportDetails.Sum(x => x.PointSubmit) + rep.OtherPoint;
                        //rep.Rank = reportInThisMonth.Count(x => x.MonthlyReportDetails.Sum(m => m.PointSubmit) > sumPointSubmit) + 1;
                        //rep.Rank = sumPointSubmits.Count(x => x > sumPointSubmitOfRep) + 1;
                    }
                    else
                    {
                        rep.Rank = null;
                    }
                }
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Lấy báo cáo theo tháng theo đoàn viện
        /// </summary>
        /// <param search="facultyId"></param>
        /// <returns></returns>
        public List<MonthlyReportViewModel> GetReportSentViewByFaculty(int facultyId)
        {

            var data = context.MonthlyReports
              .Where(x => x.FacultyID == facultyId && x.SentDate != null)
              .Select(x => new
              {
                  MonthlyReportCode = x.MonthlyReportCode,
                  ID = x.ID,
                  Month = x.Month,
                  Year = x.Year,
                  FacultyID = x.FacultyID,
                  FacultyName = x.Unit.Name,
                  Submited = x.Submited,
                  SentDate = x.SentDate,
                  PointSelfSum = context.MonthlyReportDetails.Where(m => m.MonthReportID == x.ID).Sum(m => m.PointSelf),
                  PointSubmitSum = context.MonthlyReportDetails.Where(m => m.MonthReportID == x.ID).Sum(m => m.PointSubmit) + x.OtherPoint,
                  Rank = x.Rank
              }).OrderByDescending(x => new { x.Year, x.Month }).ToList();

            var result = new List<MonthlyReportViewModel>();
            if (data.Count > 0)
            {
                foreach (var x in data)
                {
                    //Các tổng điểm theo từng tháng
                    //    var points = data.Where(x => x.Submited).Select(x => x.PointSum).Distinct(); //các điểm trong tháng

                    //ở đây mỗi tháng 1 báo cáo nen expire k trùng
                    MonthlyReportExpireDate expireDate = null;
                    using (var expireService = new MonthlyReportExpireDateService())
                    {
                        expireDate = expireService.GetSingleByMonthAndYear(x.Month, x.Year);
                    }
                    //nếu quá hạn thì không lấy
                    if ((expireDate.IsEnabledCutOffDate && x.SentDate > expireDate.CutOffDate) || (!expireDate.IsEnabledCutOffDate && x.SentDate > expireDate.DueDate))
                    {
                        continue;
                    }

                    var item = new MonthlyReportViewModel()
                    {
                        Month = x.Month,
                        Year = x.Year,
                        ID = x.ID,
                        MonthlyReportCode = x.MonthlyReportCode,
                        FacultyID = x.FacultyID,
                        FacultyName = x.FacultyName,
                        Submited = x.Submited,
                        ExpireDayAmount = x.SentDate.Value - expireDate.DueDate,
                        DeducatePoint = x.SentDate.Value <= expireDate.DueDate ? 0 : (expireDate.IsEnabledDeducateEveryExpireDay && (x.SentDate.Value - expireDate.DueDate).TotalMinutes > 0 ? ((x.SentDate.Value - expireDate.DueDate).Days + 1) * expireDate.DeducatePoint : expireDate.DeducatePoint),
                        PointSubmitSum = x.PointSubmitSum,
                        PointSelfSum = x.PointSelfSum,
                        SentDate = x.SentDate,
                        Rank = x.Rank
                    };

                    ////So sánh
                    if (item.Submited)
                    {//
                        //tìm tháng trước
                        int preMonth = item.Month, preYear = item.Year;
                        if (item.Month == 1)
                        {
                            preMonth = 12; preYear--;
                        }
                        else
                        {
                            preMonth--;
                        }
                        item.ComparePreviousMonth = item.PointSum - GetSumPointFacultyAndMonth(item.FacultyID, preMonth, preYear);

                    }
                    result.Add(item);
                }
            }
            return result;
        }

        public List<MonthlyReportViewModel> GetComposerReportViewByFaculty(long facultyId)
        {
            var result = context.MonthlyReports
              .Where(x => x.FacultyID == facultyId && x.IsComposing == true)
              .OrderByDescending(x => new { x.Year, x.Month })
              .Select(x => new
              {
                  MonthlyReportCode = x.MonthlyReportCode,
                  ID = x.ID,
                  FacultyID = facultyId,
                  Month = x.Month,
                  Year = x.Year,
                  PointSelfSum = context.TempMonthlyReportDetails.Where(m => m.MonthReportID == x.ID).Sum(m => m.PointSelf),
              })
            .Select(x => new MonthlyReportViewModel()
            {
                ID = x.ID,
                Month = x.Month,
                Year = x.Year,
                MonthlyReportCode = x.MonthlyReportCode,
                PointSelfSum = x.PointSelfSum,

            });
            return result.ToList();
        }
        /// <summary>
        /// lấy điểm tổng của đơn vị đã đc submit
        ///
        /// </summary>
        /// <param search="month"></param>
        /// <param search="year"></param>
        /// <returns></returns>
        public float GetSumPointFacultyAndMonth(int facultyID, int month, int year)
        {
            MonthlyReportExpireDate expireDate = null;
            using (var expireService = new MonthlyReportExpireDateService())
            {
                expireDate = expireService.GetSingleByMonthAndYear(month, year);
            }
            Expression<Func<MonthlyReport, bool>> condition = null;
            if (expireDate.IsEnabledCutOffDate)
            {
                condition = x => x.FacultyID == facultyID && x.Year == year && x.Month == month && x.Submited && x.SentDate <= expireDate.CutOffDate;
            }
            else
            {
                condition = x => x.FacultyID == facultyID && x.Year == year && x.Month == month && x.Submited && x.SentDate <= expireDate.DueDate;
            }
            var data = context.MonthlyReports
                .Where(condition)
                .Select(x => new
                {
                    SentDate = x.SentDate,
                    PointSubmitSum = context.MonthlyReportDetails.Where(m => m.MonthReportID == x.ID).Sum(m => m.PointSubmit) + x.OtherPoint,

                }).FirstOrDefault();

            if (data == null)
                return 0;
            //tính điểm trừ
            var expireDayAmount = data.SentDate.Value - expireDate.DueDate;
            float deducatePoint = 0;
            if (expireDayAmount.TotalMinutes > 0)
                deducatePoint = expireDate.IsEnabledDeducateEveryExpireDay ? (expireDayAmount.Days + 1) * expireDate.DeducatePoint : expireDate.DeducatePoint;

            return data.PointSubmitSum.Value - deducatePoint;
        }

        public MonthlyReport GetSingleById(long? id)
        {
            return context.MonthlyReports.Find(id);
        }



        public void Dispose()
        {
            context.Dispose();
        }
    }

}
