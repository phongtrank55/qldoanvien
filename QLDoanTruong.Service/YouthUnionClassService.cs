﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using QLDoanTruong.Model;
using QLDoanTruong.Model.ViewModel;
using QLDoanTruong.Common;

namespace QLDoanTruong.Service
{
    public class YouthUnionClassService : IService<YouthUnionClass>
    {
        private YouthUnionUniversityDbContext context;

        public YouthUnionClassService()
        {
            context = new YouthUnionUniversityDbContext();
        }


        public YouthUnionClass Add(YouthUnionClass entity)
        {

            var item = context.YouthUnionClasses.Add(entity);
            context.SaveChanges();
            return item;
        }

        public void Update(YouthUnionClass entity)
        {
            var currentEntity = context.YouthUnionClasses.AsNoTracking().FirstOrDefault(x => x.ID == entity.ID);
            entity.Type = currentEntity.Type;
            //Chi đoàn cán bộ không đc sửa khoa
            if (entity.Type == QLDoanTruong.Common.ConstraintUnionClassType.LECTURE)
            {
                entity.FacultyID = currentEntity.FacultyID;
            }
            //Nếu đổi khoa chi đoàn sv thì sẽ xóa ban chấp hành trong ban hiện tại
            if (entity.FacultyID != currentEntity.FacultyID)
            {
                var student = context.YouthUnionMembers.Where(x => x.YouthUnionClassID == entity.ID).Select(x => x.ID);
                if (student != null)
                {
                    foreach (var id in student)
                        context.LeaderUnits.RemoveRange(context.LeaderUnits
                                            .Where(x => x.UnitID == entity.FacultyID && x.YouthUnionMemberID == id));
                }
            }

            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void Delete(long id)
        {
            YouthUnionClass Class = context.YouthUnionClasses.Find(id);
            Delete(Class);

        }

        public void Delete(YouthUnionClass entity)
        {
            //Xóa sv của lớp

            var mem = context.YouthUnionMembers.Where(x => x.YouthUnionClassID == entity.ID).Select(x => x.ID);

            using (var memberService = new YouthUnionMemberService())
            {
                foreach (var memID in mem)
                {
                    memberService.Delete(memID);
                }
            }
            //Xóa khen thưởng
            context.Achievements.RemoveRange(
                    context.Achievements
                        .Where(x => x.YouthUnionID == entity.ID
                                    && x.Type == ConstraintUnionType.CLASS));
            try
            {
                context.YouthUnionClasses.Remove(entity);

                context.SaveChanges();
            }
            catch
            {
            }

        }

        public List<YouthUnionClass> GetAll()
        {
            return context.YouthUnionClasses.ToList();
        }

        public List<YouthUnionClassViewModel> GetAllView()
        {
            var currentSchoolYear = new SchoolYearService().GetCurrent();
            var currentSchoolYearID = currentSchoolYear == null ? 0 : currentSchoolYear.ID;

            return context.YouthUnionClasses
                .Select(x => new
                {
                    ID = x.ID,
                    FacultyID = x.FacultyID,
                    Name = x.Name,
                    Type = x.Type,
                    YouthUnionClassCode = x.YouthUnionClassCode,
                    FacultyName = x.Unit.Name,
                    UnionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID),
                    MaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Gender == 1),
                    FemaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Gender == 0),
                    ReligionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Religion.ID != 1),
                    EthnicAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Race.ID != 1),
                    //SecretaryName = x.Secretary.Lastname + " " + x.Secretary.Firstname
                    Secretaries = x.LeaderClasses.Where(m => m.Position.Level == 1
                                                         && m.Position.Type == QLDoanTruong.Common.ConstraintPositionType.UNION
                                                         && m.SchoolYearID == currentSchoolYearID)
                                                                         .Select(m => m.YouthUnionMember.Lastname + " " + m.YouthUnionMember.Firstname)
                }).ToList()
                .Select(x => new YouthUnionClassViewModel()
                {
                    FacultyID = x.FacultyID,
                    ID = x.ID,
                    Name = x.Name,
                    Type = x.Type,
                    YouthUnionClassCode = x.YouthUnionClassCode,
                    FacultyName = x.FacultyName,
                    UnionAmount = x.UnionAmount,
                    MaleAmount = x.MaleAmount,
                    FemaleAmount = x.FemaleAmount,
                    ReligionAmount = x.ReligionAmount,
                    EthnicAmount = x.EthnicAmount,
                    //SecretaryName = x.Secretary.Lastname + " " + x.Secretary.Firstname
                    SecretaryName = string.Join(", ", x.Secretaries)
                }).ToList();
        }

        public YouthUnionClass GetSingleByName(string name)
        {
            return context.YouthUnionClasses.FirstOrDefault(x => x.Name == name);

        }

        public YouthUnionClass GetSingleById(long? id)
        {

            return context.YouthUnionClasses.Find(id);
        }
        public YouthUnionClassViewModel GetSingleViewById(long? id)
        {

            if (id == null) return null;
            var currentSchoolYear = new SchoolYearService().GetCurrent();
            var currentSchoolYearID = currentSchoolYear == null ? 0 : currentSchoolYear.ID;

            return context.YouthUnionClasses
                .Where(x => x.ID == id)
                .Select(x => new
                {
                    ID = x.ID,
                    FacultyID = x.FacultyID,
                    Name = x.Name,
                    Type = x.Type,
                    YouthUnionClassCode = x.YouthUnionClassCode,
                    FacultyName = x.Unit.Name,
                    UnionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID),
                    MaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Gender == 1),
                    FemaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Gender == 0),
                    ReligionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Religion.ID != 1),
                    EthnicAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Race.ID != 1),
                    //SecretaryName = x.Secretary.Lastname + " " + x.Secretary.Firstname
                    Secretaries = x.LeaderClasses.Where(m => m.Position.Level == 1
                                                         && m.Position.Type == QLDoanTruong.Common.ConstraintPositionType.UNION
                                                         && m.SchoolYearID == currentSchoolYearID)
                                                                         .Select(m => m.YouthUnionMember.Lastname + " " + m.YouthUnionMember.Firstname)
                }).ToList().Select(x => new YouthUnionClassViewModel()
                {
                    FacultyID = x.FacultyID,
                    ID = x.ID,
                    Name = x.Name,
                    Type = x.Type,
                    YouthUnionClassCode = x.YouthUnionClassCode,
                    FacultyName = x.FacultyName,
                    UnionAmount = x.UnionAmount,
                    MaleAmount = x.MaleAmount,
                    FemaleAmount = x.FemaleAmount,
                    ReligionAmount = x.ReligionAmount,
                    EthnicAmount = x.EthnicAmount,
                    //SecretaryName = x.Secretary.Lastname + " " + x.Secretary.Firstname
                    SecretaryName = string.Join(", ", x.Secretaries)
                })
                .FirstOrDefault();
        }

        public bool ExistedClassCode(string classCode, long? classID = null)
        {
            if (classID == null)
            {
                return context.YouthUnionClasses.FirstOrDefault(x => x.YouthUnionClassCode == classCode) != null;
            }
            //Nếu trùng nhưng k phải lớp đang xét
            return context.YouthUnionClasses.FirstOrDefault(x => x.YouthUnionClassCode == classCode && x.ID != classID) != null;
        }

        public List<YouthUnionClass> GetByFaculty(long? facultyID)
        {
            if (facultyID == null) return GetAll();
            return context.YouthUnionClasses.Where(x => x.FacultyID == facultyID).ToList();
        }

        public List<YouthUnionClass> GetByAcademicYear(long? academicYearID)
        {
            if (academicYearID == null) return GetAll();
            return context.YouthUnionClasses.Where(x => x.AcademicYearID == academicYearID).ToList();
        }

        /// <summary>
        /// Lấy chi đoàn theo khóa
        /// </summary>
        /// <param search="facultyID"></param>
        /// <param search="academicYearID"></param>
        /// <returns></returns>
        public List<YouthUnionClass> GetByFacultyAndAcademicYear(long? facultyID, long? academicYearID, int? classType = null)
        {
            List<YouthUnionClass> result;
            if (facultyID == null && academicYearID == null)
                result = GetAll();
            else if (facultyID == null)
                result = context.YouthUnionClasses.Where(x => x.AcademicYearID == academicYearID).ToList();
            else if (academicYearID == null)
                result = context.YouthUnionClasses.Where(x => x.FacultyID == facultyID).ToList();
            else result = context.YouthUnionClasses.Where(x => x.FacultyID == facultyID && x.AcademicYearID == academicYearID).ToList();
            if (classType != null)
                return result.Where(x => x.Type == classType).ToList();
            return result.OrderBy(x => x.Type).ToList();
        }
        /// <summary>
        /// lấ theo Loại chi đoàn: CB/SV
        /// </summary>
        /// <param search="facultyID"></param>
        /// <param search="typeClass"></param>
        /// <returns></returns>
        public List<YouthUnionClass> GetByFacultyAndType(long facultyID, int typeClass)
        {
            return context.YouthUnionClasses.Where(x => x.FacultyID == facultyID && x.Type == typeClass).ToList();
        }
        public List<YouthUnionClassViewModel> GetViewByFaculty(long? facultyID)
        {
            if (facultyID == null) return GetAllView();
            var currentSchoolYear = new SchoolYearService().GetCurrent();
            var currentSchoolYearID = currentSchoolYear == null ? 0 : currentSchoolYear.ID;
            return context.YouthUnionClasses
                .Where(x => x.FacultyID == facultyID)
                .Select(x => new
                {
                    FacultyID = x.FacultyID,
                    ID = x.ID,
                    Name = x.Name,
                    Type = x.Type,
                    YouthUnionClassCode = x.YouthUnionClassCode,
                    FacultyName = x.Unit.Name,
                    UnionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID),
                    MaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Gender == 1),
                    FemaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Gender == 0),
                    ReligionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Religion.ID != 1),
                    EthnicAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Race.ID != 1),
                    //SecretaryName = x.Secretary.Lastname + " " + x.Secretary.Firstname
                    Secretaries = x.LeaderClasses.Where(m => m.Position.Level == 1
                                                         && m.Position.Type == QLDoanTruong.Common.ConstraintPositionType.UNION
                                                         && m.SchoolYearID == currentSchoolYearID)
                                                                         .Select(m => m.YouthUnionMember.Lastname + " " + m.YouthUnionMember.Firstname)
                }).ToList().Select(x => new YouthUnionClassViewModel()
                {
                    ID = x.ID,
                    FacultyID = x.FacultyID,
                    Name = x.Name,
                    Type = x.Type,
                    YouthUnionClassCode = x.YouthUnionClassCode,
                    FacultyName = x.FacultyName,
                    UnionAmount = x.UnionAmount,
                    MaleAmount = x.MaleAmount,
                    FemaleAmount = x.FemaleAmount,
                    ReligionAmount = x.ReligionAmount,
                    EthnicAmount = x.EthnicAmount,
                    //SecretaryName = x.Secretary.Lastname + " " + x.Secretary.Firstname
                    SecretaryName = string.Join(", ", x.Secretaries)
                }).ToList();
        }

        public List<YouthUnionClassViewModel> GetViewAcademicYear(long? academicYearID)
        {

            if (academicYearID == null) return GetAllView();
            var currentSchoolYear = new SchoolYearService().GetCurrent();
            var currentSchoolYearID = currentSchoolYear == null ? 0 : currentSchoolYear.ID;

            return context.YouthUnionClasses
                .Where(x => x.AcademicYearID == academicYearID)
                .Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name,
                    Type = x.Type,
                    YouthUnionClassCode = x.YouthUnionClassCode,
                    FacultyName = x.Unit.Name,
                    FacultyID = x.FacultyID,
                    UnionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID),
                    MaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Gender == 1),
                    FemaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Gender == 0),
                    ReligionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Religion.ID != 1),
                    EthnicAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Race.ID != 1),
                    //SecretaryName = x.Secretary.Lastname + " " + x.Secretary.Firstname
                    Secretaries = x.LeaderClasses.Where(m => m.Position.Level == 1
                                                         && m.Position.Type == QLDoanTruong.Common.ConstraintPositionType.UNION
                                                            && m.SchoolYearID == currentSchoolYearID)
                                                    .Select(m => m.YouthUnionMember.Lastname + " " + m.YouthUnionMember.Firstname)
                }).ToList().Select(x => new YouthUnionClassViewModel()
                {
                    FacultyID = x.FacultyID,
                    ID = x.ID,
                    Name = x.Name,
                    Type = x.Type,
                    YouthUnionClassCode = x.YouthUnionClassCode,
                    FacultyName = x.FacultyName,
                    UnionAmount = x.UnionAmount,
                    MaleAmount = x.MaleAmount,
                    FemaleAmount = x.FemaleAmount,
                    ReligionAmount = x.ReligionAmount,
                    EthnicAmount = x.EthnicAmount,
                    //SecretaryName = x.Secretary.Lastname + " " + x.Secretary.Firstname
                    SecretaryName = string.Join(", ", x.Secretaries)
                }).ToList();
        }


        public List<YouthUnionClassViewModel> GetViewByFacultyAndAcademicYear(long? facultyID, long? academicYearID, long? type = null)
        {
            var currentSchoolYear = new SchoolYearService().GetCurrent();
            List<YouthUnionClassViewModel> result = null;
            if (facultyID == null && academicYearID == null)
            {
                result = GetAllView();
            }
            else if (facultyID == null)
            {
                result = GetViewAcademicYear(academicYearID);
            }
            else if (academicYearID == null)
            {
                result = GetViewByFaculty(facultyID);
            }

            else
            {
                result = context.YouthUnionClasses
                .Where(x => x.FacultyID == facultyID && x.AcademicYearID == academicYearID)
                .Select(x => new
                {
                    ID = x.ID,

                    FacultyID = x.FacultyID,
                    Name = x.Name,
                    Type = x.Type,
                    YouthUnionClassCode = x.YouthUnionClassCode,
                    FacultyName = x.Unit.Name,
                    UnionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID),
                    MaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Gender == 1),
                    FemaleAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Gender == 0),
                    ReligionAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Religion.ID != 1),
                    EthnicAmount = context.YouthUnionMembers.Count(m => m.YouthUnionClass.ID == x.ID && m.Race.ID != 1),
                    Secretaries = x.LeaderClasses.Where(m => m.Position.Level == 1
                                                         && m.Position.Type == QLDoanTruong.Common.ConstraintPositionType.UNION
                                                                          && m.SchoolYearID == currentSchoolYear.ID)
                                                                .Select(m => m.YouthUnionMember.Lastname + " " + m.YouthUnionMember.Firstname)
                }).ToList().Select(x => new YouthUnionClassViewModel()
                {
                    ID = x.ID,
                    Name = x.Name,
                    Type = x.Type,
                    FacultyID = x.FacultyID,
                    YouthUnionClassCode = x.YouthUnionClassCode,
                    FacultyName = x.FacultyName,
                    UnionAmount = x.UnionAmount,
                    MaleAmount = x.MaleAmount,
                    FemaleAmount = x.FemaleAmount,
                    ReligionAmount = x.ReligionAmount,
                    EthnicAmount = x.EthnicAmount,
                    //SecretaryName = x.Secretary.Lastname + " " + x.Secretary.Firstname
                    SecretaryName = string.Join(", ", x.Secretaries)
                }).ToList();
            }
            return type == null ? result : result.Where(x => x.Type == type).ToList();

        }
        /// <summary>
        /// lấy ID của chi đoàn cán bộ.nếu chưa có thì tạo
        /// /// </summary>
        /// <param search="facultyID"></param>
        /// <returns></returns>
        public long GetIDStaffClass(int facultyID)
        {
            var faculty = context.Units.FirstOrDefault(x => x.ID == facultyID);
            var classStaff = context.YouthUnionClasses.FirstOrDefault(x => x.FacultyID == facultyID && x.Type == QLDoanTruong.Common.ConstraintUnionClassType.LECTURE);
            if (classStaff != null)
            {
                return classStaff.ID;
            }
            //tạo CĐ CB
            classStaff = new YouthUnionClass()
            {
                Name = "Chi đoàn cán bộ " + faculty.Name,
                Type = QLDoanTruong.Common.ConstraintUnionClassType.LECTURE,
                FacultyID = faculty.ID,
                YouthUnionClassCode = "CB" + faculty.UnitCode
            };
            return Add(classStaff).ID;

        }
        public void Dispose()
        {
            context.Dispose();
        }


    }
}
