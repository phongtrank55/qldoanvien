﻿using System;
using System.Web;
using System.Web.Mvc;
using PagedList;
using QLDoanTruong.Model;
using QLDoanTruong.Service;
using QLDoanTruong.Common;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using QLDoanTruong.Web.Common;
using QLDoanTruong.Web.Areas.YouthUnionUniversity.Controllers;
using System.Net;

namespace QLDoanTruong.Web.Areas.YouthUnionFaculty.Controllers
{
    [AllowPermission(RoleID = "SEND_REPORT")]
    public class MonthlyReportController : BaseController
    {
        private MonthlyReportExpireDateService reportExpireDateService = new MonthlyReportExpireDateService();
        private ActivityService activityService = new ActivityService();
        private MonthlyReportService reportService = new MonthlyReportService();
        //Báo cáo đang soạn
        private TempMonthlyReportDetailService tempDetailService = new TempMonthlyReportDetailService();
        //Các báo cáo bên đoàn trường nhận
        private MonthlyReportDetailService detailService = new MonthlyReportDetailService();

        //
        // GET: /YouthUnionFaculty/MonthlyReport/
        public override void AssignPermission()
        {

        }
        /// <summary>
        /// Kiểm tra người dùng hiện tại thuộc khoa nào hay không
        /// </summary>
        private bool HasNotOwnFaculty()
        {
            if (currentFaculty == null)
            {
                ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                return true;
            }
            return false;
        }
        
        public ActionResult Index()
        {
            if (HasNotOwnFaculty()) return View();

            ViewBag.FacultyName = currentFaculty.Name;
            ViewBag.Year = new SelectList(CommonList.GetYears(), "ID", "Name", DateTime.Now.Year);
            ViewBag.Month = new SelectList(CommonList.Months, "ID", "Name", DateTime.Now.Month);
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionFaculty/MonthlyReport/", Title = "Soạn thảo báo cáo" });
            return View();
        }

        public ActionResult ReportSent()
        {
            if (HasNotOwnFaculty()) return View();

            ViewBag.FacultyName = currentFaculty.Name;
            //ViewBag.Year = new SelectList(CommonList.GetYears(), "ID", "Name", DateTime.Now.Year);
            //ViewBag.Month = new SelectList(CommonList.Months, "ID", "Name", DateTime.Now.Month);
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionFaculty/MonthlyReport/ReportSent", Title = "Báo cáo đã gửi" });
    
            return View();
        }

        public PartialViewResult GetComposeReport(int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            var reports = reportService.GetComposerReportViewByFaculty(currentFaculty.ID);

            return PartialView("_MonthlyReportComposeListPartial", reports.ToPagedList(pageNumber, pageSize));
        }
        public PartialViewResult GetReportSent(int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            var reports = reportService.GetReportSentViewByFaculty(currentFaculty.ID);

            return PartialView("_MonthlyReportSentListPartial", reports.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddComposerReport(int month, int year)
        {
            var report = reportService.GetSingleByFacultyAndMonth(currentFaculty.ID, month, year);
            if (report == null)
            {
                //Thêm báo cáo
                report = reportService.Add(new MonthlyReport()
                {
                    Month = month,
                    Year = year,
                    FacultyID = currentFaculty.ID,
                });
            }
            else if (!report.IsComposing)
            {
                report.IsComposing = true;
                reportService.Update(report);
            }
            return Redirect(Url.Action("ComposeReport", new { id = report.ID }));
        }


        //
        // GET: /YouthUnionFaculty/MonthlyReport/
        public ActionResult ComposeReport(long? id)
        {
            if (HasNotOwnFaculty()) return View();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var currentReport = reportService.GetSingleById(id);

            if (currentReport == null)
            {
                return HttpNotFound();
            }

            ViewBag.ReportID = id;
            //Xét thời hạn

            var expireDate = reportExpireDateService.GetSingleByMonthAndYear(currentReport.Month, currentReport.Year);
            ViewBag.ExpireDate = expireDate;

            ViewBag.CanBeSend = expireDate.DueDate >= DateTime.Now || (expireDate.IsEnabledCutOffDate && expireDate.CutOffDate >= DateTime.Now);

            ViewBag.FacultyName = currentFaculty.Name;
            ViewBag.ReportName = String.Format("Báo cáo hoạt động tháng {0:00}/{1:0000} - {2}", currentReport.Month, currentReport.Year, currentFaculty.Name);
            ViewBag.ActivityID = new SelectList(activityService.GetActivityToReportByFaculty(id.Value), "ID", "Name");
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionFaculty/MonthlyReport/", Title = "Soạn thảo báo cáo" });
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = string.Format("Báo cáo hoạt động {0:00}/{1}", currentReport.Month, currentReport.Year) });
    
            return View("ComposeReport", tempDetailService.GetAllByMonthlyReport(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddActivityToReport(TempMonthlyReportDetail tempReport, string date, HttpPostedFileBase file, int reportID)
        {

            if (string.IsNullOrEmpty(date)) tempReport.DateActive = DateTime.Today;
            else
                tempReport.DateActive = QLDoanTruong.Common.DateUtility.GetDateFromVietNamese(date);

            tempReport.MonthReportID = reportID;
            //    ViewBag.Success = false;
            if (ModelState.IsValid)
            {

                if (file != null)
                {
                    string fileName = StringUtility.RemoveUnicode("minh chung " + tempReport.MonthReportID + " " + tempReport.ActivityID + " " + currentFaculty.Name) + file.FileName.Substring(file.FileName.LastIndexOf('.'));//lây đuôi

                    var path = HttpContext.Server.MapPath(ConstraintName.PROOF_FILE_DIRECTORY_PATH) + fileName;
                    file.SaveAs(path);
                    tempReport.FileUploadProof = fileName;
                }

                tempDetailService.Add(tempReport);
            }

            //Thêm chi tiết báo cáo

            return RedirectToAction("ComposeReport", new { id = reportID });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendReport(long reportID)
        {
            
            if (HasNotOwnFaculty()) return View();
            ViewBag.SendSuccess = false;
            var currentReport = reportService.GetSingleById(reportID);
            if (currentReport == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            try
            {
                var expireDate = reportExpireDateService.GetSingleByMonthAndYear(currentReport.Month, currentReport.Year);
                if (!(expireDate.DueDate >= DateTime.Now || (expireDate.IsEnabledCutOffDate && expireDate.CutOffDate >= DateTime.Now)))
                {
                    ViewBag.ErrorMessageSend = "Không gửi được do đã quá hạn!";
                }
                else
                {
                    tempDetailService.SendReport(reportID);
                    return RedirectToAction("ReportSent");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessageSend = "Đã có lỗi hệ thống!";
            }

            return ComposeReport(reportID);
        }

        public ActionResult Delete(long? activityID, long? reportID)
        {
            if (activityID == null || reportID == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            tempDetailService.DeleteActivityInReport(activityID, reportID);
            return RedirectToAction("ComposeReport", new { id = reportID });
        }


        //GET: /YouthUnionFaculty/ReportMonth/Details/5
        public ActionResult DetailsSentReport(long? id)
        {
            if (HasNotOwnFaculty()) return View();

            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            var report = reportService.GetSingleById(id);

            if (report == null)
            {
                return HttpNotFound();
            }
            ViewBag.SentDate = report.SentDate;
            ViewBag.FacultyName = report.Unit.Name;
            ViewBag.ReportID = report.ID;
            ViewBag.Month = report.Month;
            ViewBag.Year = report.Year;
            ViewBag.Rank = report.Rank == null ? "Chưa xếp hạng" : report.Rank.ToString();
            ViewBag.OtherPoint = report.OtherPoint;
            ViewBag.Comment = report.Comment;

            var detail = detailService.GetAllByMonthlyReport(id);
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionFaculty/MonthlyReport/ReportSent", Title = "Báo cáo đã gửi" });
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = string.Format("Báo cáo hoạt động {0:00}/{1}", report.Month, report.Year)});
    
            return View(detail);
        }


        public FileResult Export(long id)
        {

            var workbook = new XSSFWorkbook();
            var report = reportService.GetSingleById(id);
            if (report == null) return null;

            var sheet1 = workbook.CreateSheet(report.MonthlyReportCode);
            //make a header row  /
            string[] headers ={"STT", "Mã hoạt động", "Tên hoạt động", "Ngày hoạt động", "Điểm tự chấm","Xác nhận",
                                  "Điểm xác nhận"};

            //merge

            //ExportExcelHelper.SetFontCell(ref cell, workbook, 14, true);

            sheet1.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 0, headers.Length - 1));

            IRow row1 = sheet1.CreateRow(0);
            var cell = row1.CreateCell(0);
            cell.SetCellValue(string.Format("Báo cáo hoạt động {0}/{1}", report.Month, report.Year));

            row1 = sheet1.CreateRow(1);
            cell = row1.CreateCell(0);
            cell.SetCellValue(report.Unit.Name);

            row1 = sheet1.CreateRow(2);
            cell = row1.CreateCell(0);
            cell.SetCellValue("Ngày gửi: " + report.SentDate.Value.ToString("H:mm dd/MM/yyyy"));

            row1 = sheet1.CreateRow(3);
            cell = row1.CreateCell(0);
            //cell.SetCellValue("Xếp hạng: " + (report.Rank == null ? "Chưa xếp hạng" : report.Rank.ToString()));


            row1 = sheet1.CreateRow(5);

            for (int j = 0; j < headers.Length; j++)
            {
                cell = row1.CreateCell(j);
                ExportExcelHelper.SetStyleCellHeaderTable(ref cell, workbook);

                cell.SetCellValue(headers[j]);
            }

            //loops through data  
            var detail = detailService.GetAllByMonthlyReport(id);

            int rowStart = 6;
            int stt = 1;
            foreach (var mem in detail)
            {

                IRow row = sheet1.CreateRow(rowStart++);
                string[] rowData = new string[]{(stt++).ToString(), mem.Activity.ActivityCode, mem.Activity.Name,
                            mem.DateActive.ToString("dd/MM/yyyy"), 
   
                            mem.PointSelf.ToString(), mem.Submited?"OK":string.Empty,mem.PointSubmit.ToString() };

                for (int j = 0; j < rowData.Length; j++)
                {
                    cell = row.CreateCell(j);
                    ExportExcelHelper.SetStyleCellContentTable(ref cell, workbook);
                    cell.SetCellValue(rowData[j]);
                }
            }

            MemoryStream workStream = new MemoryStream();
            workbook.Write(workStream);
            return File(workStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Bao cao hoat dong " + report.MonthlyReportCode + ".xlsx");


        }

        [HttpPost]
        public bool DeleteComposeReport(long? id)
        {
            if (id == null)
                return false;
            var report = reportService.GetSingleById(id);
            if (report == null || !report.IsComposing)
                return false;
           
            try
            {
                report.IsComposing = false;
                reportService.Update(report);
                //delete temp
                tempDetailService.DeleteByReport(id);
                return true;
            }
            catch { }
            return false;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                activityService.Dispose();
                reportService.Dispose();
                detailService.Dispose();
                tempDetailService.Dispose();
                reportExpireDateService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}