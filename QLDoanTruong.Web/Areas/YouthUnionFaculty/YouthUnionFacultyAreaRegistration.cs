﻿using System.Web.Mvc;

namespace QLDoanTruong.Web.Areas.YouthUnionFaculty
{
    public class YouthUnionFacultyAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "YouthUnionFaculty";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "YouthUnionFaculty_default",
                "YouthUnionFaculty/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}