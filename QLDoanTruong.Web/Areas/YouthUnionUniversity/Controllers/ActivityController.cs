﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QLDoanTruong.Model;
using QLDoanTruong.Common;
using QLDoanTruong.Service;
using QLDoanTruong.Web.Common;
using PagedList;
using System.Collections.Generic;
using QLDoanTruong.Model.ViewModel;

namespace QLDoanTruong.Web.Areas.YouthUnionUniversity.Controllers
{
    //[Permission(RoleID = "UNIVERSITY")]

    public class ActivityController : BaseController
    {
        private LeaderUnitService leaderUnitService = new LeaderUnitService();
        private UnitService unitService = new UnitService();
        private YouthUnionMemberService memberService = new YouthUnionMemberService();
        private ActivityService activityService = new ActivityService();

        public ActivityController()
            : base()
        {

            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/Activity/", Title = "Hoạt động" });
        }

        public override void AssignPermission()
        {
            ViewBag.VIEW_ALL_ACTIVITY = permissions.Contains("VIEW_ALL_ACTIVITY");
            ViewBag.VIEW_ACTIVITY_OWN_FACULTY = permissions.Contains("VIEW_ACTIVITY_OWN_FACULTY");
            ViewBag.MANAGE_ACTIVITY = permissions.Contains("MANAGE_ACTIVITY");
        }

        // GET: /YouthUnionUniversity/Activity/
        [AllowPermission(RoleID = "VIEW_ACTIVITY_OWN_FACULTY|VIEW_ALL_ACTIVITY")]
        public ActionResult Index()
        {
            //kiểm tra người dùng đã chọn khoa
            if (!ViewBag.VIEW_ALL_ACTIVITY && ViewBag.VIEW_ACTIVITY_OWN_FACULTY && currentFaculty == null)
            {
                ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                return View();
            }

            //bAN ĐẦU CHỌN TRONG VÒNG 1 tháng
            var startDate = DateTime.Now.AddDays(-15);
            var endDate = DateTime.Now.AddDays(15);
            if (ViewBag.VIEW_ALL_ACTIVITY)
                ViewBag.UnitID = new SelectList(unitService.GetAll(), "ID", "Name");
            else
                ViewBag.UnitID = new SelectList(unitService.GetAll().Where(x => x.ID == currentFaculty.ID || x.Type == QLDoanTruong.Common.ConstraintUnitType.UNIVERSITY),
                                                "ID", "Name");

            ViewBag.StartDate = startDate.ToString("dd/MM/yyyy");
            ViewBag.EndDate = endDate.ToString("dd/MM/yyyy");


            return View();
        }

        [HttpPost, ActionName("Index")]
        [ValidateAntiForgeryToken]
        public ActionResult SearchActivities(string dateRange, int? unitID)
        {

            var range = DateUtility.GetDateRange(dateRange);
            //  if (unitID == null && currentFaculty != null) { 
            //              var activities 
            //            var activities = activityService.GetViewInDateRange(range.StartDate, range.EndDate, unitID);

            if (ViewBag.VIEW_ALL_ACTIVITY)
                ViewBag.UnitID = new SelectList(unitService.GetAll(), "ID", "Name", unitID);
            else
                ViewBag.UnitID = new SelectList(unitService.GetAll().Where(x => x.ID == currentFaculty.ID || x.Type == QLDoanTruong.Common.ConstraintUnitType.UNIVERSITY),
                                                "ID", "Name", unitID);
            ViewBag.StartDate = range.StartDate.ToString("dd/MM/yyyy");
            ViewBag.EndDate = range.EndDate.ToString("dd/MM/yyyy");

            return View();
        }

        public PartialViewResult GetActivities(int? page, string dateRange, int? unitID)
        {

            //Giuwx lại các tham số cũ để ajax
            ViewBag.DateRange = dateRange;
            ViewBag.UnitID = unitID;

            int pageSize = 12;
            int pageNumber = (page ?? 1);

            List<ActivityViewModel> activities = null;
            if (string.IsNullOrEmpty(dateRange))
            {
                var startDate = DateTime.Now.AddDays(-15);
                var endDate = DateTime.Now.AddDays(15);
                if (unitID == null && !ViewBag.VIEW_ALL_ACTIVITY)
                    activities = activityService.GetViewInDateRangeByFacultyAndUniversity(startDate, endDate, currentFaculty.ID);
                else
                    activities = activityService.GetViewInDateRange(startDate, endDate, unitID);
                return PartialView("_ActivitiesListPartial", activities.ToPagedList(pageNumber, pageSize));
            }
            var range = DateUtility.GetDateRange(dateRange);
            if (unitID == null && !ViewBag.VIEW_ALL_ACTIVITY)

                activities = activityService.GetViewInDateRangeByFacultyAndUniversity(range.StartDate, range.EndDate, currentFaculty.ID);
            else
                activities = activityService.GetViewInDateRange(range.StartDate, range.EndDate, unitID);

            return PartialView("_ActivitiesListPartial", activities.ToPagedList(pageNumber, pageSize));
        }

        // GET: /YouthUnionUniversity/Activity/Details/5
        [AllowPermission(RoleID = "VIEW_ACTIVITY_OWN_FACULTY|VIEW_ALL_ACTIVITY")]
        public PartialViewResult Details(int? id)
        {

            var activity = activityService.GetSingleViewByID(id);

            return PartialView("_DetailsPartial", activity);
        }

        // GET: /YouthUnionUniversity/Activity/Create
        [AllowPermission(RoleID = "MANAGE_ACTIVITY")]

        public ActionResult Create()
        {
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thêm hoạt động" });
          
            if (!ViewBag.VIEW_ALL_ACTIVITY && ViewBag.VIEW_ACTIVITY_OWN_FACULTY && currentFaculty == null)
            {
                ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                return View();
            }

            if (ViewBag.VIEW_ALL_ACTIVITY)
                ViewBag.UnitID = new SelectList(unitService.GetAll(), "ID", "Name", 1);
            ViewBag.StartDate = ViewBag.EndDate = DateTime.Now.ToString("dd/MM/yyyy");

            ViewBag.YouthUnionMemberStaffID = new SelectList(leaderUnitService.GetAllViewByUnitID(currentFaculty == null ? 1 : currentFaculty.ID)
                                                                .Select(x => new
                                                                {
                                                                    ID = x.MemberID,
                                                                    Name = x.Lastname + " " + x.Firstname + " - " + x.Position
                                                                }),
                                                                "ID", "Name");

            return View();
        }

        // POST: /YouthUnionUniversity/Activity/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "ID,ActivityCode,Content,Score,Name,UnitID")]Activity activity, string dateRange, HttpPostedFileBase file, long youthUnionMemberStaffID = 0)
        {
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thêm hoạt động" });
          
            ViewBag.Success = false;
            //Bổ sung ngày hoạt động
            var range = DateUtility.GetDateRange(dateRange);
            activity.StartDate = range.StartDate;
            activity.EndDate = range.EndDate;
            ViewBag.StartDate = activity.StartDate.ToString("dd/MM/yyyy");
            ViewBag.EndDate = activity.EndDate.ToString("dd/MM/yyyy");
            if (ViewBag.VIEW_ALL_ACTIVITY)
                ViewBag.UnitID = new SelectList(unitService.GetAll(), "ID", "Name", activity.UnitID);
            else
                activity.UnitID = currentFaculty.ID;
            ViewBag.YouthUnionMemberStaffID = new SelectList(leaderUnitService.GetAllViewByUnitID(activity.UnitID)
                                                    .Select(x => new
                                                    {
                                                        ID = x.MemberID,
                                                        Name = x.Lastname + " " + x.Firstname + " - " + x.Position
                                                    }),
                                                    "ID", "Name", youthUnionMemberStaffID);

            //không cho cán bộ phụ trách null
            if (youthUnionMemberStaffID != 0)
            {
                var member = memberService.GetSingleById(youthUnionMemberStaffID);
                if (member != null)
                {
                    activity.YouthUnionMemberStaff = string.Format("{0} {1} - {2}", member.Lastname, member.Firstname, member.YouthUnionMemberCode);
                    ModelState["YouthUnionMemberStaff"].Errors.Clear();
                }
            }
            if (ModelState.IsValid)
            {

                if (file != null)
                {
                    string fileName = StringUtility.RemoveUnicode("tai lieu " + activity.Name) + file.FileName.Substring(file.FileName.LastIndexOf('.'));//lây đuôi
                    var path = HttpContext.Server.MapPath(ConstraintName.DOCUMENT_ACTIVITY_DIRECTORY_PATH) + fileName;
                    file.SaveAs(path);
                    activity.Document = fileName;
                }

                activityService.Add(activity);
                ViewBag.Success = true;
                //   return RedirectToAction("Create");
                ModelState.Clear();
                return View();
                //return RedirectToAction("Index");
            }
            return View(activity);
        }

        [HttpPost]
        public JsonResult GetLeader(int unitID)
        {
            return Json(leaderUnitService.GetAllViewByUnitID(unitID), JsonRequestBehavior.AllowGet);
        }



        // GET: /YouthUnionUniversity/Activity/Edit/5
        [AllowPermission(RoleID = "MANAGE_ACTIVITY")]

        public ActionResult Edit(int? id)
        {
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật hoạt động" });
          
            if (!ViewBag.VIEW_ALL_ACTIVITY && ViewBag.VIEW_ACTIVITY_OWN_FACULTY && currentFaculty == null)
            {
                ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                return View();
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Activity activity = activityService.GetSingleById(id);

            if (activity == null)
            {
                return HttpNotFound();
            }

            //kiểm tra đã sửa hoạt động khoa mình không
            if (!ViewBag.VIEW_ALL_ACTIVITY && ViewBag.VIEW_ACTIVITY_OWN_FACULTY && activity.UnitID != currentFaculty.ID)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            ViewBag.StartDate = activity.StartDate.ToString("dd/MM/yyyy");
            ViewBag.EndDate = activity.EndDate.ToString("dd/MM/yyyy");
            if (ViewBag.VIEW_ALL_ACTIVITY)
                ViewBag.UnitID = new SelectList(unitService.GetAll(), "ID", "Name", activity.UnitID);
            //lấy mã cán bộ hiện tại
            var mem = memberService.GetSingleByMemberCode(activity.YouthUnionMemberStaff.Split('-')[1].Trim());
            long youthUnionStaffID = mem==null ? 0 :mem.ID;
            ViewBag.YouthUnionMemberStaffID = new SelectList(leaderUnitService.GetAllViewByUnitID(activity.UnitID)
                                                    .Select(x => new
                                                    {
                                                        ID = x.MemberID,
                                                        Name = x.Lastname + " " + x.Firstname + " - " + x.Position
                                                    }),
                                                    "ID", "Name", youthUnionStaffID);
            return View(activity);
        }

        // POST: /YouthUnionUniversity/Activity/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "ID,ActivityCode,Content,Name,Score,UnitID")]Activity activity, string dateRange, HttpPostedFileBase file, long youthUnionMemberStaffID = 0)
        {
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật hoạt động" });
          
            ViewBag.Success = false;

            if (ViewBag.VIEW_ALL_ACTIVITY)
                ViewBag.UnitID = new SelectList(unitService.GetAll(), "ID", "Name", activity.UnitID);
            else
                activity.UnitID = currentFaculty.ID;
            //Bổ sung ngày hoạt động
            var range = DateUtility.GetDateRange(dateRange);
            activity.StartDate = range.StartDate;
            activity.EndDate = range.EndDate;

            //không cho cán bộ phụ trách null
            if (youthUnionMemberStaffID != 0)
            {
                var member = memberService.GetSingleById(youthUnionMemberStaffID);
                if (member != null)
                {
                    activity.YouthUnionMemberStaff = string.Format("{0} {1} - {2}", member.Lastname, member.Firstname, member.YouthUnionMemberCode);
                    ModelState["YouthUnionMemberStaff"].Errors.Clear();
                }
            }

            if (ModelState.IsValid)
            {

                if (file != null)
                {
                    string fileName = StringUtility.RemoveUnicode("tai lieu " + activity.Name) + file.FileName.Substring(file.FileName.LastIndexOf('.'));//lây đuôi
                    var path = HttpContext.Server.MapPath(ConstraintName.DOCUMENT_ACTIVITY_DIRECTORY_PATH) + fileName;
                    file.SaveAs(path);
                    activity.Document = fileName;
                }
                ViewBag.Success = true;
                activityService.Update(activity);
                //return RedirectToAction("Index");
            }
            //Bổ sung ngày hoạt động

            ViewBag.StartDate = activity.StartDate.ToString("dd/MM/yyyy");
            ViewBag.EndDate = activity.EndDate.ToString("dd/MM/yyyy");

            ViewBag.YouthUnionMemberStaffID = new SelectList(leaderUnitService.GetAllViewByUnitID(activity.UnitID)
                                                    .Select(x => new
                                                    {
                                                        ID = x.MemberID,
                                                        Name = x.Lastname + " " + x.Firstname + " - " + x.Position
                                                    }),
                                                    "ID", "Name", youthUnionMemberStaffID);
            return View(activity);
        }



        // POST: /YouthUnionUniversity/Activity/Delete/5
        [HttpPost, ActionName("Delete")]

        public bool DeleteConfirmed(long? id)
        {
            if (id == null)
            {
                return false;
            }
            var activity = activityService.GetSingleById(id);
            if (activity == null)
            {
                return false;
            }
            try
            {
                activityService.Delete(activity);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                activityService.Dispose();
                memberService.Dispose();
                unitService.Dispose();
                leaderUnitService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
