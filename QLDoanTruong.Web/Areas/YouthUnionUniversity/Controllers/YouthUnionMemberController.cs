﻿using System.Collections.Generic;
using System.Web.Mvc;
using QLDoanTruong.Model;
using QLDoanTruong.Service;
using System.Linq;
using PagedList;
using QLDoanTruong.Common;
using System.Web;
using System;
using QLDoanTruong.Model.ViewModel;
using System.Net;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.Util;
using QLDoanTruong.Web.Common;
using OfficeOpenXml;


namespace QLDoanTruong.Web.Areas.YouthUnionUniversity.Controllers
{

    public class YouthUnionMemberController : BaseController
    {

        private YouthUnionFacultyService facultyService = new YouthUnionFacultyService();

        private AcademicYearService academicYearService = new AcademicYearService();
        private YouthUnionClassService classService = new YouthUnionClassService();
        private YouthUnionMemberService memberService = new YouthUnionMemberService();
        private PositionService positionService = new PositionService();

        public YouthUnionMemberController()
            : base()
        {

            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/YouthUnionMember/", Title = "Đoàn viên" });
        }
        public override void AssignPermission()
        {
            ViewBag.VIEW_MEMBER_OWN_FACULTY = permissions.Contains("VIEW_MEMBER_OWN_FACULTY");
            ViewBag.MANAGE_MEMBER = permissions.Contains("MANAGE_MEMBER");
            ViewBag.VIEW_ALL_MEMBER = permissions.Contains("VIEW_ALL_MEMBER");
            ViewBag.VIEW_MEMBER_OWN_CLASS = permissions.Contains("VIEW_MEMBER_OWN_CLASS");

            ViewBag.MANAGE_STAFF = permissions.Contains("MANAGE_STAFF");
        }
        //
        // GET: /YouthUnionUniversity/YouthUnionMember/
        [AllowPermission(RoleID = "VIEW_MEMBER_OWN_FACULTY|MANAGE_STAFF|VIEW_ALL_MEMBER|VIEW_MEMBER_OWN_CLASS")]
        public ActionResult Index()
        {

            if (!ViewBag.VIEW_ALL_MEMBER)
            {
                if (ViewBag.VIEW_MEMBER_OWN_FACULTY && currentFaculty == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                    return View();
                }
                if (ViewBag.VIEW_MEMBER_OWN_CLASS && currentClass == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Chi đoàn trong hệ thống!";
                    return View();
                }
            }

            List<Item> types = new List<Item>();//loại đoàn viên

            if (ViewBag.VIEW_ALL_MEMBER || ViewBag.VIEW_MEMBER_OWN_FACULTY)
            {
                types.Add(new Item() { ID = ConstraintUnionMemberType.STUDENT, Name = "Sinh viên" });
                types.Add(new Item() { ID = ConstraintUnionMemberType.LECTURE, Name = "Giảng viên" });
                ViewBag.AcademicYearID = new SelectList(academicYearService.GetAll(), "ID", "Name");
                int? facultyID = null;
                if (ViewBag.VIEW_MEMBER_OWN_FACULTY && currentFaculty != null)
                    facultyID = currentFaculty.ID;
                var firstAcademicYear = academicYearService.GetFirst();
                if (firstAcademicYear != null)
                {
                    ViewBag.ClassID = new SelectList(classService.GetByFacultyAndAcademicYear(facultyID, firstAcademicYear.ID, ConstraintUnionClassType.STUDENT), "ID", "Name");
                }
                else
                {
                    ViewBag.ClassID = new SelectList(null);
                }

            }
            else if (ViewBag.VIEW_MEMBER_OWN_CLASS)
            {
                if (currentClass.Type == ConstraintUnionClassType.STUDENT)
                    types.Add(new Item() { ID = ConstraintUnionMemberType.STUDENT, Name = "Sinh viên" });
                else
                    types.Add(new Item() { ID = ConstraintUnionMemberType.LECTURE, Name = "Giảng viên" });

            }

            if (ViewBag.MANAGE_STAFF)
            {
                types.Add(new Item() { ID = ConstraintUnionMemberType.STAFF, Name = "Cán bộ Đoàn trường" });
            }


            if (ViewBag.VIEW_ALL_MEMBER)
            {
                ViewBag.FacultyID = new SelectList(facultyService.GetAll(), "ID", "Name");
            }

            if (types.Count > 1)
                ViewBag.Type = new SelectList(types, "ID", "Name");


            ViewBag.ReligionID = new SelectList(new ReligionService().GetAll(), "ID", "Name");

            return View();
        }

        [HttpPost, ActionName("Index")]
        [ValidateAntiForgeryToken]
        public ActionResult Search(SearchMemberViewModel searchForm)
        {
            List<Item> types = new List<Item>();//loại đoàn viên

            if (ViewBag.VIEW_ALL_MEMBER || ViewBag.VIEW_MEMBER_OWN_FACULTY)
            {
                types.Add(new Item() { ID = ConstraintUnionMemberType.STUDENT, Name = "Sinh viên" });
                types.Add(new Item() { ID = ConstraintUnionMemberType.LECTURE, Name = "Giảng viên" });
                ViewBag.AcademicYearID = new SelectList(academicYearService.GetAll(), "ID", "Name", searchForm.AcademicYearID);


                ViewBag.ClassID = new SelectList(classService.GetByFacultyAndAcademicYear(searchForm.FacultyID, null, ConstraintUnionClassType.STUDENT), "ID", "Name", searchForm.ClassID);
            }
            else if (ViewBag.VIEW_MEMBER_OWN_CLASS)
            {
                if (currentClass.Type == ConstraintUnionClassType.STUDENT)
                    types.Add(new Item() { ID = ConstraintUnionMemberType.STUDENT, Name = "Sinh viên" });
                else
                    types.Add(new Item() { ID = ConstraintUnionMemberType.LECTURE, Name = "Giảng viên" });

            }
            if (ViewBag.VIEW_ALL_MEMBER)
            {
                ViewBag.FacultyID = new SelectList(facultyService.GetAll(), "ID", "Name", searchForm.FacultyID);
            }
            if (ViewBag.MANAGE_STAFF)
            {
                types.Add(new Item() { ID = ConstraintUnionMemberType.STAFF, Name = "Cán bộ Đoàn trường" });
            }
            if (types.Count > 1)
                ViewBag.Type = new SelectList(types, "ID", "Name", searchForm.Type);


            ViewBag.ReligionID = new SelectList(new ReligionService().GetAll(), "ID", "Name", searchForm.ReligionID);

            return View(searchForm);
        }


        /// <summary>
        /// Lấy
        /// </summary>
        /// <param search="facultyID"></param>
        /// <param search="academicYearID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetClassesStudent(long? facultyID, int? academicYearID)
        {

            int classType = ConstraintUnionClassType.STUDENT;
            if (!ViewBag.VIEW_ALL_MEMBER && ViewBag.VIEW_MEMBER_OWN_FACULTY)
            {
                facultyID = currentFaculty.ID;
            }

            return Json(new YouthUnionClassService().GetByFacultyAndAcademicYear(facultyID, academicYearID, classType)
                                                        .Select(x => new { x.ID, x.Name }), JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetMembers(int? page, SearchMemberViewModel searchData)
        {

            if (searchData.Type == null)//nếu k có select type
            {
                if (ViewBag.VIEW_ALL_MEMBER || ViewBag.VIEW_MEMBER_OWN_FACULTY)
                    searchData.Type = ConstraintUnionMemberType.STUDENT; // khi load lần đầu cho sv
                else if (ViewBag.VIEW_MEMBER_OWN_CLASS)
                    searchData.Type = currentClass.Type == ConstraintUnionClassType.LECTURE ? ConstraintUnionMemberType.LECTURE : ConstraintUnionMemberType.STUDENT;
                else if (ViewBag.MANAGE_STAFF)
                    searchData.Type = ConstraintUnionMemberType.STAFF;
            }
            //nếu k co select academicyear
            if (searchData.AcademicYearID == null && searchData.Type == ConstraintUnionMemberType.STUDENT)
            {
                if (currentClass != null && currentClass.Type == ConstraintUnionClassType.STUDENT)
                    searchData.AcademicYearID = currentClass.AcademicYearID;
                else
                {
                    var firstAcademicYear = academicYearService.GetFirst();
                    if (firstAcademicYear != null)
                    {
                        searchData.AcademicYearID = firstAcademicYear.ID;
                    }
                }
            }
            if (searchData.ClassID == null && !ViewBag.VIEW_ALL_MEMBER && !ViewBag.VIEW_MEMBER_OWN_FACULTY && ViewBag.VIEW_MEMBER_OWN_CLASS && currentClass != null)
            {
                searchData.ClassID = currentClass.ID;
                searchData.FacultyID = currentFaculty.ID;
            }
            if (searchData.FacultyID == null && !ViewBag.VIEW_ALL_MEMBER && ViewBag.VIEW_MEMBER_OWN_FACULTY && currentFaculty != null)
            {
                searchData.FacultyID = currentFaculty.ID;
            }
            int pageSize = 12;
            int pageNumber = (page ?? 1);


            var model = memberService.FindBySearchViewModel(searchData);
            ViewBag.CountResult = model.Count;
            ViewBag.SearchData = searchData;
            return PartialView("_MembersListPartial", model.ToPagedList(pageNumber, pageSize));
        }

        //Load Dropdownlist Create
        private void LoadCreateDropDownList(long? facultyID, long? classID, int? type, int? academicYearID,
            int? positionUniversityID, int? positionFacultyID, int? positionClassID,
            int raceID = 1, int religionID = 1, int day = 1, int month = 1, int year = 1997)
        {
            List<Item> types = new List<Item>();
            //LOAD kiểu đoàn viên
            if (ViewBag.VIEW_ALL_MEMBER || ViewBag.VIEW_MEMBER_OWN_FACULTY)
            {
                types.Add(new Item() { ID = ConstraintUnionMemberType.STUDENT, Name = "Sinh viên" });
                types.Add(new Item() { ID = ConstraintUnionMemberType.LECTURE, Name = "Giảng viên" });

                ViewBag.AcademicYearID = new SelectList(academicYearService.GetAll(), "ID", "Name", academicYearID);

                ViewBag.PositionFacultyID = new SelectList(positionService.GetUnionOfFaculty(), "ID", "Name", positionFacultyID);


                if (!ViewBag.VIEW_ALL_MEMBER && ViewBag.VIEW_MEMBER_OWN_FACULTY && currentFaculty != null)
                    facultyID = currentFaculty.ID;
                if (classID == null)//khi chưa chọn lớp
                {
                    ViewBag.LoadClass = true;
                    ViewBag.YouthUnionClassID = new SelectList(new List<YouthUnionClass>());
                }
                else
                {
                    ViewBag.YouthUnionClassID = new SelectList(classService.GetByFacultyAndAcademicYear(facultyID, academicYearID, ConstraintUnionClassType.STUDENT), "ID", "Name", classID);
                }
            }
            else if (ViewBag.VIEW_MEMBER_OWN_CLASS)
            {
                types.Add(new Item() { ID = ConstraintUnionMemberType.STUDENT, Name = "Sinh viên" });
            }
            if (ViewBag.VIEW_ALL_MEMBER || ViewBag.VIEW_MEMBER_OWN_FACULTY || ViewBag.VIEW_MEMBER_OWN_CLASS)
            {
                ViewBag.PositionClassID = new SelectList(positionService.GetAllOfClass(), "ID", "Name", positionClassID);
            }

            if (ViewBag.MANAGE_STAFF)
            {
                types.Add(new Item() { ID = ConstraintUnionMemberType.STAFF, Name = "Cán bộ Đoàn trường" });
                ViewBag.PositionUniversityID = new SelectList(positionService.GetUnionOfUniversity(), "ID", "Name", positionClassID);
            }


            if (ViewBag.VIEW_ALL_MEMBER)
            {
                ViewBag.FacultyID = new SelectList(facultyService.GetAll(), "ID", "Name", facultyID);
            }

            if (types.Count > 1)
                ViewBag.Type = new SelectList(types, "ID", "Name", type);
            else // CHỈ staff và lecture ms đc tạo tài khoản riêng
            {
                ViewBag.LoadAccountView = !(ViewBag.VIEW_MEMBER_OWN_CLASS && currentClass.Type == ConstraintUnionClassType.STUDENT);
            }

            ViewBag.RaceID = new SelectList(new RaceService().GetAll(), "ID", "Name", raceID);
            ViewBag.ReligionID = new SelectList(new ReligionService().GetAll(), "ID", "Name", raceID);
            //select birthday
            ViewBag.Day = new SelectList(CommonList.Days, "ID", "Name", day);
            ViewBag.Month = new SelectList(CommonList.Months, "ID", "Name", month);
            ViewBag.Year = new SelectList(CommonList.GetYears(true), "ID", "Name", year);
        }
        //Load Dropdownlist Edit
        private void LoadEditDropDownList(long? classID, int type,
            int raceID = 1, int religionID = 1, int day = 1, int month = 1, int year = 1997)
        {
            long? facultyID = null;
            int? academicYearID = null;

            if (classID != null)
            {
                var c = classService.GetSingleById(classID);
                facultyID = c.FacultyID;
                academicYearID = c.AcademicYearID;
            }
            if (type != ConstraintUnionMemberType.STAFF)
            {

                if (ViewBag.VIEW_ALL_MEMBER || ViewBag.VIEW_MEMBER_OWN_FACULTY)
                {
                    if (type == ConstraintUnionMemberType.STUDENT)
                    {
                        ViewBag.AcademicYearID = new SelectList(academicYearService.GetAll(), "ID", "Name", academicYearID);
                        ViewBag.YouthUnionClassID = new SelectList(classService.GetByFacultyAndAcademicYear(facultyID, academicYearID, ConstraintUnionClassType.STUDENT), "ID", "Name", classID);
                    }
                    if (!ViewBag.VIEW_ALL_MEMBER && ViewBag.VIEW_MEMBER_OWN_FACULTY && currentFaculty != null)
                        facultyID = currentFaculty.ID;

                }

                if (ViewBag.VIEW_ALL_MEMBER)
                {
                    ViewBag.FacultyID = new SelectList(facultyService.GetAll(), "ID", "Name", facultyID);
                }
            }
            ViewBag.RaceID = new SelectList(new RaceService().GetAll(), "ID", "Name", raceID);
            ViewBag.ReligionID = new SelectList(new ReligionService().GetAll(), "ID", "Name", raceID);
            //select birthday
            ViewBag.Day = new SelectList(CommonList.Days, "ID", "Name", day);
            ViewBag.Month = new SelectList(CommonList.Months, "ID", "Name", month);
            ViewBag.Year = new SelectList(CommonList.GetYears(true), "ID", "Name", year);
        }


        [AllowPermission(RoleID = "MANAGE_MEMBER|MANAGE_STAFF")]
        public ActionResult Create(long? facultyID, int? academicYearID, long? classID, int? type)
        {
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thêm Đoàn viên" });
          
            if (!ViewBag.VIEW_ALL_MEMBER)
            {
                if (ViewBag.VIEW_MEMBER_OWN_FACULTY && currentFaculty == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                    return View();
                }
                if (ViewBag.VIEW_MEMBER_OWN_CLASS && currentClass == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Chi đoàn trong hệ thống!";
                    return View();
                }

            }


            LoadCreateDropDownList(facultyID, classID, type, academicYearID, null, null, null);

            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowPermission(RoleID = "MANAGE_MEMBER|MANAGE_STAFF")]
        public ActionResult Create(AccountViewModel account, int day, int month, int year, int? academicYearID, int? positionFacultyID,
                                            int? positionUniversityID, int? positionClassID, HttpPostedFileBase file)
        {
            ViewBag.Success = false;
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thêm Đoàn viên" });
          
            //Xác định loại đoàn viên
            if (account.Type == null)
            {
                if (ViewBag.MANAGE_STAFF)
                {
                    account.Type = ConstraintUnionMemberType.STAFF;
                }
                else if (ViewBag.VIEW_MEMBER_OWN_CLASS)
                {
                    account.Type = currentClass.Type == ConstraintUnionClassType.LECTURE ? ConstraintUnionMemberType.LECTURE : ConstraintUnionMemberType.STUDENT;
                }

            }
            //Kiểm tra ngày sinh hợp lệ
            DateTime birthDate;
            if (DateTime.TryParse(year + "/" + month + "/" + day, out birthDate))
            {
                account.DateOfBirth = birthDate.ToString("dd/MM/yyyy");
            }
            else
            {
                ModelState.AddModelError("DateOfBirth", "Ngày sinh không hợp lệ");
            }
            //Kiểm tra mã CB/SV
            if (account.YouthUnionMemberCode != null && memberService.ExistedMemberCode(account.YouthUnionMemberCode))
            {
                ModelState.AddModelError("YouthUnionMemberCode", "Mã SV/CB này đã tồn tại!");
            }
            //Kiểm tra email
            if (account.Email == null)
            {
                ModelState.AddModelError("Email", "Email Không được trống!");
            }
            if (account.Email != null && memberService.ExistedEmail(account.Email, null))
            {
                ModelState.AddModelError("Email", "Email này đã được đăng ký tài khoản khác!");
            }
            //Tìm Đoàn Khoa/Viện
            if (account.FacultyID == null)
            {
                account.FacultyID = currentFaculty.ID;
            }
            //tìm chi đoàn
            if (!ViewBag.VIEW_ALL_MEMBER && !ViewBag.VIEW_MEMBER_OWN_FACULTY && ViewBag.VIEW_MEMBER_OWN_CLASS && account.YouthUnionClassID == null)
            {
                account.YouthUnionClassID = currentClass.ID;
            }

            if (account.YouthUnionClassID == null)
            {
                if (account.Type == ConstraintUnionMemberType.STUDENT)
                    ModelState.AddModelError("YouthUnionClassID", "Chi Đoàn không thể để trống");
                else if (account.Type == ConstraintUnionMemberType.LECTURE)
                {
                    //tìm chi đoàn cán bộ
                    account.YouthUnionClassID = (int)classService.GetIDStaffClass(account.FacultyID.Value);
                }
            }
            //Kiểm tra tên đăng nhập
            if (account.Type == ConstraintUnionMemberType.STUDENT)
            {
                //SV mặc định  username = password = mã sv
                account.Username = account.YouthUnionMemberCode;
                account.Password = account.ConfirmPassword = account.YouthUnionMemberCode;
                //gỡ bỏ validate
                ModelState["Username"].Errors.Clear();
                ModelState["Password"].Errors.Clear();
                ModelState["ConfirmPassword"].Errors.Clear();
            }
            else if (account.Username != null && userService.ExistedUsername(account.Username))
            {
                ModelState.AddModelError("Username", "Tên đăng nhập đã tồn tại!");
            }
            //check validation
            //Load dropdownlist
            LoadCreateDropDownList(account.FacultyID, account.YouthUnionClassID, account.Type, academicYearID, positionUniversityID, positionFacultyID, positionClassID);
            //Xử lý

            if (ModelState.IsValid)
            {
                //avARTAR
                account.Photo = ConstraintName.AVATAR_DEFAULT;
                if (file != null)
                {
                    string fileName = StringUtility.RemoveUnicode("avt" + account.YouthUnionMemberCode) + file.FileName.Substring(file.FileName.LastIndexOf('.'));//lây đuôi

                    var path = HttpContext.Server.MapPath(ConstraintName.AVATAR_DIRECTORY_PATH) + fileName;
                    file.SaveAs(path);
                    account.Photo = fileName;


                }
                //xÁC định GroupUser
                account.GroupUserID = 4; //Phân theo chức vụ
                if (positionUniversityID != null)
                {
                    var positionUniversity = positionService.GetSingleById(positionClassID);
                    if (positionUniversity != null && positionUniversity.Level <= 2) account.GroupUserID = 2;//quyền đoàn trường
                }
                if (account.Type != ConstraintUnionMemberType.STAFF)
                {
                    if (positionFacultyID != null && account.GroupUserID == 4)
                    {
                        var positionFaculty = positionService.GetSingleById(positionFacultyID);
                        if (positionFaculty != null && positionFaculty.Level <= 2) account.GroupUserID = 3;//quyền đoàn khoa/viện
                    }
                    if (positionClassID != null && account.GroupUserID == 4)
                    {
                        var positionClass = positionService.GetSingleById(positionClassID);
                        if (positionClass != null && positionClass.Level <= 2) account.GroupUserID = 5;//quyền cán bộ
                    }
                }
                try
                {
                    var user = new User()
                    {
                        Username = account.Username,
                        Password = account.Password,
                        GroupUserID = account.GroupUserID
                    };
                    user = userService.Add(user);
                    var member = new YouthUnionMember()
                    {
                        ID = user.ID,
                        YouthUnionMemberCode = account.YouthUnionMemberCode,
                        Lastname = account.Lastname,
                        Firstname = account.Firstname,
                        Gender = account.Gender,
                        DateOfBirth = account.DateOfBirth,
                        Email = account.Email,
                        Type = account.Type.Value,
                        PlaceOfIssue = account.PlaceOfIssue,
                        CurrentAddress = account.CurrentAddress,
                        DateIntoParty = account.DateIntoParty,
                        DateIntoYouthUnion = account.DateIntoYouthUnion,
                        DateOfIssue = account.DateOfIssue,
                        RaceID = account.RaceID,
                        Photo = account.Photo,
                        ReligionID = account.ReligionID,
                        RelativeName = account.RelativeName,
                        RelativePhone = account.RelativePhone,
                        YouthUnionCardNo = account.YouthUnionCardNo,
                        IDCardNo = account.IDCardNo,
                        HomeTown = account.HomeTown,
                        Facebook = account.Facebook,
                        RelativeAddress = account.RelativeAddress,
                        Website = account.Website
                    };

                    if (account.Type != ConstraintUnionMemberType.STAFF)
                        member.YouthUnionClassID = account.YouthUnionClassID;

                    memberService.Add(member);

                    if (account.Type != ConstraintUnionMemberType.STAFF)
                    {
                        if (positionFacultyID != null)
                        {
                            //add in leader faculty
                            new LeaderUnitService().Add(new LeaderUnit()
                            {
                                PositionID = positionFacultyID.Value,
                                UnitID = account.FacultyID.Value,
                                YouthUnionMemberID = user.ID
                            });
                        }

                        if (positionClassID != null)
                        {
                            //add leader class
                            new LeaderClassService().Add(new LeaderClass()
                            {
                                PositionID = positionClassID.GetValueOrDefault(),
                                ClassID = account.YouthUnionClassID.Value,
                                YouthUnionMemberID = user.ID,
                                SchoolYearID = new SchoolYearService().GetCurrent().ID
                            });
                        }
                    }

                    if (positionUniversityID != null)
                    {
                        //add leader univer
                        new LeaderUnitService().Add(new LeaderUnit()
                        {
                            PositionID = positionUniversityID.GetValueOrDefault(),
                            UnitID = new UnitService().GetUnionUniversity().ID,
                            YouthUnionMemberID = user.ID,
                        });
                    }
                    ViewBag.Success = true;

                    ModelState.Clear();
                    return View();
                }
                catch (Exception ex)
                {

                }
            }

            return View(account);

        }
        ////
        // GET: /YouthUnionUniversity/YouthUnionMember/Edit/5
        [AllowPermission(RoleID = "MANAGE_MEMBER|MANAGE_STAFF")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật Đoàn viên" });
          

            if (!ViewBag.VIEW_ALL_MEMBER)
            {
                if (ViewBag.VIEW_MEMBER_OWN_FACULTY && currentFaculty == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                    return View();
                }
                if (ViewBag.VIEW_MEMBER_OWN_CLASS && currentClass == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Chi đoàn trong hệ thống!";
                    return View();
                }
            }

            var member = memberService.GetSingleById(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            ViewBag.Username = member.User.Username;
            //Check các quyền truy cập với thành  viên này
            if (member.Type == ConstraintUnionMemberType.STAFF && !ViewBag.MANAGE_STAFF)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            if (member.Type != ConstraintUnionMemberType.STAFF && !ViewBag.VIEW_ALL_MEMBER)
            {
                if (ViewBag.VIEW_MEMBER_OWN_FACULTY && member.YouthUnionClass.FacultyID != currentFaculty.ID)
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                if (!ViewBag.VIEW_MEMBER_OWN_FACULTY && ViewBag.VIEW_MEMBER_OWN_CLASS && member.YouthUnionClassID != currentClass.ID)
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            //lấy ngày sinh nhật

            if (member.DateOfBirth != null)
            {
                string[] birthDate = member.DateOfBirth.Split('/');
                LoadEditDropDownList(member.YouthUnionClassID, member.Type,
                        member.RaceID, member.ReligionID, int.Parse(birthDate[0]), int.Parse(birthDate[1]), int.Parse(birthDate[2]));
            }
            else
                LoadEditDropDownList(member.YouthUnionClassID, member.Type, member.RaceID, member.ReligionID);

            return View(member);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(YouthUnionMember member, string password, string confirmPassword, HttpPostedFileBase file, int? facultyID, long? academicYearID, int day, int year, int month)
        {
            var oldMem = memberService.GetSingleById(member.ID);
            if (oldMem == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật Đoàn viên" });
          
            ViewBag.Username = oldMem.User.Username;
            ViewBag.Success = false;
            //lấy loại đoàn viên
            member.Type = oldMem.Type;
            member.YouthUnionMemberCode = oldMem.YouthUnionMemberCode;
            member.Photo = oldMem.Photo;
            //xét đổi mật khẩu

            if ((!string.IsNullOrEmpty(password.Trim()) || !string.IsNullOrEmpty(confirmPassword.Trim())) && password.Trim() != confirmPassword.Trim())
            {
                ViewBag.ErrorPassword = "Mật khẩu không trùng nhau";
            }

            //Kiểm tra ngày sinh hợp lệ
            DateTime birthDate;
            if (DateTime.TryParse(year + "/" + month + "/" + day, out birthDate))
            {
                member.DateOfBirth = birthDate.ToString("dd/MM/yyyy");
            }
            else
            {
                ModelState.AddModelError("DateOfBirth", "Ngày sinh không hợp lệ");
            }
            //Kiểm tra email
            if (member.Email == null)
            {
                ModelState.AddModelError("Email", "Email Không được trống!");
            }
            if (member.Email != null && memberService.ExistedEmail(member.Email, member.ID))
            {
                ModelState.AddModelError("Email", "Email này đã được đăng ký tài khoản khác!");
            }
            //tìm chi đoàn
            if (!ViewBag.VIEW_ALL_MEMBER && !ViewBag.VIEW_MEMBER_OWN_FACULTY && ViewBag.VIEW_MEMBER_OWN_CLASS && member.YouthUnionClassID == null)
            {
                member.YouthUnionClassID = currentClass.ID;
            }

            //tìm chi đoàn
            if (!ViewBag.VIEW_ALL_MEMBER && !ViewBag.VIEW_MEMBER_OWN_FACULTY && ViewBag.VIEW_MEMBER_OWN_CLASS && member.YouthUnionClassID == null)
            {
                member.YouthUnionClassID = currentClass.ID;
            }

            if (member.YouthUnionClassID == null)
            {
                if (member.Type == ConstraintUnionMemberType.STUDENT)
                    ModelState.AddModelError("YouthUnionClassID", "Chi Đoàn không thể để trống");
                else if (member.Type == ConstraintUnionMemberType.LECTURE)
                {
                    if (facultyID == null) facultyID = currentFaculty.ID;
                    //tìm chi đoàn cán bộ
                    member.YouthUnionClassID = (int)classService.GetIDStaffClass(facultyID.Value);
                }
            }

            if (ViewBag.ErrorPassword == null && ModelState.IsValid)
            {
                try
                {
                    //avt
                    if (file != null)
                    {
                        string fileName = StringUtility.RemoveUnicode("avt" + member.YouthUnionMemberCode) + file.FileName.Substring(file.FileName.LastIndexOf('.'));//lây đuôi
                        var path = HttpContext.Server.MapPath(ConstraintName.AVATAR_DIRECTORY_PATH) + fileName;
                        file.SaveAs(path);
                        member.Photo = fileName;
                    }
                    memberService.UpdateBasicInfo(member);
                    //thay pass
                    if (!string.IsNullOrEmpty(password))
                    {
                        var user = userService.GetSingleById(member.ID);
                        user.PasswordChangeTimeNo++;
                        user.TokenForgetPass = null;
                        user.Password = password;
                        userService.Update(user);
                    }
                    ViewBag.Success = true;
                }
                catch
                {

                }
            }
            LoadEditDropDownList(member.YouthUnionClassID, member.Type, member.RaceID, member.ReligionID, day, month, year);
            return View(member);
        }
        //
        //Get
        [AllowPermission(RoleID = "VIEW_ALL_MEMBER|MANAGE_STAFF|VIEW_MEMBER_OWN_FACULTY|VIEW_MEMBER_OWN_CLASS")]
        public ActionResult Details(long? id)
        {
            if (!ViewBag.VIEW_ALL_MEMBER)
            {
                if (ViewBag.VIEW_MEMBER_OWN_FACULTY && currentFaculty == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                    return View();
                }
                if (ViewBag.VIEW_MEMBER_OWN_CLASS && currentClass == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Chi đoàn trong hệ thống!";
                    return View();
                }
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var member = memberService.GetSingleById(id);


            if (member == null)
            {
                return HttpNotFound();
            }

            //Check các quyền truy cập với thành  viên này
            if (member.Type == ConstraintUnionMemberType.STAFF && !ViewBag.MANAGE_STAFF)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            if (member.Type != ConstraintUnionMemberType.STAFF && !ViewBag.VIEW_ALL_MEMBER)
            {
                if (ViewBag.VIEW_MEMBER_OWN_FACULTY && member.YouthUnionClass.FacultyID != currentFaculty.ID)
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                if (!ViewBag.VIEW_MEMBER_OWN_FACULTY && ViewBag.VIEW_MEMBER_OWN_CLASS && member.YouthUnionClassID != currentClass.ID)
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            ViewBag.ID = id;
            ViewBag.Fullname = member.Lastname + " " + member.Firstname;
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = ViewBag.Fullname });
          
            return View();
        }
        public PartialViewResult GetProfile(long id)
        {
            var member = memberService.GetSingleById(id);
            ViewBag.MaxPosition = memberService.GetMaxPosition(id);
            ViewBag.FullPosition = memberService.GetFullPosition(id);

            var achievements = new AchievementService().GetViewByUnionID(ConstraintUnionType.MEMBER, id);
            return PartialView("_ProfilePartial", new Tuple<YouthUnionMember, List<AchievementViewModel>>(member, achievements));
        }

        [HttpPost]
        [AllowPermission(RoleID = "MANAGE_MEMBER|MANAGE_STAFF")]
        public bool Delete(long? id)
        {
            if (id == null)
            {
                return false;
            }

            var member = memberService.GetSingleById(id);
            if (member == null)
            {
                return false;
            }
            try
            {
                memberService.Delete(member);
                return true;
            }
            catch
            {
                return false;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public FileResult ExportExcel(SearchMemberViewModel searchData)
        {
            if (!ViewBag.VIEW_ALL_MEMBER)
                searchData.FacultyID = currentFaculty.ID;
            var workbook = new XSSFWorkbook();
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Đoàn viên");

            ws.Cells["A1"].Value = "STT";
            ws.Cells["B1"].Value = "Mã Sinh Viên";
            ws.Cells["C1"].Value = "Họ";
            ws.Cells["D1"].Value = "Tên";
            ws.Cells["E1"].Value = "Ngày sinh";
            ws.Cells["F1"].Value = "Giới tính";
            ws.Cells["G1"].Value = "Lớp";
            ws.Cells["H1"].Value = "Khoa";
            ws.Cells["I1"].Value = "Chức vụ";
            ws.Cells["J1"].Value = "Tôn giáo";
            ws.Cells["K1"].Value = "Dân tộc";
            ws.Cells["L1"].Value = "Quê quán";
            ws.Cells["M1"].Value = "Thường trú";
            ws.Cells["N1"].Value = "Ngày vào đoàn";
            ws.Cells["O1"].Value = "Ngày vào đảng";
            ws.Cells["P1"].Value = "CMND";
            ws.Cells["Q1"].Value = "Điện thoại";
            ws.Cells["R1"].Value = "Email";

            var members = memberService.FindBySearchViewModel(searchData);

            int index = 1;
            int rowStart = 2;
            foreach (var item in members)
            {
                //Chức vụ
                var position = string.Join(", ", item.LeaderClasses.Select(x => x.Position.Name).Union(item.LeaderUnits.Select(m => m.Position.Name)));

                ws.Cells[string.Format("A{0}", rowStart)].Value = index++;
                ws.Cells[string.Format("B{0}", rowStart)].Value = item.YouthUnionMemberCode;
                ws.Cells[string.Format("C{0}", rowStart)].Value = item.Lastname;
                ws.Cells[string.Format("D{0}", rowStart)].Value = item.Firstname;
                ws.Cells[string.Format("E{0}", rowStart)].Value = item.DateOfBirth;
                ws.Cells[string.Format("F{0}", rowStart)].Value = item.Gender == 1 ? "Nam" : "Nữ";
                ws.Cells[string.Format("G{0}", rowStart)].Value = item.YouthUnionClass == null ? String.Empty : item.YouthUnionClass.Name;
                ws.Cells[string.Format("H{0}", rowStart)].Value = item.YouthUnionClass == null ? String.Empty : item.YouthUnionClass.Unit.Name;
                ws.Cells[string.Format("I{0}", rowStart)].Value = position;

                ws.Cells[string.Format("J{0}", rowStart)].Value = item.Race.Name;
                ws.Cells[string.Format("K{0}", rowStart)].Value = item.Religion.Name;
                ws.Cells[string.Format("L{0}", rowStart)].Value = item.HomeTown;
                ws.Cells[string.Format("M{0}", rowStart)].Value = item.PermanentAddress;
                ws.Cells[string.Format("N{0}", rowStart)].Value = item.DateIntoYouthUnion;
                ws.Cells[string.Format("O{0}", rowStart)].Value = item.DateIntoParty;
                ws.Cells[string.Format("P{0}", rowStart)].Value = item.IDCardNo;
                ws.Cells[string.Format("Q{0}", rowStart)].Value = item.Phone;
                ws.Cells[string.Format("R{0}", rowStart)].Value = item.Email;
                rowStart++;
            }

            ws.Cells["A:AZ"].AutoFitColumns();
            ws.Cells["A1:AZ1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            ws.Cells["A1:AZ1"].Style.Font.Bold = true;

            ws.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;


            var bytes = pck.GetAsByteArray();
            MemoryStream workStream = new MemoryStream();
            workStream.Write(bytes, 0, bytes.Length);
            workStream.Position = 0;

            return File(workStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Doan vien.xlsx");

        }

        #region ImportFile

        [AllowPermission(RoleID = "MANAGE_MEMBER|MANAGE_STAFF|VIEW_ALL_MEMBER")]
        public ActionResult Import()
        {
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Nhập dữ liệu" });
          
            ViewBag.AcademicYearID = new SelectList(academicYearService.GetAll(), "ID", "Name");
            ViewBag.Type = new SelectList(CommonList.ListMemberType, "ID", "Name", ConstraintUnionMemberType.STUDENT);
            ViewBag.Option = new SelectList(CommonList.ListImportOption, "ID", "Name", ConstrainImportOptionType.SKIP_EXISTS);
            List<ImportMemberViewModel> members = new List<ImportMemberViewModel>();
            return View(members);
        }

        [HttpPost]
        public PartialViewResult GetResultImport(HttpPostedFileBase file, int type, int academicYearID, int option)
        {
            if (file == null) return null;

            string path = HttpContext.Server.MapPath(ConstraintName.IMPORT_DIRECTORY_PATH) + file.FileName;
            file.SaveAs(path);
            var fileInfo = new FileInfo(path);

            var members = new List<ImportMemberViewModel>();

            if (type == ConstraintUnionMemberType.STUDENT)
            {
                NPOI.HSSF.UserModel.HSSFWorkbook hssfwb = null;//xls
                NPOI.XSSF.UserModel.XSSFWorkbook xssfwb = null;//xlsx

                using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    if (fileInfo.Extension == ".xls")
                        hssfwb = new NPOI.HSSF.UserModel.HSSFWorkbook(fileStream);
                    else
                        xssfwb = new NPOI.XSSF.UserModel.XSSFWorkbook(fileStream);
                }
                NPOI.SS.UserModel.ISheet sheet = null;
                if (hssfwb != null)
                    sheet = hssfwb.GetSheetAt(0);
                else
                    sheet = xssfwb.GetSheetAt(0);

                //validate cột
                int memberCodeColumnIndex = -1, firstNameColumnIndex = -1, lastNameColumnIndex = -1, genderColumnIndex = -1,
                        birthdateColumnIndex = -1, classColumnIndex = -1, facultyColumnIndex = -1, raceColumnIndex = -1,
                        permanentColumnIndex = -1, homeTownColumnIndex = -1, religionColumnIndex = -1,
                        emailColumnIndex = -1, phoneColumnIndex = -1, dateIntoYouthUnionColumnIndex = -1,
                        dateIntoPartyColumnIndex = -1, idCardNoColumnIndex = -1;
                //lấy các index cột
                int rowStart = 0;//hàng chứa tên cột
                var headerRow = sheet.GetRow(rowStart);
                var numberCol = headerRow.LastCellNum;

                //ViewBag.ErrorMessage = "Cột: " + numberCol + "; Data" + sheet.GetRow(rowIndex).GetCell(1);
                for (int i = 0; i < numberCol; i++)
                {
                    var columnName = headerRow.GetCell(i).ToString().ToLower().Trim();
                    switch (columnName)
                    {
                        case "mã sinh viên": memberCodeColumnIndex = i; break;
                        case "tên": firstNameColumnIndex = i; break;
                        case "họ": lastNameColumnIndex = i; break;
                        case "ngày sinh": birthdateColumnIndex = i; break;
                        case "giới tính": genderColumnIndex = i; break;
                        case "lớp": classColumnIndex = i; break;
                        case "khoa": facultyColumnIndex = i; break;
                        case "dân tộc": raceColumnIndex = i; break;
                        case "quê quán": homeTownColumnIndex = i; break;

                        case "tôn giáo": religionColumnIndex = i; break;
                        case "email": emailColumnIndex = i; break;
                        case "ngày vào đoàn": dateIntoYouthUnionColumnIndex = i; break;
                        case "ngày vào đảng": dateIntoPartyColumnIndex = i; break;
                        case "điện thoại": phoneColumnIndex = i; break;
                        case "thường trú": permanentColumnIndex = i; break;
                        case "cmnd": idCardNoColumnIndex = i; break;
                        default: break;
                    }
                }
                //nếu k có đủ cột tối thiểu thì không nhập nữa
                //ViewBag.ErrorMessage = "mã sinh viên: " + memberCodeColumnIndex + "tên: " + firstNameColumnIndex + "họ: " + lastNameColumnIndex + "ngày sinh: " + birthdateColumnIndex + "giới tính: " + genderColumnIndex + "lớp: " + classColumnIndex + "khoa: " + facultyColumnIndex + "dân tộc: " + raceColumnIndex + "tỉnh thành: " + provinceColumnIndex + "quận huyện: " + districtColumnIndex + "tôn giáo: " + religionColumnIndex + "email: " + emailColumnIndex + "ngày vào đoàn: " + dateIntoYouthUnionColumnIndex + "ngày vào đảng: " + dateIntoPartyColumnIndex + "điện thoại: " + phoneColumnIndex;
                if (memberCodeColumnIndex == -1 || firstNameColumnIndex == -1 || lastNameColumnIndex == -1 || genderColumnIndex == -1
                    || birthdateColumnIndex == -1 || classColumnIndex == -1 || facultyColumnIndex == -1)
                {
                    ViewBag.ErrorMessage = "Không tìm thấy đủ số cột tối thiểu";
                }
                else
                {
                    ViewBag.NumberSuccess = 0;
                    ViewBag.NumberFail = 0;
                    //lấy danh sách khoa
                    var faculties = facultyService.GetAll().Select(x => new { x.ID, x.Name });
                    //lấy danh sách các lớp
                    var races = new RaceService().GetAll().Select(x => new { x.ID, x.Name, x.IsPopular }).ToList();
                    var religions = new ReligionService().GetAll().Select(x => new { x.ID, x.Name, x.IsPopular }).ToList();
                    var classes = classService.GetAll().Select(x => new { x.ID, x.Name, x.FacultyID, x.AcademicYearID, x.Type }).ToList();

                    for (int rowIndex = rowStart + 1; rowIndex <= sheet.LastRowNum; rowIndex++)
                    {
                        var rowData = sheet.GetRow(rowIndex);
                        var errors = new List<string>();
                        var item = new ImportMemberViewModel();
                        item.StudentCode = rowData.GetCell(memberCodeColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();
                        item.LastName = rowData.GetCell(lastNameColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();
                        item.FirstName = rowData.GetCell(firstNameColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();
                        item.DateOfBirth = rowData.GetCell(birthdateColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();
                        item.Gender = rowData.GetCell(genderColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();
                        item.Class = rowData.GetCell(classColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();
                        item.Faculty = rowData.GetCell(facultyColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();

                        //Kiểm tra trống các thông tin cần thiết
                        if (item.StudentCode == string.Empty)
                            errors.Add("Mã SV/CB để trống");
                        if (item.LastName == string.Empty)
                            errors.Add("Họ để trống");
                        //if (item.FirstName == string.Empty)
                        //    errors.Add("Tên để trống");
                        if (item.FirstName == string.Empty)
                            item.FirstName = "  ";
                        if (item.Gender == string.Empty)
                            errors.Add("Giới tính để trống");
                        //if (item.DateOfBirth == string.Empty)
                        //    errors.Add("Ngày sinh để trống");
                        if (item.Class == string.Empty)
                            errors.Add("Lớp để trống");
                        if (item.Faculty == string.Empty)
                            errors.Add("Khoa để trống");

                        var mem = item.StudentCode == string.Empty ? null : memberService.GetSingleByMemberCode(item.StudentCode);
                        //Nếu đã tồn tại sv và chế độ bỏ qua sv tồn tại
                        if (mem != null && option == ConstrainImportOptionType.SKIP_EXISTS)
                        {
                            item.IsSuccess = true;
                            ViewBag.NumberSuccess++;
                            item.Message = "Bỏ qua";
                        }
                        else
                        {
                            //validate dữ liệu
                            // Giới tính 
                            if (item.Gender != string.Empty && item.Gender.ToLower() != "nam" && item.Gender.ToLower() != "nữ")
                            {
                                errors.Add("Chỉ chấp nhận giới tính \"Nam\" hoặc \"Nữ\"");
                            }
                            //khoa
                            int classID = 0;
                            if (item.Faculty != string.Empty)
                            {
                                var faculty = faculties.FirstOrDefault(x => string.Equals(x.Name, item.Faculty, StringComparison.OrdinalIgnoreCase));
                                if (faculty == null)
                                {
                                    errors.Add("Đoàn \"" + item.Faculty + "\" không tồn tại trong hệ thống");
                                }
                                else
                                {
                                    if (item.Gender != string.Empty)
                                    {
                                        //lớp
                                        var _class = classes.FirstOrDefault(x => x.FacultyID == faculty.ID &&
                                            //x.AcademicYearID == academicYearID &&
                                                        string.Equals(x.Name, item.Class, StringComparison.OrdinalIgnoreCase));

                                        if (_class != null)
                                        {
                                            if (_class.Type != ConstraintUnionClassType.STUDENT)
                                            {
                                                errors.Add("Lớp không thuộc chi đoàn Sinh viên");
                                            }
                                            else
                                            {
                                                classID = _class.ID;
                                            }
                                        }
                                        else
                                        {
                                            if (errors.Count == 0)
                                            {
                                                //thêm lớp
                                                try
                                                {
                                                    var newClass = classService.Add(new YouthUnionClass()
                                                    {
                                                        AcademicYearID = academicYearID,
                                                        Name = item.Class,
                                                        YouthUnionClassCode = item.Class,
                                                        FacultyID = faculty.ID,
                                                        Type = ConstraintUnionClassType.STUDENT
                                                    });
                                                    classID = newClass.ID;
                                                    //thêm vào danh sách hiện tại

                                                    classes.Add(new { ID = newClass.ID, Name = newClass.Name, FacultyID = newClass.FacultyID, AcademicYearID = newClass.AcademicYearID, Type = newClass.Type });
                                                }
                                                catch
                                                {
                                                    errors.Add("Thêm chi đoàn \"" + item.Class + "\" thất bại");
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            int raceID = 0, religionID = 0;
                            if (errors.Count == 0)
                            {
                                // Dân tộc
                                var raceName = raceColumnIndex == -1 ? string.Empty : rowData.GetCell(raceColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();
                                dynamic race = null;
                                if (raceName == string.Empty || !System.Text.RegularExpressions.Regex.IsMatch(raceName, @"[a-zA-Z]"))
                                {
                                    race = races.FirstOrDefault(x => x.IsPopular);
                                    if (race == null)
                                    {
                                        race = new RaceService().GetPopular();
                                        races.Add(race);
                                    }
                                    raceID = race.ID;
                                }
                                else
                                {
                                    race = races.FirstOrDefault(x => string.Equals(raceName, x.Name, StringComparison.OrdinalIgnoreCase));

                                    if (race == null)
                                    {
                                        try
                                        {
                                            var newRace = new RaceService().Add(new Race()
                                            {
                                                Name = raceName,
                                                IsPopular = false
                                            });
                                            raceID = newRace.ID;
                                            //THÊM VÔ DAnh sách
                                            races.Add(new { ID = newRace.ID, Name = newRace.Name, IsPopular = false });
                                        }
                                        catch
                                        {
                                            errors.Add("Thêm dân tộc \"" + raceName + "\" thất bại");
                                        }
                                    }
                                    else
                                    {
                                        raceID = race.ID;
                                    }
                                }

                                //Tôn giáo
                                dynamic religion = null;
                                var religionName = religionColumnIndex == -1 ? string.Empty : rowData.GetCell(religionColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString();

                                if (religionName == string.Empty || !System.Text.RegularExpressions.Regex.IsMatch(religionName, @"[a-zA-Z]"))
                                {
                                    religion = religions.FirstOrDefault(x => x.IsPopular);
                                    if (religion == null)
                                    {
                                        religion = new RaceService().GetPopular();
                                        religions.Add(religion);
                                    }
                                    //THÊM VÔ DAnh sách

                                    religionID = religion.ID;
                                }
                                else
                                {
                                    religion = religions.FirstOrDefault(x => string.Equals(religionName, x.Name,
                                                                                       StringComparison.OrdinalIgnoreCase));
                                    if (religion == null)
                                    {
                                        try
                                        {
                                            var newReligion = new ReligionService().Add(new Religion()
                                            {
                                                Name = religionName,
                                                IsPopular = false
                                            });
                                            religionID = newReligion.ID;
                                            //THÊM VÔ DAnh sách
                                            religions.Add(new { ID = newReligion.ID, Name = newReligion.Name, IsPopular = false });
                                        }
                                        catch
                                        {
                                            errors.Add("Thêm tôn giáo \"" + religionName + "\" thất bại");
                                        }
                                    }
                                    else
                                    {
                                        religionID = religion.ID;
                                    }
                                }
                            }

                            if (errors.Count > 0)
                            {
                                ViewBag.NumberFail++;
                                item.IsSuccess = false;
                                item.Message = string.Join("; ", errors);
                            }
                            else
                            {
                                // chèn sinh viên
                                try
                                {
                                    if (mem == null)
                                    {
                                        mem = new YouthUnionMember();
                                    }
                                    mem.YouthUnionMemberCode = item.StudentCode;
                                    mem.Lastname = item.LastName;
                                    mem.Firstname = item.FirstName;
                                    mem.Gender = item.Gender.ToLower().Equals("nam") ? 1 : 0;
                                    mem.YouthUnionClassID = classID;
                                    mem.DateOfBirth = item.DateOfBirth;
                                    if (religionColumnIndex != -1) mem.ReligionID = religionID;
                                    if (raceColumnIndex != -1) mem.RaceID = raceID;
                                    if (homeTownColumnIndex != -1) mem.HomeTown = rowData.GetCell(homeTownColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();
                                    if (permanentColumnIndex != -1) mem.PermanentAddress = rowData.GetCell(permanentColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();
                                    if (dateIntoPartyColumnIndex != -1) mem.DateIntoParty = rowData.GetCell(dateIntoPartyColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();
                                    if (dateIntoYouthUnionColumnIndex != -1) mem.DateIntoYouthUnion = rowData.GetCell(dateIntoYouthUnionColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();
                                    if (phoneColumnIndex != -1) mem.Phone = rowData.GetCell(phoneColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();
                                    //validation
                                    if (mem.Phone != null)
                                    {
                                        if (!(new System.ComponentModel.DataAnnotations.PhoneAttribute().IsValid(mem.Phone)))
                                            mem.Phone = null;

                                    }

                                    if (emailColumnIndex != -1) mem.Email = rowData.GetCell(emailColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim().ToLower();
                                    if (mem.Email != null)
                                    {
                                        //validation
                                        if (!(new System.ComponentModel.DataAnnotations.EmailAddressAttribute().IsValid(mem.Email)))
                                            mem.Email = null;
                                    }
                                    if (idCardNoColumnIndex != -1) mem.IDCardNo = rowData.GetCell(idCardNoColumnIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString().Trim();

                                    if (mem.ID <= 0) //thêm mới
                                    {
                                        item.Message = "Thêm đoàn viên mới";
                                        //chèn user
                                        User user = userService.Add(new User()
                                        {
                                            GroupUserID = 4, //SV bình thường
                                            Username = item.StudentCode,
                                            Password = item.StudentCode,
                                            PasswordChangeTimeNo = 0
                                        });
                                        //bổ sung
                                        mem.Photo = ConstraintName.AVATAR_DEFAULT;
                                        mem.ID = user.ID;
                                        mem.Type = type;
                                        var res = memberService.Add(mem);
                                        if (res == null)
                                        {
                                            item.IsSuccess = false;
                                            ViewBag.NumberFail++;
                                        }
                                        else
                                        {
                                            ViewBag.NumberSuccess++;
                                            item.IsSuccess = true;
                                        }
                                    }
                                    else //cập nhật
                                    {
                                        item.Message = "Cập nhật đoàn viên";
                                        memberService.UpdateBasicInfo(mem);
                                        ViewBag.NumberSuccess++;
                                        item.IsSuccess = true;
                                    }

                                }
                                catch (Exception ex)
                                {
                                    item.IsSuccess = false;
                                    ViewBag.NumberFail++;

                                }
                            }
                        }

                        members.Add(item);
                    }
                }
            }
            fileInfo.Delete();

            return PartialView("_ImportMemberPartial", members.OrderBy(x => x.IsSuccess));
        }

        #endregion
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                academicYearService.Dispose();
                classService.Dispose();
                facultyService.Dispose();
                memberService.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}