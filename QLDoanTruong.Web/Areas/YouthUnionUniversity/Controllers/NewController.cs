﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QLDoanTruong.Model;
using PagedList;
using QLDoanTruong.Service;
using System.Net;
using QLDoanTruong.Common;
using QLDoanTruong.Web.Common;

namespace QLDoanTruong.Web.Areas.YouthUnionUniversity.Controllers
{
    public class NewController : BaseController
    {
        private NewService newService = new NewService();
        //
        // GET: /YouthUnionUniversity/New/
        public NewController()
            : base()
        {

            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/New/", Title = "Tin tức" });
        }
        public override void AssignPermission()
        {
            ViewBag.EditNew = new UserService()
                        .GetPermissionByUsername(System.Web.HttpContext.
                                Current.Session["Username"].ToString()).Contains("MANAGE_NEW");
        }

        [AllowPermission(RoleID = "VIEW_NEW")]

        public ActionResult Index()
        {
            return View();
        }


        public PartialViewResult GetNews(int? page)
        {
            int pageSize = 12;
            int pageNumber = (page ?? 1);
            //lấy quyền để trên giao diện
            AssignPermission();

            return PartialView("_NewsListPartial", newService.GetAll().ToPagedList(pageNumber, pageSize));
        }

        [AllowPermission(RoleID = "MANAGE_NEW")]
        public ActionResult Create()
        {
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thêm Tin tức" });
        
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "ID,Content,Title,Author,IsOutstanding")] New _new, HttpPostedFileBase file)
        {
            ViewBag.Success = false;
            if (ModelState.IsValid)
            {

                if (file != null)
                {
                    string fileName = StringUtility.RemoveUnicode("img " + _new.Title) + file.FileName.Substring(file.FileName.LastIndexOf('.'));//lây đuôi

                    var path = HttpContext.Server.MapPath(ConstraintName.NEW_THUMB_DIRECTORY_PATH) + fileName;
                    file.SaveAs(path);
                    _new.Image = fileName;
                }
                else
                    _new.Image = "default.PNG";

                newService.Add(_new);
                ViewBag.Success = true;
                return View();
            }
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thêm Tin tức" });
        
            return View(_new);
        }

        public ActionResult Details(long? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var _new = newService.GetSingleById(id);

            if (_new == null)
            {
                return HttpNotFound();
            }
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "" + id, Title = _new.Title });
        
            return View(_new);
        }
        // GET: /YouthUnionUniversity/New/Edit/5
        [AllowPermission(RoleID = "MANAGE_NEW")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var _new = newService.GetSingleById(id);
            if (_new == null)
            {
                return HttpNotFound();
            }
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật tin tức" });
        
            return View(_new);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "ID,Content,Title,Author,IsOutstanding")] New _new, HttpPostedFileBase file)
        {
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật tin tức" });
        
            ViewBag.Success = false;
            if (ModelState.IsValid)
            {

                if (file != null)
                {
                    string fileName = StringUtility.RemoveUnicode("img " + _new.Title) + file.FileName.Substring(file.FileName.LastIndexOf('.'));//lây đuôi

                    var path = HttpContext.Server.MapPath(ConstraintName.NEW_THUMB_DIRECTORY_PATH) + fileName;
                    file.SaveAs(path);
                    _new.Image = fileName;
                }

                newService.Update(_new);
                ViewBag.Success = true; 

                return View();
            }
            return View(_new);
        }
        [AllowPermission(RoleID = "MANAGE_NEW")]
        [HttpPost]
        public bool TickOutstanding(long? id, bool outstanding)
        {
            return newService.UpdateOutstanding(id, outstanding);
        }

        [HttpPost]
        [AllowPermission(RoleID = "MANAGE_NEW")]
        public bool Delete(long? id)
        {
            if (id == null)
            {
                return false;
            }
            var _new = newService.GetSingleById(id);
            if (_new == null)
            {
                return false;
            }
            try
            {
                newService.Delete(_new);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}