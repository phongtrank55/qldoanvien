﻿using QLDoanTruong.Model;
using QLDoanTruong.Service;
using System.Collections.Generic;
using System.Web.Mvc;
using QLDoanTruong.Common;

namespace QLDoanTruong.Web.Areas.YouthUnionUniversity.Controllers
{
    public abstract class BaseController : Controller
    {
        protected UserService userService = new UserService();
        protected Unit currentFaculty = null;
        protected YouthUnionClass currentClass = null;
        protected string username = null;
        protected List<string> permissions = null;
        public BaseController()
        {
            ViewBag.BreadCrumb = new List<BreadcrumbLink>()
            {
                new BreadcrumbLink(){Link="/Home/Dashboard/", Title = "Dashboard"},
            };

            if (System.Web.HttpContext.Current.Session["Username"] == null && System.Web.HttpContext.Current.Request.Cookies["UserInfo"] != null)
            {
                System.Web.HttpContext.Current.Session["Username"] = System.Web.HttpContext.Current.Request.Cookies["UserInfo"]["Username"].ToString();
                System.Web.HttpContext.Current.Session["Fullname"] = System.Web.HttpContext.Current.Request.Cookies["UserInfo"]["Fullname"].ToString();
                System.Web.HttpContext.Current.Session["Position"] = System.Web.HttpContext.Current.Request.Cookies["UserInfo"]["Position"].ToString();
            }
           if(System.Web.HttpContext.Current.Session["Username"] ==null)
           {
               Redirect("Home/Login");
               return;
           }
            username = System.Web.HttpContext.Current.Session["Username"].ToString();
            permissions = userService.GetPermissionByUsername(username);
            currentClass = userService.GetClassByUsername(username);
            if (currentClass != null)
                currentFaculty = currentClass.Unit;
            AssignPermission();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                userService.Dispose();
            }
            base.Dispose(disposing);
        }

        public abstract void AssignPermission();


    }
}