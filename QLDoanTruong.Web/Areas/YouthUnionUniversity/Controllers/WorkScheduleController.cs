﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QLDoanTruong.Model.ViewModel;
using QLDoanTruong.Model;
using QLDoanTruong.Service;
using QLDoanTruong.Common;
using System.Net;
using System;
using System.Threading.Tasks;
using System.IO;
using QLDoanTruong.Web.Common;
using PagedList;

namespace QLDoanTruong.Web.Areas.YouthUnionUniversity.Controllers
{
    //[Permission(RoleID = "UNIVERSITY")]
    [AllowUser]
    public class WorkScheduleController : Controller
    {
        private WorkScheduleService scheduleService = new WorkScheduleService();
        private WorkScheduleDetailService detailScheduleService = new WorkScheduleDetailService();

        public WorkScheduleController()
            : base()
        {
            ViewBag.BreadCrumb = new List<BreadcrumbLink>()
            {
                new BreadcrumbLink(){Link="/Home/Dashboard/", Title = "Dashboard"},
            };
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/WorkSchedule/", Title = "Lịch tuần" });
        }
        //
        // GET: /YouthUnionUniversity/WorkSchedule/
        [AllowPermission(RoleID = "VIEW_SCHEDULE")]
        public ActionResult Index()
        {
            using (var schoolYearService = new SchoolYearService())
            {
                var currentSchoolYear = schoolYearService.GetCurrent();
                if (currentSchoolYear == null)
                {
                    ViewBag.ErrorMessage = "Chưa thiết lập năm học hiện tại";
                    return View();
                }
                ViewBag.SchoolYear = currentSchoolYear.Name;
                //Tìm tuần cuối cùng đã có lịch
                //int lastWeek = scheduleService.GetLastWeekHasSchedule(currentSchoolYear.ID);
                //số tuần của năm học
                int weekAmount = (int)(currentSchoolYear.EndDate - currentSchoolYear.StartDate).TotalDays / 7;
                List<SelectListItem> weeks = new List<SelectListItem>();
                for (int week = 1; week <= weekAmount; week++)
                {
                    var startDate = currentSchoolYear.StartDate.AddDays(week * 7);
                    var endDate = startDate.AddDays(6);
                    weeks.Add(new SelectListItem()
                    {
                        Value = (week + 1).ToString(),
                        Text = string.Format("Tuần {0} - Từ {1:dd/MM/yyyy} đến {2:dd/MM/yyyy}", week + 1, startDate, endDate)
                    });
                }
                ViewBag.Week = new SelectList(weeks, "Value", "Text");
            }
            ///Xem có quyền sử không
            ViewBag.EditSchedule = new UserService()
                        .GetPermissionByUsername(System.Web.HttpContext.
                                Current.Session["Username"].ToString()).Contains("MANAGE_SCHEDULE");

            return View();

        }

        public PartialViewResult GetWorkSchedules(int? page)
        {

            int pageSize = 12;
            int pageNumber = (page ?? 1);
            //lấy quyền để trên giao diện
            ViewBag.EditSchedule = new UserService()
                        .GetPermissionByUsername(System.Web.HttpContext.
                                Current.Session["Username"].ToString()).Contains("MANAGE_SCHEDULE");

            return PartialView("_WorkSchedulesListPartial", scheduleService.GetAll().ToPagedList(pageNumber, pageSize));
        }



        //[HttpPost]
        //[ValidateInput(false)]
        //public FileResult Export(string html)
        //{
        //    //string fil HttpContext.Server.MapPath("~/Uploads/" + "avc.pdf"));
        //    var bytes = CommonUtility.PdfSharpConvert(html);
        //    MemoryStream workStream = new MemoryStream();
        //    workStream.Write(bytes, 0, bytes.Length);
        //    workStream.UnionPosition = 0;  

        //    return File(workStream, "application/pdf", "avc.pdf");  


        //}

        [AllowPermission(RoleID = "MANAGE_SCHEDULE")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int week)
        {
            var currentSchoolYear = new SchoolYearService().GetCurrent();
            var startDate = currentSchoolYear.StartDate.AddDays((week - 1) * 7);
            var endDate = startDate.AddDays(6);
            //KIỂM TRA ĐÃ tồn tại
            var workSchedule = scheduleService.GetByWeek(week, currentSchoolYear.ID);
            if (workSchedule == null)
            {
                workSchedule = scheduleService.Add(new WorkSchedule()
                {
                    Name = string.Format("Lịch công tác tuần {0} năm học {1} (Từ {2:dd/MM/yyyy} đến {3:dd/MM/yyyy})", week, currentSchoolYear.Name, startDate, endDate),
                    UnitID = 1,//tạm thời của đoàn trường
                    SchoolYearID = currentSchoolYear.ID,
                    StartDate = startDate,
                    Week = week
                });
                return RedirectToAction("CreateDetail", new { workSchedule.ID });
            }
            return RedirectToAction("Details", new { id = workSchedule.ID });
        }

        //public ActionResult Details(long? facultyID)
        //{
        //    if (facultyID == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var faculty = scheduleService.GetSingleById(facultyID);
        //    if (faculty == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.ScheduleName = faculty.FunctionName;
        //    ViewBag.ScheduleID = facultyID;
        //    var normalSchedule = detailScheduleService.GetAllViewByShedule(facultyID);
        //    var everydaySchedule = detailScheduleService.GetEveryDay(facultyID);
        //    return View(new Tuple<List<WorkScheduleDetailViewModel>, List<WorkScheduleDetail>>(normalSchedule, everydaySchedule));

        //}
        [AllowPermission(RoleID = "VIEW_SCHEDULE")]
        public ActionResult Details(long? id)
        {


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var workSchedule = scheduleService.GetSingleById(id);
            if (workSchedule == null)
            {
                return HttpNotFound();
            }
            ViewBag.EditSchedule = new UserService()
                        .GetPermissionByUsername(System.Web.HttpContext.
                                Current.Session["Username"].ToString()).Contains("MANAGE_SCHEDULE");

            ViewBag.ScheduleName = workSchedule.Name;
            ViewBag.ScheduleID = id;
            var normalSchedule = detailScheduleService.GetDisplayBySchedule(id).OrderBy(x => x.Date).ToList();
            var everydaySchedule = detailScheduleService.GetEveryDay(id);
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = workSchedule.Name });
        
            return View(new Tuple<List<WorkScheduleDisplayViewModel>, List<WorkScheduleDetail>>(normalSchedule, everydaySchedule));
        }

        [AllowPermission(RoleID = "MANAGE_SCHEDULE")]
        public ActionResult CreateDetail(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var workSchedule = scheduleService.GetSingleById(id);
            if (workSchedule == null)
            {
                return HttpNotFound();
            }
            ViewBag.ScheduleID = id;
            ViewBag.ScheduleName = workSchedule.Name;
            //Tải Ngày trong tuần
            List<SelectListItem> dayOfWeekList = new List<SelectListItem>();
            for (int i = 0; i < 7; i++)
            {
                var date = workSchedule.StartDate.AddDays(i);
                dayOfWeekList.Add(new SelectListItem()
                {
                    Value = date.ToShortDateString(),
                    Text = DateUtility.GetDayOfWeekVietnamese(date) + " - " + date.ToString("dd/MM/yyyy")

                });
            }
            ViewBag.Date = new SelectList(dayOfWeekList, "Value", "Text");
            //tải đơn vị
            ViewBag.Unit = new UnitService().GetAll().Select(x => new SelectListItem
                                                            {
                                                                Value = "1::" + x.ID,
                                                                Text = x.Name
                                                            }).ToList();
            //tải lảnh đạo
            ViewBag.Leader = new LeaderUnitService().GetAllView().Select(x => new SelectListItem
                                    {
                                        Value = "2::" + x.MemberID,
                                        Text = string.Format("{0} {1} - {2} - {3}", x.Lastname, x.Firstname, x.Position, x.UnitName)
                                    }).ToList();
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/WorkSchedule/Details/"+id, Title = workSchedule.Name });
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "" + id, Title = "Thêm lịch" });
        
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult CreateNormalDetail(WorkScheduleDetailViewModel scheduleDetailView)
        {
            var detail = detailScheduleService.Add(scheduleDetailView);
            return RedirectToAction("Details", new { id = scheduleDetailView.WorkScheduleID });

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult CreateEveryDayDetail(string content, int workScheduleId)
        {
            var detail = new WorkScheduleDetail()
            {
                Date = DateTime.Today,//cho có
                Content = content,
                WorkScheduleID = workScheduleId,
                IsEveryDay = true
            };
            detailScheduleService.Add(detail);
            return RedirectToAction("Details", new { id = workScheduleId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult CreateUniversaryDetail(string content, int workScheduleId, System.DateTime date)
        {
            var detail = new WorkScheduleDetail()
            {
                Content = content,
                WorkScheduleID = workScheduleId,
                IsDayLong = true,
                Date = date,
                DayOfWeek = DateUtility.GetDayOfWeekVietnamese(date)
            };
            detailScheduleService.Add(detail);
            return RedirectToAction("Details", new { id = workScheduleId });
        }
        [AllowPermission(RoleID = "MANAGE_SCHEDULE")]
        [HttpPost]
        public bool Delete(long? id)
        {
            if (id == null)
            {
                return false;
            }
            var workSchedule = scheduleService.GetSingleById(id);
            if (workSchedule == null)
            {
                return false;
            }
            try
            {
                scheduleService.Delete(workSchedule);
                return true;
            }
            catch
            {
                return false;
            }
        }

        [HttpPost]
        [AllowPermission(RoleID = "MANAGE_SCHEDULE")]
        public bool DeleteDetail(long? id)
        {
            if (id == null)
            {
                return false;
            }
            var detail = detailScheduleService.GetSingleById(id);
            if (detail == null)
            {
                return false;
            }
            try
            {
                detailScheduleService.Delete(detail);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                detailScheduleService.Dispose();
                scheduleService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}