﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QLDoanTruong.Common;
using System.Web.Mvc;
using QLDoanTruong.Model;
using QLDoanTruong.Service;
using QLDoanTruong.Common;
using QLDoanTruong.Model.ViewModel;
using System.IO;
using QLDoanTruong.Web.Common;

namespace QLDoanTruong.Web.Areas.YouthUnionUniversity.Controllers
{
     [AllowUser]
    public class AchievementController : Controller
    {
        private AchievementService achievementService = new AchievementService();
        private AwardService awardService = new AwardService();
        private YouthUnionMemberService memberService = new YouthUnionMemberService();
        private AwardLevelService awardLevelService = new AwardLevelService();
        private SchoolYearService schoolYearService = new SchoolYearService();

        public AchievementController()
        {
            ViewBag.BreadCrumb = new List<BreadcrumbLink>()
            {
                new BreadcrumbLink(){Link="/Home/Dashboard/", Title = "Dashboard"},
            };
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/Achievement/", Title = "Khen thưởng" });
        }

        //GET: /YouthUnionUniversity/Achievement/
        public ActionResult Index()
        {
            ViewBag.AwardLevelID = new SelectList(awardLevelService.GetAll(), "ID", "Name");
            ViewBag.AwardID = new SelectList(awardService.GetAll(), "ID", "Name");
            ViewBag.SchoolYearID = new SelectList(schoolYearService.GetAll(), "ID", "Name");

            return View(achievementService.GetAllView());
        }

        [HttpPost, ActionName("Index")]
        [ValidateAntiForgeryToken]
        public ActionResult SearchAchievement(int? awardID, int? awardLevelID, int? schoolYearID)
        {
            ViewBag.AwardLevelID = new SelectList(awardLevelService.GetAll(), "ID", "Name", awardLevelID);

            ViewBag.AwardID = new SelectList(awardService.GetByAwardLevel(awardLevelID), "ID", "Name", awardID);
            ViewBag.SchoolYearID = new SelectList(schoolYearService.GetAll(), "ID", "Name", schoolYearID);

            return View(achievementService.GetViewByAward(awardLevelID, awardID, schoolYearID));

        }
        //[HttpPost]
        //public JsonResult GetAward(int? awardLevelID)
        //{
        //    return Json(awardService.GetByAwardLevel(awardLevelID).Select(x => new { x.ID, x.FunctionName }), JsonRequestBehavior.AllowGet);
        //}


        //// GET: /YouthUnionUniversity/Achievement/Details/5
        //public ActionResult Details(int? facultyID)
        //{
        //    if (facultyID == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var achievement = achievementService.GetSingleViewByID(facultyID);
        //    if (achievement == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(achievement);
        //}

        // GET: /YouthUnionUniversity/Achievement/Create
        public ActionResult Create()
        {
            //BreadCrumb
            
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/Achievement/Create", Title = "Thêm khen thưởng" });

            ViewBag.AwardLevelID = new SelectList(awardLevelService.GetAll(), "ID", "Name");
            ViewBag.SchoolYearID = new SelectList(schoolYearService.GetAll(), "ID", "Name");

            return View();
        }

        [HttpPost]
        public JsonResult GetAwards(int awardLevelID)
        {
            var result = awardService.GetByAwardLevel(awardLevelID).Select(x => new { x.ID, x.Name });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public PartialViewResult GetResultImport(HttpPostedFileBase file, int schoolYearID, int awardID)
        {
            if (file == null) return null;

            string path = HttpContext.Server.MapPath("~/Uploads/Imports/") + file.FileName;
            file.SaveAs(path);
            var fileInfo = new FileInfo(path);
            var members = new List<ImportAchievementStudentViewModel>();

            var typeUnion = awardService.GetSingleById(awardID).Type;

            //Đọc file excel
            NPOI.HSSF.UserModel.HSSFWorkbook hssfwb = null;//xls
            NPOI.XSSF.UserModel.XSSFWorkbook xssfwb = null;//xlsx

            using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                if (fileInfo.Extension == ".xls")
                    hssfwb = new NPOI.HSSF.UserModel.HSSFWorkbook(fileStream);
                else
                    xssfwb = new NPOI.XSSF.UserModel.XSSFWorkbook(fileStream);
            }
            NPOI.SS.UserModel.ISheet sheet = null;
            if (hssfwb != null)
                sheet = hssfwb.GetSheetAt(0);
            else
                sheet = xssfwb.GetSheetAt(0);
            if (typeUnion == ConstraintUnionType.MEMBER)
            {

                for (int row = 3; row <= sheet.LastRowNum; row++)
                {
                    var item = new ImportAchievementStudentViewModel()
                    {
                        StudentCode = sheet.GetRow(row).GetCell(1).StringCellValue,
                        LastName = sheet.GetRow(row).GetCell(3).StringCellValue,
                        FirstName = sheet.GetRow(row).GetCell(5).StringCellValue,
                    };
                    //Xử lý và chen vào CSDL nếu hợp lệ
                    if (item.StudentCode == string.Empty || item.FirstName == string.Empty
                      || item.LastName == string.Empty)
                    {
                        item.Result = "Lỗi: Thiếu thông tin cá nhân";

                    }
                    else if (!memberService.ExistedMemberCode(item.StudentCode))
                    {
                        item.Result = "Lỗi: Mã SV/CB Không tồn tại trong hệ thống!";
                    }
                    else
                    {
                        var mem = memberService.GetSingleByMemberCode(item.StudentCode);
                        var htMem = StringUtility.RemoveRedundancySpace(mem.Lastname + " " + mem.Firstname);
                        var htImport = StringUtility.RemoveRedundancySpace(item.LastName + " " + item.FirstName);
                        if (!string.Equals(htMem, htImport, StringComparison.CurrentCultureIgnoreCase))
                        {
                            item.Result = "Lỗi: Sai thông tin họ tên";
                        }
                        else
                        {
                            try
                            {
                                achievementService.Add(new Achievement()
                                {
                                    Type = typeUnion,
                                    AwardID = awardID,
                                    SchoolYearID = schoolYearID,
                                    YouthUnionID = mem.ID
                                });
                                item.Result = "Thành công!";
                            }
                            catch
                            {
                                item.Result = "Lỗi hệ thống!";
                            }
                        }
                    }

                    members.Add(item);
                }
            }


            fileInfo.Delete();

            return PartialView("_ImportAchievementPartial", members.OrderBy(x => x.Result));
        }


        public bool Delete(long? id)
        {
            if (id == null)
            {
                return false;
            }
            var achievement = achievementService.GetSingleById(id);
            if (achievement == null)
            {
                return false;
            }
            try
            {
                achievementService.Delete(achievement);
                return true;
            }
            catch
            {
                return false;
            }
        }


        //// POST: /YouthUnionUniversity/Achievement/Create

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,AchievementCode,Content,Score,FunctionName,UnitID,YouthUnionMemberStaffID")]Achievement achievement, string dateRange, HttpPostedFileBase fileStream)
        //{
        //    //Bổ sung ngày hoạt động
        //    var range = DateUtility.GetDateRange(dateRange);
        //    achievement.StartDate = range.StartDate;
        //    achievement.EndDate = range.EndDate;

        //    //không cho cán bộ phụ trách null
        //    if (achievement.YouthUnionMemberStaffID == 0)
        //        ModelState.AddModelError("YouthUnionMemberStaffID", "Cán bộ phụ trách không thể để trống");
        //    if (ModelState.IsValid)
        //    {

        //        if (fileStream != null)
        //        {
        //            var path = HttpContext.Server.MapPath(ConstraintName.DOCUMENT_ACTIVITY_DIRECTORY_PATH) + fileStream.FileName;
        //            fileStream.SaveAs(path);
        //            achievement.Document = fileStream.FileName;
        //        }

        //        achievementService.Add(achievement);
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.StartDate = achievement.StartDate.ToString("dd/MM/yyyy");
        //    ViewBag.EndDate = achievement.EndDate.ToString("dd/MM/yyyy");

        //    ViewBag.UnitID = new SelectList(new UnitService().GetAll(), "ID", "FunctionName", achievement.UnitID);
        //    ViewBag.YouthUnionMemberStaffID = new SelectList(new LeaderService().GetAllViewByUnitID(achievement.UnitID)
        //                                            .Select(x => new
        //                                            {
        //                                                ID = x.MemberID,
        //                                                FunctionName = x.Lastname + " " + x.Firstname + " - " + x.YouthUnionMemberCode
        //                                            }),
        //                                            "ID", "FunctionName", achievement.YouthUnionMemberStaffID);
        //    return View(achievement);
        //}

        //[HttpPost]
        //public JsonResult GetLeader(int unitID)
        //{
        //    return Json(new LeaderService().GetAllViewByUnitID(unitID), JsonRequestBehavior.AllowGet);
        //}



        //// GET: /YouthUnionUniversity/Achievement/Edit/5
        //public ActionResult Edit(int? facultyID)
        //{
        //    if (facultyID == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    Achievement achievement = achievementService.GetSingleById(facultyID);

        //    if (achievement == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    ViewBag.StartDate = achievement.StartDate.ToString("dd/MM/yyyy");
        //    ViewBag.EndDate = achievement.EndDate.ToString("dd/MM/yyyy");

        //    ViewBag.UnitID = new SelectList(new UnitService().GetAll(), "ID", "FunctionName", achievement.UnitID);
        //    ViewBag.YouthUnionMemberStaffID = new SelectList(new LeaderService().GetAllViewByUnitID(achievement.UnitID)
        //                                            .Select(x => new
        //                                            {
        //                                                ID = x.MemberID,
        //                                                FunctionName = x.Lastname + " " + x.Firstname + " - " + x.YouthUnionMemberCode
        //                                            }),
        //                                            "ID", "FunctionName", achievement.YouthUnionMemberStaffID);
        //    return View(achievement);
        //}

        //// POST: /YouthUnionUniversity/Achievement/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[ValidateInput(false)]
        //public ActionResult Edit([Bind(Include = "ID,AchievementCode,Content,FunctionName,Score,UnitID,YouthUnionMemberStaffID")]Achievement achievement, string dateRange, HttpPostedFileBase fileStream)
        //{
        //    //Bổ sung ngày hoạt động
        //    var range = DateUtility.GetDateRange(dateRange);
        //    achievement.StartDate = range.StartDate;
        //    achievement.EndDate = range.EndDate;

        //    //không cho cán bộ phụ trách null
        //    if (achievement.YouthUnionMemberStaffID == 0)
        //        ModelState.AddModelError("YouthUnionMemberStaffID", "Cán bộ phụ trách không thể để trống");
        //    if (ModelState.IsValid)
        //    {

        //        if (fileStream != null)
        //        {
        //            var path = HttpContext.Server.MapPath(ConstraintName.DOCUMENT_ACTIVITY_DIRECTORY_PATH) + fileStream.FileName;
        //            fileStream.SaveAs(path);
        //            achievement.Document = fileStream.FileName;
        //        }

        //        achievementService.Update(achievement);
        //        return RedirectToAction("Index");
        //    }
        //    //Bổ sung ngày hoạt động

        //    ViewBag.StartDate = achievement.StartDate.ToString("dd/MM/yyyy");
        //    ViewBag.EndDate = achievement.EndDate.ToString("dd/MM/yyyy");

        //    ViewBag.UnitID = new SelectList(new UnitService().GetAll(), "ID", "FunctionName", achievement.UnitID);
        //    ViewBag.YouthUnionMemberStaffID = new SelectList(new LeaderService().GetAllViewByUnitID(achievement.UnitID)
        //                                            .Select(x => new
        //                                            {
        //                                                ID = x.MemberID,
        //                                                FunctionName = x.Lastname + " " + x.Firstname + " - " + x.YouthUnionMemberCode
        //                                            }),
        //                                            "ID", "FunctionName", achievement.YouthUnionMemberStaffID);
        //    return View(achievement);
        //}


        //// GET: /YouthUnionUniversity/Achievement/Delete/5
        //public ActionResult Delete(int? facultyID)
        //{
        //    if (facultyID == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Achievement achievement = achievementService.GetSingleById(facultyID);
        //    if (achievement == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    achievementService.Delete(facultyID);
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                awardLevelService.Dispose();
                schoolYearService.Dispose();
                memberService.Dispose();

                achievementService.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}