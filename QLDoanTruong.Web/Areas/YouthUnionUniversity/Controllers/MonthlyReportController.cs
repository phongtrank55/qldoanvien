﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QLDoanTruong.Model;
using QLDoanTruong.Model.ViewModel;
using QLDoanTruong.Service;
using QLDoanTruong.Common;
using System.Net;
using QLDoanTruong.Web.Common;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;

namespace QLDoanTruong.Web.Areas.YouthUnionUniversity.Controllers
{
    //[AllowUser]
    public class MonthlyReportController : Controller
    {
        private MonthlyReportExpireDateService reportExpireDateService = new MonthlyReportExpireDateService();
        private MonthlyReportService reportService = new MonthlyReportService();
        private MonthlyReportDetailService detailService = new MonthlyReportDetailService();

        public MonthlyReportController()
            : base()
        {
            ViewBag.BreadCrumb = new List<BreadcrumbLink>()
            {
                new BreadcrumbLink(){Link="/Home/Dashboard/", Title = "Dashboard"},
            };
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/MonthlyReport/", Title = "Báo cáo hoạt động" });
        }
        //
        // GET: /YouthUnionUniversity/ReportMonth/
        [AllowPermission(RoleID = "VIEW_REPORT")]

        
        [HttpGet, ActionName("Index")]
        public ActionResult Index(int? month, int? year)
        {
            month = month ?? DateTime.Now.Month;
            year = year ?? DateTime.Now.Year;

            ViewBag.Year = new SelectList(CommonList.GetYears(), "ID", "Name", year);
            ViewBag.Month = new SelectList(CommonList.Months, "ID", "Name", month);
            ViewBag.ExpireDate = reportExpireDateService.GetSingleByMonthAndYear(month.Value, year.Value);
            var model = reportService.GetSentViewByMonth(month.Value, year.Value);
       
            return View(model);
        }

        
        public PartialViewResult GetRankTable(int month, int year)
        {

            var model = new MonthlyReportService().GetDisplayViewAllFaculty(month, year);

            return PartialView("_RankTablePartial", model);
        }


        [AllowPermission(RoleID = "VIEW_REPORT")]
        public ActionResult SetupExpireDate(int? id)
        {
           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var expireDate = reportExpireDateService.GetSingleById(id);

            if (expireDate == null)
            {
                return HttpNotFound();
            }
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thiết lập thời hạn "+string.Format("{0:00}/{1}", expireDate.Month, expireDate.Year) });
        
            return View(expireDate);
        }
        
        [HttpPost, ActionName("SetupExpireDate")]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitExpireDate(MonthlyReportExpireDate expireDate)
        {
            if (expireDate.IsEnabledCutOffDate)
            {
                if (expireDate.CutOffDate == null)
                    ModelState.AddModelError("CutOffDate", "Ngày cắt không thể để trống!");
                else if (expireDate.DueDate >= expireDate.CutOffDate)
                    ModelState.AddModelError("CutOffDate", "Ngày cắt phải lớn hơn ngày hết hạn!");
            }

            if (ModelState.IsValid)
            {
                reportExpireDateService.Update(expireDate);
                reportService.UpdateRank(expireDate.Month, expireDate.Year);
                return RedirectToAction("Index", new {month=expireDate.Month, year= expireDate.Year});
            }
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thiết lập thời hạn " + string.Format("{0:00}/{1}", expireDate.Month, expireDate.Year) });
        

            return View(expireDate);
            
        }
        //GET: /YouthUnionUniversity/ReportMonth/Details/5
        [AllowPermission(RoleID = "VIEW_REPORT")]

        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var report = reportService.GetSingleById(id);

            if (report == null)
            {
                return HttpNotFound();
            }

            ViewBag.SubmitReport = new UserService().GetPermissionByUsername(Session["Username"].ToString()).Contains("SUBMIT_REPORT");

            ViewBag.MonthReportID = report.ID;
            ViewBag.SentDate = report.SentDate.Value.ToString("HH:mm dd/MM/yyyy");
            ViewBag.FacultyName = report.Unit.Name;
            ViewBag.Month = report.Month;
            ViewBag.Year = report.Year;
            ViewBag.Rank = report.Rank == null ? "Chưa xếp hạng" : report.Rank.ToString();
            ViewBag.OtherPoint = report.OtherPoint;
            ViewBag.Comment = report.Comment;

            var detail = detailService.GetAllByMonthlyReport(id);

            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = ViewBag.FacultyName });
        
            return View(detail);
        }

        [AllowPermission(RoleID = "SUBMIT_REPORT")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Submit(List<MonthlyReportDetail> details, long monthReportID, float otherPoint=0, string comment="")
        {
            foreach (var item in details)
            {
                if (item.Submited == true)
                {
                    detailService.SubmitPoint(item.ActivityID, monthReportID, item.PointSubmit);
                }
            }
            
            var report = reportService.GetSingleById(monthReportID);
            report.Comment = comment;
            report.OtherPoint = otherPoint;
            reportService.Update(report);
            
            reportService.UpdateRank(report.Month, report.Year);

            return RedirectToAction("Index", new { month=report.Month, year=report.Year});
        }


        

        [AllowPermission(RoleID = "VIEW_REPORT")]
        public ActionResult Export(int month, int year)
        {
            var workbook = new XSSFWorkbook();
            //xuất rank table
            
                var sheetStart = workbook.CreateSheet("Bảng xếp hạng");
                //make a header row  /
                string[] headers1 ={"Hạng", "Đơn vị", "Điểm"};

                //merge

                //ExportExcelHelper.SetFontCell(ref cell, workbook, 14, true);

                sheetStart.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 0, headers1.Length - 1));

                IRow row = sheetStart.CreateRow(0);
                var cell = row.CreateCell(0);
                cell.SetCellValue(string.Format("Bảng xếp hạng hoạt động {0:00}/{1}", month, year));

                row = sheetStart.CreateRow(2);

                for (int j = 0; j < headers1.Length; j++)
                {
                    cell = row.CreateCell(j);
                    ExportExcelHelper.SetStyleCellHeaderTable(ref cell, workbook);

                    cell.SetCellValue(headers1[j]);
                }

                //loops through data  
                var ranks = reportService.GetDisplayViewAllFaculty(month, year);
         
                int rowStart = 3;
                foreach (var mem in ranks)
                {

                    IRow row2 = sheetStart.CreateRow(rowStart++);
                    string[] rowData = new string[]{mem.Submited==false?"---":mem.Rank.ToString(), mem.FacultyName, mem.Submited==false?"---":mem.PointSum.ToString() };

                    for (int j = 0; j < rowData.Length; j++)
                    {
                        cell = row2.CreateCell(j);
                        ExportExcelHelper.SetStyleCellContentTable(ref cell, workbook);
                        cell.SetCellValue(rowData[j]);
                    }
                }
            
            
            //Xuất báo cáo
            var model = reportService.GetSentViewByMonth(month, year);
            foreach (var report in model)
            {

                var sheet1 = workbook.CreateSheet(report.MonthlyReportCode);
                //make a header row  /
                string[] headers ={"STT", "Mã hoạt động", "Tên hoạt động", "Ngày hoạt động", "Điểm tự chấm","Xác nhận",
                                  "Điểm xác nhận"};

                //merge

                //ExportExcelHelper.SetFontCell(ref cell, workbook, 14, true);

                sheet1.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 0, headers.Length - 1));

                IRow row1 = sheet1.CreateRow(0);
                var cell2 = row1.CreateCell(0);
                cell2.SetCellValue(string.Format("Báo cáo hoạt động {0}/{1}", month, year));

                row1 = sheet1.CreateRow(1);
                cell2 = row1.CreateCell(0);
                cell2.SetCellValue(report.FacultyName);

                row1 = sheet1.CreateRow(2);
                cell2 = row1.CreateCell(0);
                cell2.SetCellValue("Ngày gửi: " + report.SentDate.Value.ToString("H:mm dd/MM/yyyy"));

                row1 = sheet1.CreateRow(3);
                cell2 = row1.CreateCell(0);
                cell2.SetCellValue("Xếp hạng: " + (report.Rank == null ? "Chưa xếp hạng" : report.Rank.ToString()));


                row1 = sheet1.CreateRow(5);

                for (int j = 0; j < headers.Length; j++)
                {
                    cell2 = row1.CreateCell(j);
                    ExportExcelHelper.SetStyleCellHeaderTable(ref cell2, workbook);

                    cell2.SetCellValue(headers[j]);
                }

                //loops through data  
                var detail = detailService.GetAllByMonthlyReport(report.ID);

                int rowStart2 = 6;
                int stt2 = 1;
                foreach (var mem in detail)
                {

                    IRow row2 = sheet1.CreateRow(rowStart2++);
                    string[] rowData = new string[]{(stt2++).ToString(), mem.Activity.ActivityCode, mem.Activity.Name,
                            mem.DateActive.ToString("dd/MM/yyyy"), 
   
                            mem.PointSelf.ToString(), mem.Submited?"OK":string.Empty,mem.PointSubmit.ToString() };

                    for (int j = 0; j < rowData.Length; j++)
                    {
                        cell2 = row2.CreateCell(j);
                        ExportExcelHelper.SetStyleCellContentTable(ref cell2, workbook);
                        cell2.SetCellValue(rowData[j]);
                    }
                }
            }

            MemoryStream workStream = new MemoryStream();
            workbook.Write(workStream);
            return File(workStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Bao cao hoat dong thang" + month + "/" + year + ".xlsx");

        }
        //public FileResult ExportExcel(int month, int year)
        //{
        //    var workbook = new XSSFWorkbook();

        //    var sheet1 = workbook.CreateSheet("Sheet 1");


        //    //style cell2
        //    var defaultFont = workbook.CreateFont();
        //    defaultFont.FontHeight = 12;
        //    defaultFont.FontName = "Arial";
        //    defaultFont.Color = IndexedColors.Black.Index;
        //    defaultFont.IsBold = true;

        //    //make a header row  
        //    IRow row1 = sheet1.CreateRow(0);
        //    string[] headers ={"STT", "Mã SV/CB", "Họ", "Tên", "Ngày sinh", "Giới tính", 
        //                            "Lớp", "Liên chi đoàn", "Dân tộc","Tôn giáo","SĐT", "Email"};


        //    for (int j = 0; j < headers.Length; j++)
        //    {
        //        var cell2 = row1.CreateCell(j);
        //        cell2.CellStyle.Alignment = HorizontalAlignment.Center;
        //        cell2.CellStyle.SetFont(defaultFont);

        //        cell2.SetCellValue(headers[j]);
        //    }

        //    //loops through data  
        //    var members = memberService.FindBySearchViewModel(searchData);
        //    int rowStart = 1;
        //    foreach (var mem in members)
        //    {
        //        IRow row = sheet1.CreateRow(rowStart++);
        //        string[] rowData = new string[]{(rowStart-1).ToString(), mem.YouthUnionMemberCode, mem.Lastname, mem.Firstname, 
        //                       mem.DateOfBirth??string.Empty, mem.Gender==1?"Nam":"Nữ", 
        //                       mem.YouthUnionClass==null?string.Empty:mem.YouthUnionClass.Name,
        //                       mem.YouthUnionClass==null?"Đoàn trường":mem.YouthUnionClass.Unit.Name,
        //                       mem.Race.Name, mem.Religion.Name, 
        //                       mem.Phone??string.Empty, mem.Email??string.Empty  };

        //        for (int j = 0; j < rowData.Length; j++)
        //        {
        //            ICell cell2 = row.CreateCell(j);
        //            cell2.SetCellValue(rowData[j]);
        //        }
        //    }

        //    MemoryStream workStream = new MemoryStream();
        //    //     workStream.UnionPosition = 0;
        //    workbook.Write(workStream);
        //    return File(workStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Doan vien.xlsx");


        //}


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                reportService.Dispose();
                detailService.Dispose();
                reportExpireDateService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}