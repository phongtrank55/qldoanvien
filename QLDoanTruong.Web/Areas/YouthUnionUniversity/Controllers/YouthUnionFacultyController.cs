﻿using System.Web.Mvc;
using QLDoanTruong.Model;
using QLDoanTruong.Model.ViewModel;
using QLDoanTruong.Service;
using System.Net;
using QLDoanTruong.Web.Common;
using System;
using PagedList;
using System.Collections.Generic;
using QLDoanTruong.Common;

namespace QLDoanTruong.Web.Areas.YouthUnionUniversity.Controllers
{

    public class YouthUnionFacultyController : BaseController
    {
        public YouthUnionFacultyController():base()
        {
            
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/YouthUnionFaculty/", Title = "Đoàn Khoa/Viện" });
        }
        
        private YouthUnionFacultyService facultyService = new YouthUnionFacultyService();

        public override void AssignPermission()
        {
            ViewBag.VIEW_ALL_FACULTY = permissions.Contains("VIEW_ALL_FACULTY");
            ViewBag.VIEW_OWN_FACULTY = permissions.Contains("VIEW_OWN_FACULTY");
            ViewBag.MANAGE_FACULTY = permissions.Contains("MANAGE_FACULTY");
            ViewBag.VIEW_ALL_CLASS = permissions.Contains("VIEW_ALL_CLASS");
            ViewBag.VIEW_CLASS_OWN_FACULTY = permissions.Contains("VIEW_CLASS_OWN_FACULTY");
            ViewBag.MANAGE_CLASS = permissions.Contains("MANAGE_CLASS");
        }


        // GET: /YouthUnionUniversity/YouthUnionFaculty/
        [AllowPermission(RoleID = "VIEW_ALL_FACULTY|VIEW_OWN_FACULTY")]
        public ActionResult Index()
        {
            if (ViewBag.VIEW_ALL_FACULTY)
            {
                return View(facultyService.GetAll());
            }
            if (ViewBag.VIEW_OWN_FACULTY && currentFaculty == null)
            {
                ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                return View();
            }
            return Details(currentFaculty.ID);


        }


        // GET: /YouthUnionUniversity/YouthUnionFaculty/Create
        [AllowPermission(RoleID = "MANAGE_FACULTY&VIEW_ALL_FACULTY")]
        public ActionResult Create()
        {
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thêm Đoàn Khoa/viện" });
           
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,UnitCode,Name,Email")] Unit faculty)
        {
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thêm Đoàn Khoa/viện" });
           
            if (faculty.UnitCode != null && facultyService.ExistedFacultyCode(faculty.UnitCode))
            {
                ModelState.AddModelError("UnitCode", "Mã Liên chi đoàn đã tồn tại");
            }
            if (ModelState.IsValid)
            {
                faculty.Type = QLDoanTruong.Common.ConstraintUnitType.FACULTY;
                facultyService.Add(faculty);
                return RedirectToAction("Index");
            }
            return View(faculty);
        }

        // GET: /YouthUnionUniversity/YouthUnionFaculty/Edit/5
         [AllowPermission(RoleID = "MANAGE_FACULTY&VIEW_ALL_FACULTY")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var faculty = facultyService.GetSingleById(id);
            if (faculty == null)
            {
                return HttpNotFound();
            }
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật Đoàn Khoa/Viện" });
           
            return View(faculty);
        }

        // POST: /YouthUnionUniversity/YouthUnionFaculty/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,UnitCode,Name,Email")] Unit faculty)
        {

            if (faculty.UnitCode != null && facultyService.ExistedFacultyCode(faculty.UnitCode, faculty.ID))
            {
                ModelState.AddModelError("UnitCode", "Mã Liên chi đoàn đã tồn tại");
            }

            if (faculty.Email != null && facultyService.ExistedEmail(faculty.UnitCode, faculty.ID))
            {
                ModelState.AddModelError("Email", "Email này đã được sử dụng!");
            }
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật Đoàn Khoa/Viện" });
         

            if (ModelState.IsValid)
            {
                faculty.Type = QLDoanTruong.Common.ConstraintUnitType.FACULTY;
                facultyService.Update(faculty);
                return RedirectToAction("Index");
            }
            return View(faculty);
        }

        // POST: /YouthUnionUniversity/YouthUnionFaculty/Delete/5
        [HttpPost, ActionName("Delete")]

        public bool Delete(long? id)
        {
            if (id == null)
            {
                return false;
            }
            var faculty = facultyService.GetSingleById(id);
            if (faculty == null)
            {
                return false;
            }
            try
            {
                facultyService.Delete(faculty);
                return true;
            }
            catch
            {
                return false;
            }
        }
        [AllowPermission(RoleID = "VIEW_ALL_FACULTY")]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var faculty = facultyService.GetSingleViewById(id);
            if (faculty == null)
            {
                return HttpNotFound();
            }
            

            ViewBag.PositionID = new SelectList(new PositionService().GetUnionOfFaculty(), "ID", "Name");
            var achievements = new AchievementService().GetViewByUnionID(QLDoanTruong.Common.ConstraintUnionType.FACULTY, id);
            
            IEnumerable<YouthUnionClassViewModel> classes = null;
            if (ViewBag.VIEW_ALL_CLASS
                   || ViewBag.VIEW_CLASS_OWN_FACULTY && currentFaculty != null && currentFaculty.ID == id)
            {
                //nếu có quyền xem chi đoàn
                classes = new YouthUnionClassService().GetViewByFaculty(id.GetValueOrDefault());
            }
            if (ViewBag.VIEW_ALL_FACULTY)
            {
                ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = " /YouthUnionUniversity/YouthUnionFaculty/Details/"+id, Title = faculty.Name });
            }
            return View("Details", new Tuple<YouthUnionFacultyViewModel, IEnumerable<YouthUnionClassViewModel>, IEnumerable<AchievementViewModel>>(faculty, classes, achievements));
        }

        [AllowPermission(RoleID = "MANAGE_FACULTY")]
        public ActionResult AddLeader(long? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (!ViewBag.VIEW_ALL_FACULTY && ViewBag.VIEW_OWN_FACULTY)
            {
                if (currentFaculty == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                    return View();
                }
                if (currentFaculty.ID != id)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }
            }

            var faculty = (!ViewBag.VIEW_ALL_FACULTY && ViewBag.VIEW_OWN_FACULTY) ? currentFaculty : facultyService.GetSingleById(id);

            if (faculty == null)
            {
                return HttpNotFound();
            }
            //breadcrumb
            if (ViewBag.VIEW_ALL_FACULTY)
            {
                ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = " /YouthUnionUniversity/YouthUnionFaculty/Details/" + id, Title = faculty.Name });
            }
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = " ", Title = "Bổ nhiệm cán bộ" });


            ViewBag.PositionID = new SelectList(new PositionService().GetUnionOfFaculty(), "ID", "Name");
            ViewBag.FacultyName = faculty.Name;
            ViewBag.FacultyID = faculty.ID;
            return View();
        }

        //[HttpPost]
        //public ActionResult AddLeader(long? id, string search)
        //{

        //    //var faculty = facultyService.GetSingleById(id);
        //    //ViewBag.PositionID = new SelectList(new PositionService().GetAll(), "ID", "Name", 1);
        //    //ViewBag.FacultyName = faculty.Name;
        //    //ViewBag.FacultyID = faculty.ID;
        //    return View();
        //}


        public PartialViewResult SearchMemberForLeader(int? page, long id, string search)
        {
            ViewBag.ID = id;
            ViewBag.Search = search;

            int pageSize = 12;

            int pageNumber = (page ?? 1);
            search = search.Trim().ToLower();

            if (string.IsNullOrEmpty(search))
            {
                return PartialView("_SearchMemberPartial");
            }

            var result = new YouthUnionMemberService().GetMemberNotLeaderFaculty(id, search);

            return PartialView("_SearchMemberPartial", result.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddLeaderForFaculty(LeaderUnit leader)
        {
            new LeaderUnitService().Add(leader);
            return RedirectToAction("AddLeader", new { id = leader.UnitID });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditLeaderForFaculty(int leaderID, int positionID)
        {
            var leaderService = new LeaderUnitService();
            var leader = leaderService.GetSingleById(leaderID);
            leader.PositionID = positionID;
            leaderService.Update(leader);

            return RedirectToAction("AddLeader", new { id = leader.UnitID });
        }
        public PartialViewResult GetLeaders(long id)
        {
            var leaders = new LeaderUnitService().GetAllViewByUnitID(id);
            return PartialView("_LeaderListPartial", leaders);
        }

        [HttpPost]
        public bool DeletePosition(long? id)
        {
            if (id == null)
            {
                return false;
            }
            var leaderService = new LeaderUnitService();
            var leader = leaderService.GetSingleById(id);

            if (leader == null)
            {
                return false;
            }
            try
            {
                leaderService.Delete(id.GetValueOrDefault());
                return true;
            }
            catch
            {
                return false;
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                facultyService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
