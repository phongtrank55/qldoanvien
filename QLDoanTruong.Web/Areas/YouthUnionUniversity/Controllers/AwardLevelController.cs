﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QLDoanTruong.Model;
using QLDoanTruong.Service;
using QLDoanTruong.Web.Common;
using QLDoanTruong.Common;

namespace QLDoanTruong.Web.Areas.YouthUnionUniversity
{
    [AllowPermission(RoleID = "VIEW_AWARD")]
    public class AwardLevelController : Controller
    {
        private AwardLevelService awardLevelService = new AwardLevelService();

        public AwardLevelController()
        {
            ViewBag.BreadCrumb = new List<BreadcrumbLink>()
            {
                new BreadcrumbLink(){Link="/Home/Dashboard/", Title = "Dashboard"},
            };
            //BREADCRUMB
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/AwardLevel/", Title = "Cấp bậc khen thưởng" });
        }
        //
        // GET: /YouthUnionUniversity/AwardLevel/
        public ActionResult Index(bool?update=null)
        {
            
            if (update != null)
            {
                ViewBag.Success = update;
            }
            return View("Index", awardLevelService.GetAll());
        
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(AwardLevel level)
        {
            ViewBag.Success = false;
            try
            {
                if (level.ID == 0)
                    awardLevelService.Add(level);
                else
                    awardLevelService.Update(level);
                ViewBag.Success = true;
            }
            catch { }
            return RedirectToAction("Index", new { update = ViewBag.Success });

        }

        [HttpPost]
        public bool Delete(int? id)
        {
            if (id == null) return false;
            var level = awardLevelService.GetSingleById(id);
            if (level == null) return false;
            try
            {
                awardLevelService.Delete(level);
                return true;
            }
            catch { }
            return false;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                awardLevelService.Dispose();
            }
            base.Dispose(disposing);
        }


	}
}