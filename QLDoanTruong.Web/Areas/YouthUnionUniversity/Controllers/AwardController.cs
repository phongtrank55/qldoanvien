﻿using System.Web.Mvc;
using QLDoanTruong.Model;
using QLDoanTruong.Service;
using QLDoanTruong.Common;
using System.Net;
using QLDoanTruong.Web.Common;
using System.Collections.Generic;

namespace QLDoanTruong.Web.Areas.YouthUnionUniversity.Controllers
{
    [AllowPermission(RoleID="VIEW_AWARD")]
    public class AwardController : Controller
    {
        private AwardLevelService awardLevelService = new AwardLevelService();
        private AwardService awardService = new AwardService();
        public AwardController()
        {
            ViewBag.BreadCrumb = new List<BreadcrumbLink>()
            {
                new BreadcrumbLink(){Link="/Home/Dashboard/", Title = "Dashboard"},
            };
            //BREADCRUMB
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/Award/", Title = "Danh hiệu" });
        }

        //GET: /YouthUnionUniversity/Award/
        public ActionResult Index()
        {
            ViewBag.AwardLevelID = new SelectList(awardLevelService.GetAll(), "ID", "Name");
            ViewBag.Type = new SelectList(CommonList.ListUnionType, "ID", "Name");
            return View(awardService.GetAll());
        }

        [HttpPost, ActionName("Index")]
        [ValidateAntiForgeryToken]
        public ActionResult SearchAward(int? type, int? awardLevelID)
        {
            ViewBag.AwardLevelID = new SelectList(awardLevelService.GetAll(), "ID", "Name", awardLevelID);
            ViewBag.Type = new SelectList(CommonList.ListUnionType, "ID", "Name", type);

            return View(awardService.GetByAwardLevelAndType(awardLevelID, type));

        }
        // GET: /YouthUnionUniversity/Award/Create
        public ActionResult Create()
        {
            ViewBag.AwardLevelID = new SelectList(awardLevelService.GetAll(), "ID", "Name");
            ViewBag.Type = new SelectList(CommonList.ListUnionType, "ID", "Name", ConstraintUnionType.MEMBER);
            //BREADCRUMB
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thêm Danh hiệu" });
            return View();
        }

        // POST: /YouthUnionUniversity/Award/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Award award)
        {
            //BREADCRUMB
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thêm Danh hiệu" });
            
            ViewBag.AwardLevelID = new SelectList(awardLevelService.GetAll(), "ID", "Name", award.AwardLevelID);
            ViewBag.Type = new SelectList(CommonList.ListUnionType, "ID", "Name", award.Type);

            ViewBag.Success = false;
            if (ModelState.IsValid)
            {

                awardService.Add(award);
                ViewBag.Success = true;
                return View();
            }
          
            return View(award);
        }


        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Award award = awardService.GetSingleById(id);

            if (award == null)
            {
                return HttpNotFound();
            }
            //BREADCRUMB
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật Danh hiệu" });
            
            ViewBag.AwardLevelID = new SelectList(awardLevelService.GetAll(), "ID", "Name", award.AwardLevelID);
            ViewBag.Type = new SelectList(CommonList.ListUnionType, "ID", "Name", award.Type);

            return View(award);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Award award)
        {
            //BREADCRUMB
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật Danh hiệu" });
           
            ViewBag.AwardLevelID = new SelectList(awardLevelService.GetAll(), "ID", "Name", award.AwardLevelID);
            ViewBag.Type = new SelectList(CommonList.ListUnionType, "ID", "Name", award.Type);
            ViewBag.Success = false;

            //Bổ sung ngày hoạt động
            if (ModelState.IsValid)
            {
                awardService.Update(award);
                ViewBag.Success = true;
                return View();
            }
            
            return View(award);
        }

        public bool Delete(long? id)
        {
            if (id == null)
            {
                return false;
            }
            var award = awardService.GetSingleById(id);
            if (award == null)
            {
                return false;
            }
            try
            {
                awardService.Delete(award);
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                awardService.Dispose();
                awardLevelService.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}