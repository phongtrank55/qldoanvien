﻿using System.Web.Mvc;
using System.Linq;
using QLDoanTruong.Service;
using QLDoanTruong.Common;
using QLDoanTruong.Model;
using QLDoanTruong.Model.ViewModel;
using System.Net;
using PagedList;
using System.Collections.Generic;
using System;
using QLDoanTruong.Web.Common;

namespace QLDoanTruong.Web.Areas.YouthUnionUniversity.Controllers
{
    public class YouthUnionClassController : BaseController
    {
        private YouthUnionClassService classService = new YouthUnionClassService();

        private SchoolYearService schoolYearService = new SchoolYearService();
        private YouthUnionMemberService memberService = new YouthUnionMemberService();
        private LeaderClassService leaderClassService = new LeaderClassService();
        private YouthUnionFacultyService facultyService = new YouthUnionFacultyService();
        public YouthUnionClassController()
            : base()
        {

            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/YouthUnionClass/", Title = "Chi đoàn" });
        }
        public override void AssignPermission()
        {
            ViewBag.VIEW_OWN_CLASS = permissions.Contains("VIEW_OWN_CLASS");
            ViewBag.VIEW_ALL_CLASS = permissions.Contains("VIEW_ALL_CLASS");
            ViewBag.VIEW_CLASS_OWN_FACULTY = permissions.Contains("VIEW_CLASS_OWN_FACULTY");
            ViewBag.MANAGE_CLASS = permissions.Contains("MANAGE_CLASS");
            ViewBag.VIEW_MEMBER_OWN_FACULTY = permissions.Contains("VIEW_MEMBER_OWN_FACULTY");
            ViewBag.MANAGE_MEMBER = permissions.Contains("MANAGE_MEMBER");
            ViewBag.VIEW_ALL_MEMBER = permissions.Contains("VIEW_ALL_MEMBER");
            ViewBag.VIEW_MEMBER_OWN_CLASS = permissions.Contains("VIEW_MEMBER_OWN_CLASS");
        }

        //
        // GET: /YouthUnionUniversity/YouthUnionClass/
        [AllowPermission(RoleID = "VIEW_ALL_CLASS|VIEW_CLASS_OWN_FACULTY|VIEW_OWN_CLASS")]
        public ActionResult Index()
        {
            if (!ViewBag.VIEW_ALL_CLASS && !ViewBag.VIEW_CLASS_OWN_FACULTY && ViewBag.VIEW_OWN_CLASS) 
            {
                if (currentClass == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Chi đoàn trong hệ thống!";
                    return View();
                }
                return Details(currentClass.ID);

            }

            ViewBag.AcademicYearID = new SelectList(new AcademicYearService().GetAll(), "ID", "Name");

            ViewBag.Type = new SelectList(CommonList.ListUnionClassType, "ID", "Name", ConstraintUnionClassType.STUDENT);

            if (ViewBag.VIEW_ALL_CLASS)
            {
                ViewBag.FacultyID = new SelectList(facultyService.GetAll(), "ID", "Name");
            }
            else if (ViewBag.VIEW_CLASS_OWN_FACULTY && currentFaculty == null) // nếu quản lý cấp khoa mà k có thông tin về khoa mình thì báo lỗi
            {
                ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
            }
            
            return View();
        }
        

        [HttpPost]
        public ActionResult Index(long? facultyID, long? academicYearID, long? type)
        {
            ViewBag.Type = new SelectList(CommonList.ListUnionClassType, "ID", "Name", type);
            if (type == ConstraintUnionClassType.STUDENT)
            {
                ViewBag.AcademicYearID = new SelectList(new AcademicYearService().GetAll(), "ID", "Name", academicYearID);
            }
            if (ViewBag.VIEW_ALL_CLASS)
            {
                ViewBag.FacultyID = new SelectList(facultyService.GetAll(), "ID", "Name", facultyID);
            }
            else if (ViewBag.VIEW_CLASS_OWN_FACULTY && currentFaculty == null) // nếu quản lý cấp khoa mà k có thông tin về khoa mình thì báo lỗi
            {
                ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
            }
            return View();
        }

        public PartialViewResult GetClasses(int? page, long? facultyID, long? academicYearID, long? type = ConstraintUnionClassType.STUDENT)
        {

            if (!ViewBag.VIEW_ALL_CLASS && ViewBag.VIEW_CLASS_OWN_FACULTY && facultyID == null)
                facultyID = currentFaculty.ID;
            if (type == ConstraintUnionClassType.LECTURE)
            {
                academicYearID = null;
            }
            ViewBag.FacultyID = facultyID;
            ViewBag.AcademicYearID = academicYearID;
            ViewBag.Type = type;
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var classes = classService.GetViewByFacultyAndAcademicYear(facultyID, academicYearID, type);

            return PartialView("_ClassesListPartial", classes.ToPagedList(pageNumber, pageSize));
        }

        // GET: /YouthUnionUniversity/YouthUnionClass/Create
        [AllowPermission(RoleID = "MANAGE_CLASS")]
        public ActionResult Create(int? facultyID)
        {

            if (!ViewBag.VIEW_ALL_CLASS && !ViewBag.VIEW_CLASS_OWN_FACULTY)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thêm Chi đoàn" });
           
            if (ViewBag.VIEW_ALL_CLASS)
            {
                ViewBag.FacultyID = new SelectList(facultyService.GetAll(), "ID", "Name", facultyID);
            }
            else if (ViewBag.VIEW_CLASS_OWN_FACULTY)
            {
                if (currentFaculty == null)// nếu quản lý cấp khoa mà k có thông tin về khoa mình thì báo lỗi
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                    return View();
                }
                else if (currentFaculty.ID != facultyID)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }

            }

           
            ViewBag.AcademicYearID = new SelectList(new AcademicYearService().GetAll(), "ID", "Name");

            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(YouthUnionClass youthClass)
        {

            ViewBag.AcademicYearID = new SelectList(new AcademicYearService().GetAll(), "ID", "Name", youthClass.AcademicYearID);
            if (ViewBag.VIEW_ALL_CLASS)
            {
                ViewBag.FacultyID = new SelectList(facultyService.GetAll(), "ID", "Name", youthClass.FacultyID);
            }
            else if (ViewBag.VIEW_CLASS_OWN_FACULTY) // nếu quản lý cấp khoa mà k có thông tin về khoa mình thì báo lỗi
            {
                if (currentFaculty == null)
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                else
                    youthClass.FacultyID = currentFaculty.ID;
            }

            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Thêm Chi đoàn" });
           
            ViewBag.Success = false;
            if (youthClass.YouthUnionClassCode != null && classService.ExistedClassCode(youthClass.YouthUnionClassCode))
            {
                ModelState.AddModelError("YouthUnionClassCode", "Mã Chi đoàn đã tồn tại");
            }

            if (ModelState.IsValid)
            {
                youthClass.Type = 1;//CĐ SV 
                classService.Add(youthClass);
                ViewBag.Success = true;
                return View();
            }


            return View(youthClass);
        }

        // GET: /YouthUnionUniversity/YouthUnionClass/Edit/5
        [AllowPermission(RoleID = "MANAGE_CLASS")]
        public ActionResult Edit(long? id)
        {


            if (!ViewBag.VIEW_ALL_CLASS && !ViewBag.VIEW_CLASS_OWN_FACULTY)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var youthClass = classService.GetSingleById(id);
            if (youthClass == null)
            {
                return HttpNotFound();
            }
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật Chi đoàn" });
           
            //ViewBag.SecretaryID = new SelectList(new YouthUnionMemberService().GetByClass(youthClass.ID).Select(x => new
            //{
            //    ID = x.ID,
            //    Name = x.Lastname + " " + x.Firstname
            //}), "ID", "Name", youthClass.SecretaryID);

            ViewBag.AcademicYearID = new SelectList(new AcademicYearService().GetAll(), "ID", "Name", youthClass.AcademicYearID);
            if (ViewBag.VIEW_ALL_CLASS)
            {
                ViewBag.FacultyID = new SelectList(facultyService.GetAll(), "ID", "Name", youthClass.FacultyID);
            }
            else if (ViewBag.VIEW_CLASS_OWN_FACULTY) // nếu quản lý cấp khoa mà k có thông tin về khoa mình thì báo lỗi
            {
                if (currentFaculty == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                    return View();
                }
                else if (currentFaculty.ID != youthClass.ID)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }

            }

            return View(youthClass);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Edit(YouthUnionClass youthClass)
        {
         
            //ViewBag.SecretaryID = new SelectList(new YouthUnionMemberService().GetByClass(youthClass.ID).Select(x => new
            //{
            //    ID = x.ID,
            //    Name = x.Lastname + " " + x.Firstname
            //}), "ID", "Name", youthClass.ID);
            ViewBag.AcademicYearID = new SelectList(new AcademicYearService().GetAll(), "ID", "Name", youthClass.AcademicYearID);
            if (ViewBag.VIEW_ALL_CLASS)
            {
                ViewBag.FacultyID = new SelectList(facultyService.GetAll(), "ID", "Name", youthClass.FacultyID);
            }
            else if (ViewBag.VIEW_CLASS_OWN_FACULTY) // nếu quản lý cấp khoa mà k có thông tin về khoa mình thì báo lỗi
            {
                if (currentFaculty == null)
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                else
                    youthClass.FacultyID = currentFaculty.ID;
            }

            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật Chi đoàn" });
           
            ViewBag.Success = false;
            if (youthClass.YouthUnionClassCode != null && classService.ExistedClassCode(youthClass.YouthUnionClassCode, youthClass.ID))
            {
                ModelState.AddModelError("YouthUnionClassCode", "Mã Chi đoàn đã tồn tại");
            }

            if (ModelState.IsValid)
            {
                classService.Update(youthClass);
                ViewBag.Success = true;

            }
            return View(youthClass);
        }

        [AllowPermission(RoleID = "VIEW_ALL_CLASS|VIEW_CLASS_OWN_FACULTY")]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var youthClass = classService.GetSingleViewById(id);

            if (youthClass == null)
            {
                return HttpNotFound();
            }
            if (ViewBag.VIEW_CLASS_OWN_FACULTY) // nếu quản lý cấp khoa mà k có thông tin về khoa mình thì báo lỗi
            {
                if (currentFaculty == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                    return View();
                }
                 if (currentFaculty.ID != youthClass.FacultyID)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }
            }
            IEnumerable<YouthUnionMember> members = null;
            if (ViewBag.VIEW_ALL_MEMBER
                    || ViewBag.VIEW_MEMBER_OWN_FACULTY && currentFaculty != null && youthClass.FacultyID == currentFaculty.ID
                     || ViewBag.VIEW_MEMBER_OWN_CLASS && currentClass != null && currentClass.ID == youthClass.ID)
            {
                //Nếu có quyền xem Đoàn viên
                members = memberService.GetByClass(youthClass.ID);
            }
            //ViewBag.UnionLeaders = memberService.GetLeaderClass(youthClass.ID, QLDoanTruong.Common.ConstraintPositionType.UNION);
            //ViewBag.ClassLeaders = memberService.GetLeaderClass(youthClass.ID, QLDoanTruong.Common.ConstraintPositionType.CLASS);
            var achievements = new AchievementService().GetViewByUnionID(QLDoanTruong.Common.ConstraintUnionType.CLASS, id);
            var currentSchoolYear = schoolYearService.GetCurrent();
            ViewBag.SchoolYearID = new SelectList(schoolYearService.GetAll(), "ID", "Name", currentSchoolYear.ID);
            ViewBag.ClassID = id;
            ViewBag.CurrentSchoolYearID = currentSchoolYear.ID;
            

            //breadcrumb
            if (ViewBag.VIEW_ALL_CLASS)
            {
                ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/YouthUnionClass/Details/" + id, Title = youthClass.Name });
            }
            return View("Details", new Tuple<YouthUnionClassViewModel, IEnumerable<YouthUnionMember>, IEnumerable<AchievementViewModel>>(youthClass, members, achievements));

        }

        [HttpPost]
        public JsonResult GetPosition(long schoolYearID, long memberID)
        {
            return Json(leaderClassService
                            .GetPositionForClass(memberID, schoolYearID)
                            .Select(x => new { x.ID, x.Name }), JsonRequestBehavior.AllowGet);
        }

        [AllowPermission(RoleID = "MANAGE_CLASS")]
        public ActionResult AddLeader(long? classID, long? schoolYearID, bool? edit=null)
        {
            if (edit != null)
                ViewBag.Success = edit;

            if (classID == null || schoolYearID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!ViewBag.VIEW_ALL_CLASS)
            {

                if (ViewBag.VIEW_CLASS_OWN_FACULTY && currentFaculty == null)
                {
                    ViewBag.ErrorMessage = "Bạn chưa có thông tin về Đoàn Viện/Khoa trong hệ thống!";
                    return View();
                }
                if (ViewBag.VIEW_OWN_CLASS)
                {
                    if (currentClass == null)
                    {
                        ViewBag.ErrorMessage = "Bạn chưa có thông tin về Chi đoàn trong hệ thống!";
                        return View();
                    }
                    if (currentClass.ID != classID)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                    }
                }
            }
            var youthClass = (ViewBag.VIEW_ALL_CLASS || ViewBag.VIEW_CLASS_OWN_FACULTY) ? classService.GetSingleById(classID) : currentClass;

            if (youthClass == null)
            {
                return HttpNotFound();
            }
            if (!ViewBag.VIEW_ALL_CLASS && ViewBag.VIEW_CLASS_OWN_FACULTY && youthClass.FacultyID != currentFaculty.ID)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            var schoolYear = schoolYearService.GetSingleById(schoolYearID);
            if (schoolYearID == null)
            {
                return HttpNotFound();
            }

            ViewBag.ClassName = youthClass.Name;
            ViewBag.SchoolYearID = new SelectList(schoolYearService.GetAll(), "ID", "Name", schoolYearID);
            ViewBag.ClassID = classID;
            ViewBag.CurrentSchoolYearID = schoolYearID;
            
            var members = memberService.GetByClass(youthClass.ID);

            //Breadcrumb
            if (ViewBag.VIEW_ALL_CLASS)
            {
                ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/YouthUnionClass/Details/" + classID, Title = youthClass.Name });
            }
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = " ", Title = "Bổ nhiệm cán bộ" });
            //return View(new Tuple<IEnumerable<LeaderClassViewModel>, IEnumerable<YouthUnionMember>>(leaders, members));
            return View(members);
        }

        
        public PartialViewResult GetLeader(long classID, long schoolYearID, string page="Detail")
        {
            ViewBag.Page = page;
            var leaders = new LeaderClassService().GetViewBySchoolYearAndClass(schoolYearID, classID);
            return PartialView("_LeaderListPartial", leaders);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddLeader(LeaderClass leader)
        {
            ViewBag.Success = false;
            //ViewBag.ClassName = classService.GetSingleById(leader.ClassID).Name;
            //ViewBag.SchoolYearID = new SelectList(schoolYearService.GetAll(), "ID", "Name", leader.SchoolYearID);
            //ViewBag.ClassID = leader.ClassID;
           //var leaders = new LeaderClassService().GetViewBySchoolYearAndClass(leader.SchoolYearID, leader.ClassID);
           // var members = memberService.GetByClass(leader.ClassID);
            try
            {
                leaderClassService.Add(leader);
                ViewBag.Success = true;
            }
            catch { }
            //Breadcrumb
            //if (ViewBag.VIEW_ALL_CLASS)
            //{
            //    ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/YouthUnionUniversity/YouthUnionClass/Details/" + leader.ClassID, Title = ViewBag.ClassName });
            //}
            //ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = " ", Title = "Bổ nhiệm cán bộ" });
            return RedirectToAction("AddLeader", new { classID = leader.ClassID, schoolYearID = leader.SchoolYearID, edit = ViewBag.Success});
        }

        
        [HttpPost]
        public bool DeleteLeader(long? id)
        {
            if (id == null)
            {
                return false;
            }

            var leader = leaderClassService.GetSingleById(id);

            if (leader == null)
            {
                return false;
            }
            try
            {
                leaderClassService.Delete(id.GetValueOrDefault());
                return true;
            }
            catch
            {
                return false;
            }
        }
        [HttpPost]
        [AllowPermission(RoleID = "MANAGE_CLASS")]
        public bool Delete(long? id)
        {
            if (id == null)
            {
                return false;
            }
            var youthClass = classService.GetSingleById(id);

            if (youthClass == null)
            {
                return false;
            }


            if (youthClass.Type == QLDoanTruong.Common.ConstraintUnionClassType.LECTURE)
            {
                return false;
            }

            try
            {
                classService.Delete(youthClass);
                return true;
            }
            catch
            {
                return false;
            }
        }

        [AllowPermission(RoleID = "MANAGE_MEMBER")]
        public ActionResult RemoveMember(long? memberID)
        {
            if (memberID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YouthUnionMember member = null;
            using (var memberService = new YouthUnionMemberService())
            {
                member = memberService.GetSingleById(memberID);
                if (member == null)
                {
                    return HttpNotFound();
                }
                memberService.Delete(member);
            }
            return RedirectToAction("Details", new { id = member.YouthUnionClassID });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                leaderClassService.Dispose();
                memberService.Dispose();
                schoolYearService.Dispose();
                classService.Dispose();
                facultyService.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}