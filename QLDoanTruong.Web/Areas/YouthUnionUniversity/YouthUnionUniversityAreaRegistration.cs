﻿using System.Web.Mvc;

namespace QLDoanTruong.Web.Areas.YouthUnionUniversity
{
    public class YouthUnionUniversityAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "YouthUnionUniversity";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            //context.MapRoute("Download")
            //context.MapRoute(
            //     "LCD_default",
            //    "YouthUnionUniversity/{controller}/{action}/{id}",
            //    new { action = "Index", id = UrlParameter.Optional }

            //    );
            
            context.MapRoute(
                "YouthUnionUniversity_default",
                "YouthUnionUniversity/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }


            );
        }
    }
}