﻿using System.Linq;
using System.Web.Mvc;
using QLDoanTruong.Service;
using QLDoanTruong.Model;
using QLDoanTruong.Model.ViewModel;
using PagedList;
using QLDoanTruong.Common;
using System.Net;
using QLDoanTruong.Web.Common;
using System.Collections.Generic;

namespace QLDoanTruong.Web.Areas.Admin.Controllers
{
    [AllowPermission(RoleID = "MANAGE_ACCOUNT")]
    public class AccountController : Controller
    {
        private YouthUnionMemberService memberService = new YouthUnionMemberService();
        private GroupUserService groupService = new GroupUserService();
        private UserService userService = new UserService();

        public AccountController()
        {
            ViewBag.BreadCrumb = new List<BreadcrumbLink>()
            {
                new BreadcrumbLink(){Link="/Home/Dashboard/", Title = "Dashboard"},
            };
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/Admin/Account/", Title = "Tài khoản" });
        }

        //
        // GET: /Admin/Account/
        public ActionResult Index()
        {
            ViewBag.GroupUserID = new SelectList(groupService.GetAll().Select(x => new { x.ID, x.Name }), "ID", "Name");

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(int? groupUserID, string txtSearch)
        {

            ViewBag.GroupUserID = new SelectList(groupService.GetAll().Select(x => new { x.ID, x.Name }), "ID", "Name", groupUserID);
            ViewBag.TxtSearch = txtSearch;
            return View();
        }

        public PartialViewResult GetAccounts(int? page, int? groupUserID, string txtSearch)
        {
            ViewBag.GroupUserID = groupUserID;
            ViewBag.TxtSearch = txtSearch;

            int pageSize = 12;
            int pageNumber = (page ?? 1);
            return PartialView("_AccountListPartial", userService.GetViewByGroup(groupUserID, txtSearch)
                                    .ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public JsonResult GetClasses(long facultyID, int typeMember)
        {
            int typeClass = typeMember == ConstraintUnionMemberType.LECTURE ? ConstraintUnionClassType.LECTURE : ConstraintUnionClassType.STUDENT;
            return Json(new YouthUnionClassService().GetByFacultyAndType(facultyID, typeClass).Select(x => new { x.ID, x.Name }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public bool ActiveAccount(long id, bool isActive)
        {
            try
            {
                var user = userService.GetSingleById(id);
                user.Status = isActive;
                userService.Update(user);
                return true;
            }catch
            {
                return false;
            }
            
        }
        public ActionResult Create()
        {
            ViewBag.FacultyID = new SelectList(new YouthUnionFacultyService().GetAll(), "ID", "Name");
            ViewBag.GroupUserID = new SelectList(groupService.GetAll().Select(x => new { x.ID, x.Name }), "ID", "Name");
            ViewBag.Type = new SelectList(CommonList.ListMemberType, "ID", "Name", 0);
            ViewBag.RaceID = new SelectList(new RaceService().GetAll(), "ID", "Name");
            ViewBag.ReligionID = new SelectList(new ReligionService().GetAll(), "ID", "Name");
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/Admin/Account/Create", Title = "Thêm tài khoản" });
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AccountViewModel account)
        {
            if (account.YouthUnionMemberCode != null && memberService.ExistedMemberCode(account.YouthUnionMemberCode))
            {
                ModelState.AddModelError("YouthUnionMemberCode", "Mã SV/CB này đã tồn tại!");

            }
            if (account.Email != null && memberService.ExistedEmail(account.Email, null))
            {
                ModelState.AddModelError("Email", "Email này đã được đăng ký tài khoản khác!");
            }
            ViewBag.Success = false;
            if (ModelState.IsValid)
            {
                //nếu là cán bộ đoàn trường sẽ k cần lớp
                if (account.Type == ConstraintUnionMemberType.STAFF)
                    account.YouthUnionClassID = null;
                //Tách User và đoàn viên
                var user = userService.Add(new User()
                                    {
                                        Username = account.Username,
                                        Password = account.Password,
                                        GroupUserID = account.GroupUserID
                                    });
                memberService.Add(new YouthUnionMember()
                {
                    ID = user.ID,
                    YouthUnionClassID = account.YouthUnionClassID,
                    YouthUnionMemberCode = account.YouthUnionMemberCode,
                    Lastname = account.Lastname,
                    Firstname = account.Firstname,
                    Gender = account.Gender,
                    DateOfBirth = account.DateOfBirth,
                    Email = account.Email,
                    Type = account.Type.Value,
                    PlaceOfIssue = account.PlaceOfIssue,
                    CurrentAddress = account.CurrentAddress,
                    DateIntoParty = account.DateIntoParty,
                    DateIntoYouthUnion = account.DateIntoYouthUnion,
                    DateOfIssue = account.DateOfIssue,
                    RaceID = account.RaceID,
                    Photo = "default.PNG",
                    ReligionID = account.ReligionID,
                    RelativeName = account.RelativeName,
                    RelativePhone = account.RelativePhone,
                    YouthUnionCardNo = account.YouthUnionCardNo,
                    IDCardNo = account.IDCardNo,
                    HomeTown = account.HomeTown,

                });
                ModelState.Clear();
                ViewBag.Success = true;
                
                
            }
            ViewBag.FacultyID = new SelectList(new YouthUnionFacultyService().GetAll(), "ID", "Name", account.FacultyID);
            ViewBag.GroupUserID = new SelectList(groupService.GetAll().Select(x => new { x.ID, x.Name }), "ID", "Name", account.GroupUserID);
            ViewBag.Type = new SelectList(CommonList.ListMemberType, "ID", "Name", account.Type);
            ViewBag.RaceID = new SelectList(new RaceService().GetAll(), "ID", "Name", account.RaceID);
            ViewBag.ReligionID = new SelectList(new ReligionService().GetAll(), "ID", "Name", account.ReligionID);
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/Admin/Account/Create", Title = "Thêm tài khoản" });
            
            return View(account);
        }
        public ActionResult Details(long? id)
        {
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Chi tiết tài khoản" });
            
            ViewBag.ID = id;
            return View();
        }

        public ActionResult Edit(long? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var member = memberService.GetSingleById(id);
            if (member == null)
                return HttpNotFound();
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật tài khoản" });
            
            ViewBag.ID = member.ID;
            ViewBag.Username = member.User.Username;
            ViewBag.FacultyID = new SelectList(new YouthUnionFacultyService().GetAll(), "ID", "Name", member.YouthUnionClass.FacultyID);
            ViewBag.GroupUserID = new SelectList(groupService.GetAll().Select(x => new { x.ID, x.Name }), "ID", "Name", member.User.GroupUserID);
            ViewBag.Type = new SelectList(CommonList.ListMemberType, "ID", "Name", member.Type);
            ViewBag.RaceID = new SelectList(new RaceService().GetAll(), "ID", "Name", member.RaceID);
            ViewBag.ReligionID = new SelectList(new ReligionService().GetAll(), "ID", "Name", member.ReligionID);

            return View(member);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPersonal(YouthUnionMember member, int? facultyID, int groupUserID)
        {
            ViewBag.Success = false;
            if (member.Email != null && memberService.ExistedEmail(member.Email, member.ID))
            {
                ModelState.AddModelError("Email", "Email này đã được đăng ký tài khoản khác!");
            }
            if (member.Type == ConstraintUnionMemberType.STUDENT && member.YouthUnionClassID == null)
            {
                ModelState.AddModelError("YouthUnionClassID", "Chi Đoàn không thể để trống");
            }

            var user = userService.GetSingleById(member.ID);
            if (ModelState.IsValid)
            {
                
                if (user.GroupUserID != groupUserID)
                {
                    user.GroupUserID = groupUserID;
                    userService.Update(user);
                }
                memberService.Update(member);
                ViewBag.Success = true;
            }

            ViewBag.FacultyID = new SelectList(new YouthUnionFacultyService().GetAll(), "ID", "Name", facultyID);
            ViewBag.GroupUserID = new SelectList(groupService.GetAll().Select(x => new { x.ID, x.Name }), "ID", "Name", groupUserID);
            ViewBag.Type = new SelectList(CommonList.ListMemberType, "ID", "Name", member.Type);
            ViewBag.RaceID = new SelectList(new RaceService().GetAll(), "ID", "Name", member.RaceID);
            ViewBag.ReligionID = new SelectList(new ReligionService().GetAll(), "ID", "Name", member.ReligionID);
            ViewBag.ID = member.ID;
            ViewBag.Username = user.Username;
            //breadcrumb
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "", Title = "Cập nhật tài khoản" });
            
            return View("Edit",member);
        }
        
        public ActionResult ChangePassword(int id, string username)
        {
            return PartialView("_ChangePasswordPartial", new ChangePasswordViewModel() { ID = id, Username = username });
        }
        
        [HttpPost]
        
        public ActionResult ChangePassword(ChangePasswordViewModel form)
        {
            ViewBag.Success = false;
            if (ModelState.IsValid)
            {
                var user = userService.GetSingleById(form.ID);
                user.Password = form.Password;
                userService.Update(user);
                ViewBag.Success = true;    
            }
            return PartialView("_ChangePasswordPartial", form);
            
        }

        public ActionResult Delete(long? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //var member = memberService.GetSingleById(facultyID);
            var user = userService.GetSingleById(id);
            if (user == null)
                return HttpNotFound();
            userService.Delete(user);
            return RedirectToAction("Index");

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                memberService.Dispose();
                groupService.Dispose();
                userService.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}