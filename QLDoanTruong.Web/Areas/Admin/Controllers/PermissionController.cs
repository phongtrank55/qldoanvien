﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QLDoanTruong.Model;
using QLDoanTruong.Model.ViewModel;
using QLDoanTruong.Service;
using System.Threading.Tasks;
using QLDoanTruong.Web.Common;
using QLDoanTruong.Common;


namespace QLDoanTruong.Web.Areas.Admin.Controllers
{
    [AllowPermission(RoleID = "MANAGE_PERMISSION")]
    public class PermissionController : Controller
    {
        private GroupUserService groupService = new GroupUserService();
        private CredentialService credentialService = new CredentialService();
        public PermissionController()
        {
            ViewBag.BreadCrumb = new List<BreadcrumbLink>()
            {
                new BreadcrumbLink(){Link="/Home/Dashboard/", Title = "Dashboard"},
            };
            ViewBag.BreadCrumb.Add(new BreadcrumbLink() { Link = "/Admin/Permission/", Title = "Phân quyền" });
        }

        //
        // GET: /Admin/Permission/
        public async Task<ActionResult> Index()
        {
            var groups = groupService.GetAll();
            var credentials = credentialService.GetAllView();
            return View(new Tuple<List<GroupUser>, List<CredentialViewModel>>(groups, credentials));
        }
        

        [HttpPost]
        public bool TickPermission(int group, bool isChecked, string role)
        {
            try
            {
                var credential = credentialService.GetSingle(role, group);

                if (isChecked) //add
                {
                    if (credential == null)

                        credentialService.Add(new Credential() { GroupUserID = group, RoleID = role });
                }
                else
                {
                    if (credential != null)
                        credentialService.Delete(credential);
                }
                return true;

            }
            catch { }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateGroup(GroupUser group)
        {
            groupService.Add(group);
            return RedirectToAction("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditGroup(GroupUser group)
        {
            groupService.Update(group);
            return RedirectToAction("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteGroup(int id)
        {
            groupService.Delete(id);
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                groupService.Dispose();
                credentialService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}