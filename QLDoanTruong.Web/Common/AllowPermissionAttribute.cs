﻿using System.Web;
using System.Web.Mvc;
using QLDoanTruong.Service;

namespace QLDoanTruong.Web.Common
{
    public class AllowPermissionAttribute : AllowUserAttribute
    {
        public string RoleID { get; set; }
        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            //if (HttpContext.Current.Session["Username"] == null) return false;
            string username = HttpContext.Current.Session["Username"].ToString();

            var roles = new UserService().GetPermissionByUsername(username);
            //Tách các quyền
            if (RoleID.Contains("|"))
            {
                var roleInput = RoleID.Split('|');
                foreach (var role in roleInput)
                {
                    if (roles.Contains(role))
                        return true;
                }
                return false;
            }
            else if (RoleID.Contains("&"))
            {
                var roleInput = RoleID.Split('&');
                foreach (var role in roleInput)
                {
                    if (!roles.Contains(role))
                        return false;
                }
                return true;
            }
            else
            {
                return roles.Contains(RoleID);
            }
            
        }
        
    }
}