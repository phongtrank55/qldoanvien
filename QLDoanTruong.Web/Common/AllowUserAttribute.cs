﻿using System.Web;
using System.Web.Mvc;
using QLDoanTruong.Service;

namespace QLDoanTruong.Web.Common
{
    public class AllowUserAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            return true;
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (System.Web.HttpContext.Current.Session["Username"] == null && System.Web.HttpContext.Current.Request.Cookies["UserInfo"] != null)
            {
                System.Web.HttpContext.Current.Session["Username"] = HttpUtility.UrlDecode(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]["Username"].ToString());
                System.Web.HttpContext.Current.Session["Fullname"] = HttpUtility.UrlDecode(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]["Fullname"].ToString());
                System.Web.HttpContext.Current.Session["Position"] = HttpUtility.UrlDecode(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]["Position"].ToString());
            }
            if (HttpContext.Current.Session["Username"] == null)
            {
                filterContext.Result = new RedirectResult("/Home/Login");
            }
            else
                base.OnAuthorization(filterContext);
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectResult("/pages/errors/401.html");
        }
    }
}