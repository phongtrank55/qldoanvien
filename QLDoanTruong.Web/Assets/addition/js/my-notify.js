﻿

function notifySuccess(message)
{
    $.notifyClose();
    $.notify({
        // options
        icon: 'fa fa-star',
        
        message: message
    }, {
        // settings
        type: 'success',
        placement: {
            from: "top",
            align: "center"
        },
        z_index: 1031,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },

    });
}

function clearNotify() {
    $.notifyClose();
}

function notifyError(message)
{
    $.notifyClose();
    $.notify({
        // options
        icon: 'fa fa-exclamation-triangle',

        message: message
    }, {
        // settings
        type: 'danger',
        placement: {
            from: "top",
            align: "center"
        },
        z_index: 1031,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'

        },
        z_index: 2000,
        
    });
}


function notifyLoading(message) {
    $.notify({
        // options
        icon: 'fa fa-circle-o-notch fa-spin',

        message: message
    }, {
        // settings
        type: 'warning',
        placement: {
            from: "top",
            align: "center"
        },
        z_index: 1031,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        delay: 0
    });
}
