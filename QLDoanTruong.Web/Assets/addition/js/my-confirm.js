﻿
function confirmDelete(url, data, callback) {
    BootstrapDialog.confirm({
        title: 'Cảnh báo',
        message: 'Bạn có chắc chắn xóa?',
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        size: BootstrapDialog.SIZE_SMALL,
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: 'Không', // <-- Default value is 'Cancel',
        btnOKLabel: 'Ừ nà!', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                notifyLoading('Đang xử lý...');
                $.ajax({
                    type: 'post',
                    url: url,
                    data: data,
                    success: function (isSuccess) {
                        //console.log(isSuccess);
                        if (isSuccess=='True') {
                            
                            notifySuccess("Thao tác thành công!");
                            callback();
                        }
                        else
                            notifyError("Thất bại! Đã có lỗi xảy ra!");
                    },
                    error: function () {
                        notifyError("Lỗi hệ thống!");

                    }
                });

            }

        }
    });

}
