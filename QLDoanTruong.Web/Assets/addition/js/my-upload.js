﻿function getFileSize(selector) {
    try {
        var fileSize = 0;
        //for IE
        //if ($.browser.msie) {
        //    //before making an object of ActiveXObject, 
        //    //please make sure ActiveX is enabled in your IE browser
        //    var objFSO = new ActiveXObject("Scripting.FileSystemObject"); var filePath = $(selector)[0].value;
        //    var objFile = objFSO.getFile(filePath);
        //    var fileSize = objFile.size; //size in kb
        //    fileSize = fileSize / 1048576; //size in mb 
        //}
        //    //for FF, Safari, Opeara and Others
        //else {
            fileSize = $(selector)[0].files[0].size; //size in kb
            fileSize = fileSize / 1048576; //size in mb 
        //}
        //return fileSize;
    }
    catch (e) {
        //alert("Error is :" + e);
    }
    return fileSize;
}

function getErrorUploadFile(selector)
{
    var fileExt = '.' + $(selector)[0].files[0].name.split('.').pop().toLowerCase();
    if ($(selector)[0].getAttribute('accept').indexOf(fileExt) == -1)
    {
        return "File không đúng định dạng yêu cầu!";
    }
    var fsize = getFileSize(selector);
    if (fsize == 0) {
        return 'Bạn chưa chọn file!';
    }

    if (fsize > 15) {

        return 'File lớn hơn 15 MB!';
    }

    return '';
}