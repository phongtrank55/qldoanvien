﻿using System.Web.Mvc;
using QLDoanTruong.Common;
using QLDoanTruong.Model.ViewModel;
using QLDoanTruong.Service;
using System;
using System.Net;
using System.Collections.Generic;
using QLDoanTruong.Model;
using QLDoanTruong.Web.Common;
using System.Web;

namespace QLDoanTruong.Web.Controllers
{
    public class HomeController : Controller
    {
        private UserService userService = new UserService();
        private NewService newService = new NewService();

        private YouthUnionMemberService memberService = new YouthUnionMemberService();
        public ActionResult Index()
        {
            ViewBag.Year = new SelectList(CommonList.GetYears(), "ID", "Name", DateTime.Now.Year);
            ViewBag.Month = new SelectList(CommonList.Months, "ID", "Name", DateTime.Now.Month);
            var lastestNew = newService.GetLastestNews();
            var featuredNew = newService.GetFeaturedNews();
            return View(new Tuple<List<New>, List<New>>(lastestNew, featuredNew));
        }

        [HttpPost]
        public ActionResult GetRankTable(int month, int year)
        {

            return RedirectToAction("GetRankTable", "MonthlyReport", new { area = "YouthUnionUniversity", month = month, year = year });
        }


        public ActionResult Login()
        {
            if (Session["Username"] != null)
                return RedirectToAction("Dashboard");
            return View();
        }


        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            
            if (ModelState.IsValid)
            {
                var user = userService.GetByLogin(model.Username, model.Password);

                if (user == null)
                {
                    ModelState.AddModelError("", "Sai tên đăng nhập hoặc mật khẩu");
                }
                else if (user.Status == true)
                {
                    Session["Username"] = user.Username;
                    Session["Fullname"] = user.YouthUnionMember.Lastname + " " + user.YouthUnionMember.Firstname;
                    var position = new YouthUnionMemberService().GetMaxPosition(user.ID);
                    Session["Position"] = position == null ? string.Empty : position;
                    if (model.IsRemember)
                    {
                        HttpCookie aCookie = new HttpCookie("UserInfo");
                        aCookie.Values["Username"] = HttpUtility.UrlEncode(user.Username);
                        aCookie.Values["Fullname"] = HttpUtility.UrlEncode(Session["Fullname"].ToString());
                        aCookie.Values["Position"] = HttpUtility.UrlEncode(Session["Position"].ToString());
                        aCookie.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Add(aCookie);
                    }
                    if (Request.IsAjaxRequest())
                        return JavaScript("window.location ='" + Url.Action("Dashboard") + "'");
                    else
                        return RedirectToAction("Dashboard");
                }
                else ModelState.AddModelError("", "Tài khoản của bạn đang bị khóa!");

            }
            if (Request.IsAjaxRequest())
                return PartialView("_LoginPartial", model);
            else
                return View(model);

        }

        [AllowUser]
        public ActionResult Logout()
        {
            Session["Username"] = null;
            Session["Fullname"] = null;
            Session["Position"] = null;
            //clear cookie
            if (Response.Cookies["UserInfo"]!=null)
            Response.Cookies["UserInfo"].Expires = DateTime.Now.AddDays(-1);
            return RedirectToAction("Index");
        }

        [AllowUser]
        public ActionResult Download(string fileName, int type)
        {
            string path;
            if (type == ConstraintName.DOCUMENT_FILE) path = ConstraintName.DOCUMENT_ACTIVITY_DIRECTORY_PATH;
            else if (type == ConstraintName.PROOF_FILE)
                path = ConstraintName.PROOF_FILE_DIRECTORY_PATH;
            //else if (classType == ConstraintName.NEW_ATTACHMENT)
            //    path = ConstraintName.NEW_THUMB_DIRECTORY_PATH;
            else
                path = ConstraintName.ACHIEVEMENT_FILE_DIRECTORY_PATH;

            path = HttpContext.Server.MapPath(path + fileName);

            if (!System.IO.File.Exists(path))
            {
                return HttpNotFound();
            }

            var fileBytes = System.IO.File.ReadAllBytes(path);
            var response = new FileContentResult(fileBytes, "application/octet-stream")
            {
                FileDownloadName = fileName
            };
            return response;
        }
        [AllowUser]
        public ActionResult Dashboard()
        {
            var menus = new FunctionService().GetMenusByUserName(Session["Username"].ToString());

            return View(menus);
        }

        public PartialViewResult GetFunction()
        {
            var functions = new FunctionService().GetMenusByUserName(Session["Username"].ToString());
            return PartialView("_DashboardPartial", functions);
        }
        public PartialViewResult GetBreadCrumbMenu(List<BreadcrumbLink> model)
        {
            
            return PartialView("_BreadCrumbMenuPartial", model);
        }

        [HttpPost]
        public string ForgetPassword(string email)
        {
            var user = userService.GetSingleByEmail(email);
            if (user == null)
                return "Không tìm thấy email của bạn trong hệ thống!";
            if (user.Status == false)
                return "Tài khoản đang bị khóa!";
            //update token
            string tokenForgetPass;
            do
            {
                tokenForgetPass = GenerateString.GenerateToken();
            } while (userService.ExistedToken(tokenForgetPass));

            user.TokenForgetPass = tokenForgetPass;
            userService.Update(user);
            string url = Request.Url.GetLeftPart(UriPartial.Authority) + "/ResetPassword/" + tokenForgetPass;
            string fullname = user.YouthUnionMember.Lastname + " " + user.YouthUnionMember.Firstname;
            if (fullname.Trim() == string.Empty)
                fullname = user.Username;
            string content = string.Format("Chào {0}, <br> Bạn đã yêu cầu đặt lại mật khẩu! Vui lòng vào {1} để thưc hiện thao tác!<br> Nếu bạn không yeu cầu xin bỏ qua email này", fullname, url);
            try
            {
                CommonUtility.SendMail(email, "Đặt lại mật khẩu", content);
                return "true";
            }
            catch
            {
                return "Lỗi gửi mail!";
            }
            //QLDoanTruong.Common.CommonUtility.SendMail("phongtrank55@gmail.com", "abc", "<h1>content</h1>");

        }

        public ActionResult ResetPassword(string token)
        {
            if (token == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            userService.ExistedToken(token);
            if (!userService.ExistedToken(token))
            {
                return HttpNotFound();
            }
            ViewBag.Token = token;
            return View();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel form, string token)
        {
            var user = userService.GetSingleByToken(token);
            if (ModelState.IsValid && user != null)
            {
                user.PasswordChangeTimeNo++;
                user.TokenForgetPass = null;
                user.Password = form.Password;
                userService.Update(user);
                return RedirectToAction("Login");
            }
            ViewBag.Token = token;
            return View(form);

        }
        /// <summary>
        /// chi tiết tin tức
        /// </summary>
        /// <param search="facultyID"></param>
        /// <returns></returns>
        public ActionResult DetailsNew(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ////Chọn chế độ nút trở về
            //ViewBag.BackHomePage = true;

            var _new = newService.GetSingleById(id);
            if (_new == null)
            {
                return HttpNotFound();
            }
            return View("~/Areas/YouthUnionUniversity/Views/New/Details.cshtml", _new);
        }

        [ChildActionOnly]
        public PartialViewResult GetAccount()
        {
            if (System.Web.HttpContext.Current.Session["Username"] == null && System.Web.HttpContext.Current.Request.Cookies["UserInfo"] != null)
            {
                System.Web.HttpContext.Current.Session["Username"] = HttpUtility.UrlDecode(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]["Username"].ToString());
                System.Web.HttpContext.Current.Session["Fullname"] = HttpUtility.UrlDecode(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]["Fullname"].ToString());
                System.Web.HttpContext.Current.Session["Position"] = HttpUtility.UrlDecode(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]["Position"].ToString());
            }

            if (Session["Username"] != null)
            {
                ViewBag.Avatar = userService.GetAvatarByUsername(Session["Username"].ToString());
                ViewBag.Fullname = Session["Fullname"];
                ViewBag.Position = Session["Position"];
            }
            return PartialView("_AccountPartial");
        }
        [AllowUser]
        public ActionResult Profile()
        {
            ViewBag.ID = userService.GetIDByUsername(Session["Username"].ToString());
            return View();
        }
        /// <summary>
        /// thiết lập tài khoản
        /// </summary>
        /// <returns></returns>
        [AllowUser]
        public ActionResult SettingAccount()
        {

            return View();
        }

        public PartialViewResult EditPersonalInfo()
        {
            long id = userService.GetIDByUsername(Session["Username"].ToString());
            var member = memberService.GetSingleById(id);
            //khóa ngoại
            ViewBag.RaceID = new SelectList(new RaceService().GetAll(), "ID", "Name", member.RaceID);
            ViewBag.ReligionID = new SelectList(new ReligionService().GetAll(), "ID", "Name", member.ReligionID);

            return PartialView("_EditPersonalInfoPartial", member);
        }
        [HttpPost]
        public PartialViewResult EditPersonalInfo(YouthUnionMember entity, System.Web.HttpPostedFileBase file)
        {
            //khóa ngoại
            ViewBag.RaceID = new SelectList(new RaceService().GetAll(), "ID", "Name", entity.RaceID);
            ViewBag.ReligionID = new SelectList(new ReligionService().GetAll(), "ID", "Name", entity.ReligionID);

            ViewBag.Success = false;

            long id = userService.GetIDByUsername(Session["Username"].ToString());
            var member = memberService.GetSingleById(id);

            member.IDCardNo = entity.IDCardNo;
            member.HomeTown = entity.HomeTown;
            member.Gender = entity.Gender;
            member.Phone = entity.Phone;
            member.PlaceOfIssue = entity.PlaceOfIssue;
            member.PermanentAddress = entity.PermanentAddress;
            member.RaceID = entity.RaceID;
            member.RelativeName = entity.RelativeName;
            member.RelativeAddress = entity.RelativeAddress;
            member.RelativePhone = entity.RelativePhone;
            member.ReligionID = entity.ReligionID;
            member.Website = entity.Website;
            member.DateIntoParty = entity.DateIntoParty;
            member.CurrentAddress = entity.CurrentAddress;
            member.DateIntoYouthUnion = entity.DateIntoYouthUnion;
            member.DateOfIssue = entity.DateOfIssue;

            if (member.Type != QLDoanTruong.Common.ConstraintUnionMemberType.STUDENT)
            {
                member.DateOfBirth = entity.DateOfBirth;
            }
            //Check mail
            //if (member.Email == null)
            //{
            //    ModelState.AddModelError("Email", "Email Không được trống!");
            //}

            if (member.Email != null && memberService.ExistedEmail(member.Email, member.ID))
            {
                ModelState.AddModelError("Email", "Email này đã được đăng ký tài khoản khác!");
            }

            member.Email = entity.Email;
            if (!ModelState.IsValidField("Email"))
            {
                return PartialView("_EditPersonalInfoPartial", member);
            }
            //Đổi avartar
            if (file != null)
            {
                string fileName = StringUtility.RemoveUnicode("avt" + member.YouthUnionMemberCode) + file.FileName.Substring(file.FileName.LastIndexOf('.'));//lây đuôi

                var path = HttpContext.Server.MapPath(ConstraintName.AVATAR_DIRECTORY_PATH) + fileName;
                file.SaveAs(path);
                member.Photo = fileName;

            }
            //else
            //{
            //    member.Photo = QLDoanTruong.Common.ConstraintName.AVATAR_DEFAULT;
            //}


            memberService.Update(member);
            ViewBag.Success = true;
            return PartialView("_EditPersonalInfoPartial", member);
        }
        [HttpPost]
        public int ChangePassword(string oldPassword, string newPassword, string changePassword)
        {
            var user = userService.GetByLogin(Session["Username"].ToString(), oldPassword);

            if (user == null)
            {
                return -1;
            }
            try
            {
                user.PasswordChangeTimeNo++;
                user.TokenForgetPass = null;
                user.Password = newPassword;
                userService.Update(user);
                return 1;

            }
            catch
            {
                return 0;
            }

        }

        [HttpPost]
        public int RegisterEmail(string email)
        {
            using (var memberService = new YouthUnionMemberService())
            {
                if (memberService.ExistedEmail(email))
                {
                    return -1;
                }

                try
                {
                    userService.UpdateEmailByUsername(Session["Username"].ToString(), email);
                    return 1;

                }
                catch
                {
                    return 0;
                }
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                memberService.Dispose();
                newService.Dispose();
                userService.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}