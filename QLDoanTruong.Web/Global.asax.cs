﻿using System.IO;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace QLDoanTruong.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            
            System.Data.Entity.Database.SetInitializer<QLDoanTruong.Model.YouthUnionUniversityDbContext>(null);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            CreateDirectoryUpload();
        }
        private void CreateDirectoryUpload()
        {
            
            string[] folderPaths = new string[]{
                Server.MapPath(QLDoanTruong.Common.ConstraintName.ACHIEVEMENT_FILE_DIRECTORY_PATH), 
                Server.MapPath(QLDoanTruong.Common.ConstraintName.AVATAR_DIRECTORY_PATH), 
                Server.MapPath(QLDoanTruong.Common.ConstraintName.DOCUMENT_ACTIVITY_DIRECTORY_PATH), 
                Server.MapPath(QLDoanTruong.Common.ConstraintName.PROOF_FILE_DIRECTORY_PATH), 
                Server.MapPath(QLDoanTruong.Common.ConstraintName.MENU_DIRECTORY_PATH), 
                Server.MapPath(QLDoanTruong.Common.ConstraintName.IMPORT_DIRECTORY_PATH)
            };
            foreach (var path in folderPaths)
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
        }
    }
}
