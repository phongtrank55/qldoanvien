﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("Positions")]
    public class Position
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [Display(Name="Chức vụ đoàn")]
        public string Name { get; set; }

        public int Level { get; set; }
        public int Type { get; set; }
        public int UnitType { get; set; }
        public int Amount { get; set; }
        public virtual ICollection<LeaderUnit> LeaderUnits { get; set; }
        public virtual ICollection<LeaderClass> LeaderClasses { get; set; }
    }
}
