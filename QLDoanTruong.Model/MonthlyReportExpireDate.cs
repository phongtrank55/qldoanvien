﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("MonthlyReportExpireDates")]
    public class MonthlyReportExpireDate
    {
        public MonthlyReportExpireDate()
        {
            DeducatePoint = 0;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int Month { get; set; }
        public int Year { get; set; }

        [Display(Name = "Hạn nộp")]
        [DisplayFormat(DataFormatString = "{0:H:mm dd/MM/yyyy}")]
        [Required(ErrorMessage="Ngày hết hạn không thể để trống")]
        public System.DateTime DueDate { get; set; }

        [Display(Name = "Ngày cắt")]
        [DisplayFormat(DataFormatString = "{0:H:mm dd/MM/yyyy}")]
        public System.DateTime? CutOffDate { get; set; }
        
        [Display(Name="Cho phép nạp muộn")]
        public bool IsEnabledCutOffDate { get; set; }

        [Display(Name = "Điểm trừ")]
        public float DeducatePoint { get; set; }

        [Display(Name = "Kích hoạt trừ điểm ngày quá hạn")]
        public bool IsEnabledDeducateEveryExpireDay { get; set; }

    }
}
