﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("Awards")]
    public class Award
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        
        [Display(Name = "Tên giải thưởng")]
        [Required(ErrorMessage = "Tên giải thưởng không thể để trống")]
        public string Name { get; set; }

        [Display(Name = "Mô tả")]
        public string Description { get; set; }

        public int Type { get; set; }
        public int AwardLevelID { get; set; }

        [ForeignKey("AwardLevelID")]
        public virtual AwardLevel AwardLevel { get; set; }
    }
}
