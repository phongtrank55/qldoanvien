namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMember7 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.YouthUnionMembers", "Firstname", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.YouthUnionMembers", "Firstname", c => c.String(nullable: false));
        }
    }
}
