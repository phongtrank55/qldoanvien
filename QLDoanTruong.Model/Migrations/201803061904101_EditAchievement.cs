namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditAchievement : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Achievements",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        YouthUnionID = c.Int(),
                        Type = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Document = c.String(),
                        Description = c.String(storeType: "ntext"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Units", t => t.YouthUnionID)
                .ForeignKey("dbo.YouthUnionMembers", t => t.YouthUnionID)
                .ForeignKey("dbo.YouthUnionClasses", t => t.YouthUnionID)
                .Index(t => t.YouthUnionID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Achievements", "YouthUnionID", "dbo.YouthUnionClasses");
            DropForeignKey("dbo.Achievements", "YouthUnionID", "dbo.YouthUnionMembers");
            DropForeignKey("dbo.Achievements", "YouthUnionID", "dbo.Units");
            DropIndex("dbo.Achievements", new[] { "YouthUnionID" });
            DropTable("dbo.Achievements");
        }
    }
}
