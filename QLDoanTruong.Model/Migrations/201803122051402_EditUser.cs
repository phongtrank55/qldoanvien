namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Status", c => c.Boolean(nullable: false));
            AddColumn("dbo.Users", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Users", "ModifiedDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.YouthUnionMembers", "CreatedDate");
            DropColumn("dbo.YouthUnionMembers", "ModifiedDate");
            DropColumn("dbo.Users", "TokenResetPass");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "TokenResetPass", c => c.String());
            AddColumn("dbo.YouthUnionMembers", "ModifiedDate", c => c.String());
            AddColumn("dbo.YouthUnionMembers", "CreatedDate", c => c.String());
            DropColumn("dbo.Users", "ModifiedDate");
            DropColumn("dbo.Users", "CreatedDate");
            DropColumn("dbo.Users", "Status");
        }
    }
}
