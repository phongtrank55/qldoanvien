namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMenu : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Menus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(),
                        Url = c.String(nullable: false),
                        ParentMenuID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Menus", t => t.ParentMenuID)
                .Index(t => t.ParentMenuID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Menus", "ParentMenuID", "dbo.Menus");
            DropIndex("dbo.Menus", new[] { "ParentMenuID" });
            DropTable("dbo.Menus");
        }
    }
}
