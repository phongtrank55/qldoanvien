namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMember5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LeaderClasses", "YouthUnionMemberID", "dbo.YouthUnionMembers");
            AddForeignKey("dbo.LeaderClasses", "YouthUnionMemberID", "dbo.YouthUnionMembers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LeaderClasses", "YouthUnionMemberID", "dbo.YouthUnionMembers");
            AddForeignKey("dbo.LeaderClasses", "YouthUnionMemberID", "dbo.YouthUnionMembers", "ID", cascadeDelete: true);
        }
    }
}
