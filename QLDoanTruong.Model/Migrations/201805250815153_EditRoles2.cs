namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditRoles2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Roles", "Order", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Roles", "Order");
        }
    }
}
