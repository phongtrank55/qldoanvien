namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditRaceAndReligion1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Races", "IsPopular", c => c.Boolean(nullable: false));
            AddColumn("dbo.Religions", "IsPopular", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Religions", "IsPopular");
            DropColumn("dbo.Races", "IsPopular");
        }
    }
}
