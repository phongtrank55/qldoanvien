namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditPosition2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.YouthUnionClasses", "SecretaryID", "dbo.YouthUnionMembers");
            DropIndex("dbo.YouthUnionClasses", new[] { "SecretaryID" });
            AddColumn("dbo.YouthUnionMembers", "PositionID", c => c.Int());
            AddColumn("dbo.Positions", "Type", c => c.Int(nullable: false));
            CreateIndex("dbo.YouthUnionMembers", "PositionID");
            AddForeignKey("dbo.YouthUnionMembers", "PositionID", "dbo.Positions", "ID");
            DropColumn("dbo.YouthUnionClasses", "SecretaryID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.YouthUnionClasses", "SecretaryID", c => c.Int());
            DropForeignKey("dbo.YouthUnionMembers", "PositionID", "dbo.Positions");
            DropIndex("dbo.YouthUnionMembers", new[] { "PositionID" });
            DropColumn("dbo.Positions", "Type");
            DropColumn("dbo.YouthUnionMembers", "PositionID");
            CreateIndex("dbo.YouthUnionClasses", "SecretaryID");
            AddForeignKey("dbo.YouthUnionClasses", "SecretaryID", "dbo.YouthUnionMembers", "ID");
        }
    }
}
