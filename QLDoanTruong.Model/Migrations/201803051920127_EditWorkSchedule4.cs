namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditWorkSchedule4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkScheduleDetails", "DayOfWeek", c => c.String());
            AddColumn("dbo.WorkScheduleDetails", "IsDayLong", c => c.Boolean(nullable: false));
            AddColumn("dbo.WorkScheduleDetails", "IsEveryDay", c => c.Boolean(nullable: false));
            AddColumn("dbo.WorkScheduleDetails", "IsImportant", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WorkScheduleDetails", "IsImportant");
            DropColumn("dbo.WorkScheduleDetails", "IsEveryDay");
            DropColumn("dbo.WorkScheduleDetails", "IsDayLong");
            DropColumn("dbo.WorkScheduleDetails", "DayOfWeek");
        }
    }
}
