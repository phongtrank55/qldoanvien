namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AcademicYears",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.YouthUnionClasses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        YouthUnionClassCode = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        AcademicYearID = c.Int(),
                        FacultyID = c.Int(nullable: false),
                        SecretaryID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AcademicYears", t => t.AcademicYearID)
                .ForeignKey("dbo.Units", t => t.FacultyID, cascadeDelete: true)
                .ForeignKey("dbo.YouthUnionMembers", t => t.SecretaryID)
                .Index(t => t.AcademicYearID)
                .Index(t => t.FacultyID)
                .Index(t => t.SecretaryID);
            
            CreateTable(
                "dbo.YouthUnionMembers",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Lastname = c.String(nullable: false),
                        Firstname = c.String(nullable: false),
                        YouthUnionClassID = c.Int(),
                        Type = c.Int(nullable: false),
                        YouthUnionMemberCode = c.String(nullable: false),
                        DateOfBirth = c.String(),
                        Gender = c.Int(nullable: false),
                        RaceID = c.Int(nullable: false),
                        ReligionID = c.Int(nullable: false),
                        Phone = c.String(),
                        Email = c.String(),
                        HomeTown = c.String(),
                        CurrentAddress = c.String(),
                        PermanentAddress = c.String(),
                        RelativeName = c.String(),
                        RelativePhone = c.String(),
                        Photo = c.String(),
                        YouthUnionCardNo = c.String(),
                        DateIntoYouthUnion = c.String(),
                        DateIntoParty = c.String(),
                        IDCardNo = c.String(),
                        DateOfIssue = c.String(),
                        PlaceOfIssue = c.String(),
                        CreatedDate = c.String(),
                        ModifiedDate = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Races", t => t.RaceID, cascadeDelete: true)
                .ForeignKey("dbo.Religions", t => t.ReligionID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.ID)
                .ForeignKey("dbo.YouthUnionClasses", t => t.YouthUnionClassID)
                .Index(t => t.ID)
                .Index(t => t.YouthUnionClassID)
                .Index(t => t.RaceID)
                .Index(t => t.ReligionID);
            
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ActivityCode = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        UnitID = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        YouthUnionMemberStaffID = c.Int(nullable: false),
                        Document = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Units", t => t.UnitID, cascadeDelete: true)
                .ForeignKey("dbo.YouthUnionMembers", t => t.YouthUnionMemberStaffID)
                .Index(t => t.UnitID)
                .Index(t => t.YouthUnionMemberStaffID);
            
            CreateTable(
                "dbo.MonthlyReportDetails",
                c => new
                    {
                        MonthReportID = c.Int(nullable: false),
                        ActivityID = c.Int(nullable: false),
                        DateActive = c.DateTime(nullable: false),
                        PointSelf = c.Single(),
                        PointSubmit = c.Single(),
                        Submited = c.Boolean(nullable: false),
                        FileUploadProof = c.String(),
                        OutsideLinkProof = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.MonthReportID, t.ActivityID })
                .ForeignKey("dbo.MonthlyReports", t => t.MonthReportID, cascadeDelete: true)
                .ForeignKey("dbo.Activities", t => t.ActivityID, cascadeDelete: true)
                .Index(t => t.MonthReportID)
                .Index(t => t.ActivityID);
            
            CreateTable(
                "dbo.MonthlyReports",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MonthlyReportCode = c.String(nullable: false),
                        FacultyID = c.Int(nullable: false),
                        Month = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                        Submited = c.Boolean(nullable: false),
                        SentDate = c.DateTime(),
                        Rank = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Units", t => t.FacultyID)
                .Index(t => t.FacultyID);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UnitCode = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Email = c.String(),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Leaders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PositionID = c.Int(nullable: false),
                        UnitID = c.Int(nullable: false),
                        YouthUnionMemberID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Positions", t => t.PositionID, cascadeDelete: true)
                .ForeignKey("dbo.Units", t => t.UnitID, cascadeDelete: true)
                .ForeignKey("dbo.YouthUnionMembers", t => t.YouthUnionMemberID)
                .Index(t => t.PositionID)
                .Index(t => t.UnitID)
                .Index(t => t.YouthUnionMemberID);
            
            CreateTable(
                "dbo.Positions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Content = c.String(nullable: false, storeType: "ntext"),
                        CreatedDate = c.DateTime(nullable: false),
                        AuthorID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.YouthUnionMembers", t => t.AuthorID, cascadeDelete: true)
                .Index(t => t.AuthorID);
            
            CreateTable(
                "dbo.Races",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Religions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Email = c.String(),
                        PasswordChangeTimeNo = c.Int(nullable: false),
                        TokenResetPass = c.String(),
                        GroupUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.GroupUsers", t => t.GroupUserID, cascadeDelete: true)
                .Index(t => t.GroupUserID);
            
            CreateTable(
                "dbo.GroupUsers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Credentials",
                c => new
                    {
                        GroupUserID = c.Int(nullable: false),
                        RoleID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.GroupUserID, t.RoleID })
                .ForeignKey("dbo.GroupUsers", t => t.GroupUserID, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleID, cascadeDelete: true)
                .Index(t => t.GroupUserID)
                .Index(t => t.RoleID);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.WorkSchedules",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SchoolYearID = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Time = c.Time(nullable: false, precision: 7),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        YouthUnionMemberID = c.Int(nullable: false),
                        Location = c.String(),
                        Participants = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SchoolYears", t => t.SchoolYearID, cascadeDelete: true)
                .ForeignKey("dbo.YouthUnionMembers", t => t.YouthUnionMemberID, cascadeDelete: true)
                .Index(t => t.SchoolYearID)
                .Index(t => t.YouthUnionMemberID);
            
            CreateTable(
                "dbo.SchoolYears",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Status = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TempMonthlyReportDetails",
                c => new
                    {
                        MonthReportID = c.Int(nullable: false),
                        ActivityID = c.Int(nullable: false),
                        DateActive = c.DateTime(nullable: false),
                        PointSelf = c.Single(),
                        FileUploadProof = c.String(),
                        OutsideLinkProof = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.MonthReportID, t.ActivityID })
                .ForeignKey("dbo.Activities", t => t.ActivityID, cascadeDelete: true)
                .ForeignKey("dbo.MonthlyReports", t => t.MonthReportID, cascadeDelete: true)
                .Index(t => t.MonthReportID)
                .Index(t => t.ActivityID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TempMonthlyReportDetails", "MonthReportID", "dbo.MonthlyReports");
            DropForeignKey("dbo.TempMonthlyReportDetails", "ActivityID", "dbo.Activities");
            DropForeignKey("dbo.YouthUnionMembers", "YouthUnionClassID", "dbo.YouthUnionClasses");
            DropForeignKey("dbo.YouthUnionClasses", "SecretaryID", "dbo.YouthUnionMembers");
            DropForeignKey("dbo.WorkSchedules", "YouthUnionMemberID", "dbo.YouthUnionMembers");
            DropForeignKey("dbo.WorkSchedules", "SchoolYearID", "dbo.SchoolYears");
            DropForeignKey("dbo.YouthUnionMembers", "ID", "dbo.Users");
            DropForeignKey("dbo.Users", "GroupUserID", "dbo.GroupUsers");
            DropForeignKey("dbo.Credentials", "RoleID", "dbo.Roles");
            DropForeignKey("dbo.Credentials", "GroupUserID", "dbo.GroupUsers");
            DropForeignKey("dbo.YouthUnionMembers", "ReligionID", "dbo.Religions");
            DropForeignKey("dbo.YouthUnionMembers", "RaceID", "dbo.Races");
            DropForeignKey("dbo.News", "AuthorID", "dbo.YouthUnionMembers");
            DropForeignKey("dbo.Leaders", "YouthUnionMemberID", "dbo.YouthUnionMembers");
            DropForeignKey("dbo.Activities", "YouthUnionMemberStaffID", "dbo.YouthUnionMembers");
            DropForeignKey("dbo.MonthlyReportDetails", "ActivityID", "dbo.Activities");
            DropForeignKey("dbo.YouthUnionClasses", "FacultyID", "dbo.Units");
            DropForeignKey("dbo.MonthlyReports", "FacultyID", "dbo.Units");
            DropForeignKey("dbo.Leaders", "UnitID", "dbo.Units");
            DropForeignKey("dbo.Leaders", "PositionID", "dbo.Positions");
            DropForeignKey("dbo.Activities", "UnitID", "dbo.Units");
            DropForeignKey("dbo.MonthlyReportDetails", "MonthReportID", "dbo.MonthlyReports");
            DropForeignKey("dbo.YouthUnionClasses", "AcademicYearID", "dbo.AcademicYears");
            DropIndex("dbo.TempMonthlyReportDetails", new[] { "ActivityID" });
            DropIndex("dbo.TempMonthlyReportDetails", new[] { "MonthReportID" });
            DropIndex("dbo.WorkSchedules", new[] { "YouthUnionMemberID" });
            DropIndex("dbo.WorkSchedules", new[] { "SchoolYearID" });
            DropIndex("dbo.Credentials", new[] { "RoleID" });
            DropIndex("dbo.Credentials", new[] { "GroupUserID" });
            DropIndex("dbo.Users", new[] { "GroupUserID" });
            DropIndex("dbo.News", new[] { "AuthorID" });
            DropIndex("dbo.Leaders", new[] { "YouthUnionMemberID" });
            DropIndex("dbo.Leaders", new[] { "UnitID" });
            DropIndex("dbo.Leaders", new[] { "PositionID" });
            DropIndex("dbo.MonthlyReports", new[] { "FacultyID" });
            DropIndex("dbo.MonthlyReportDetails", new[] { "ActivityID" });
            DropIndex("dbo.MonthlyReportDetails", new[] { "MonthReportID" });
            DropIndex("dbo.Activities", new[] { "YouthUnionMemberStaffID" });
            DropIndex("dbo.Activities", new[] { "UnitID" });
            DropIndex("dbo.YouthUnionMembers", new[] { "ReligionID" });
            DropIndex("dbo.YouthUnionMembers", new[] { "RaceID" });
            DropIndex("dbo.YouthUnionMembers", new[] { "YouthUnionClassID" });
            DropIndex("dbo.YouthUnionMembers", new[] { "ID" });
            DropIndex("dbo.YouthUnionClasses", new[] { "SecretaryID" });
            DropIndex("dbo.YouthUnionClasses", new[] { "FacultyID" });
            DropIndex("dbo.YouthUnionClasses", new[] { "AcademicYearID" });
            DropTable("dbo.TempMonthlyReportDetails");
            DropTable("dbo.SchoolYears");
            DropTable("dbo.WorkSchedules");
            DropTable("dbo.Roles");
            DropTable("dbo.Credentials");
            DropTable("dbo.GroupUsers");
            DropTable("dbo.Users");
            DropTable("dbo.Religions");
            DropTable("dbo.Races");
            DropTable("dbo.News");
            DropTable("dbo.Positions");
            DropTable("dbo.Leaders");
            DropTable("dbo.Units");
            DropTable("dbo.MonthlyReports");
            DropTable("dbo.MonthlyReportDetails");
            DropTable("dbo.Activities");
            DropTable("dbo.YouthUnionMembers");
            DropTable("dbo.YouthUnionClasses");
            DropTable("dbo.AcademicYears");
        }
    }
}
