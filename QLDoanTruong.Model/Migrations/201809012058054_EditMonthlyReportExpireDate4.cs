namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMonthlyReportExpireDate4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Activities", "YouthUnionMemberStaffID", "dbo.YouthUnionMembers");
            DropIndex("dbo.Activities", new[] { "YouthUnionMemberStaffID" });
            AddColumn("dbo.Activities", "YouthUnionMemberStaff", c => c.String(nullable: false));
            DropColumn("dbo.Activities", "YouthUnionMemberStaffID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Activities", "YouthUnionMemberStaffID", c => c.Int(nullable: false));
            DropColumn("dbo.Activities", "YouthUnionMemberStaff");
            CreateIndex("dbo.Activities", "YouthUnionMemberStaffID");
            AddForeignKey("dbo.Activities", "YouthUnionMemberStaffID", "dbo.YouthUnionMembers", "ID", cascadeDelete: true);
        }
    }
}
