﻿namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<QLDoanTruong.Model.YouthUnionUniversityDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(QLDoanTruong.Model.YouthUnionUniversityDbContext context)
        {
            // context.GroupUsers.AddOrUpdate(x => x.ID,
            //    new GroupUser() { ID = 1, Name = "Admin" },
            //    new GroupUser() { ID = 2, Name = "Đoàn trường" },
            //    new GroupUser() { ID = 3, Name = "Liên chi đòan" },
            //    new GroupUser() { ID = 4, Name = "Đoàn viên" }

            //);
            // context.Functions.AddOrUpdate(x => x.ID,
            //     new Function() { ID = 1, Name = "Liên chi đoàn", Link = "/YouthUnionUniversity/YouthUnionFaculty", Image = "lienchidoan.PNG", Order = 1 },
            //     new Function() { ID = 2, Name = "Chi đoàn", Link = "/YouthUnionUniversity/YouthUnionClass/", Image = "chidoan.PNG", Order = 2 },
            //     new Function() { ID = 3, Name = "Đoàn viên", Link = "/YouthUnionUniversity/YouthUnionMember/", Image = "doanvien.PNG", Order = 3 },
            //     new Function() { ID = 4, Name = "Hoạt động", Link = "/YouthUnionUniversity/Activity/", Image = "active.PNG", Order = 4 },
            //     new Function() { ID = 5, Name = "Báo cáo hoạt động", Link = "/YouthUnionUniversity/MonthlyReport/", Image = "report.PNG", Order = 5 },
            //     new Function() { ID = 6, Name = "Lịch tuần", Link = "/YouthUnionUniversity/WorkSchedule/", Image = "lichtuan.PNG", Order = 6 },
            //     new Function() { ID = 7, Name = "Báo cáo hoạt động", Link = "/YouthUnionFaculty/MonthlyReport/", Image = "sendreport.PNG", Order = 7 },
            //     new Function() { ID = 8, Name = "Tin tức", Link = "/YouthUnionUniversity/New/", Image = "news.PNG", Order = 8 },

            //     new Function() { ID = 9, Name = "Danh hiệu", Link = "/YouthUnionUniversity/Award/", Image = "award.PNG", Order = 9 },
            //     new Function() { ID = 10, Name = "Khen thưởng", Link = "/YouthUnionUniversity/Achievement/", Image = "khenthuong.PNG", Order = 10 },
            //     new Function() { ID = 11, Name = "Quản lý Tài khoản", Link = "/Admin/Account/", Image = "account.PNG", Order = 11 },
            //     new Function() { ID = 12, Name = "Quản lý Phân Quyền", Link = "/Admin/Permission/", Image = "permission.PNG", Order = 12 }

            //     //
            //     //new Function() { ID = 9, FunctionName = "Quản lý Chi đoàn", Link = "/YouthUnionFaculty/YouthUnionClass/", Image = "menu.PNG", Order = 1 },
            //     //new Function() { ID = 10, FunctionName = "Quản lý Đoàn viên", Link = "/YouthUnionFaculty/YouthUnionMember/", Image = "menu.PNG", Order = 2 },
            //     //new Function() { ID = 11, FunctionName = "Quản lý Hoạt động", Link = "/YouthUnionFaculty/Activity/", Image = "menu.PNG", Order = 3 },

            //     //new Function() { ID = 13, FunctionName = "Quản lý Lịch tuần", Link = "/YouthUnionFaculty/WorkSchedule/", Image = "menu.PNG", Order = 5 },
            //     //new Function() { ID = 18, FunctionName = "Quản lý Tài khoản LCĐ", Link = "/YouthUnionFaculty/WorkSchedule/", Image = "menu.PNG", Order = 6 },
            //     //
            //     //new Function() { ID = 14, FunctionName = "Thông tin cá nhân", Link = "/YouthUnionMember/Infomation/", Image = "menu.PNG", Order = 1 },
            //     //new Function() { ID = 15, FunctionName = "Xem hoạt động", Link = "/YouthUnionMember/Activity/", Image = "menu.PNG", Order = 2 },
            //     //new Function() { ID = 16, FunctionName = "Đăng ký hoạt động***", Link = "/YouthUnionMember/RegisterActivity/", Image = "menu.PNG", Order = 3 },
            //     //new Function() { ID = 17, FunctionName = "Gửi ý kiến góp ý***", Link = "/YouthUnionMember/Suggestion/", Image = "menu.PNG", Order = 4 }
            //     //
            //     //new Function() { ID = 19, FunctionName = "Quản lý tài khoản", Link = "/Admin/Account/", Image = "menu.PNG", Order = 1 },
            //     //new Function() { ID = 20, FunctionName = "Quản lý phân quyền", Link = "/Admin/Permission/", Image = "menu.PNG", Order = 2 },
            //     //new Function() { ID = 21, FunctionName = "Tùy chỉnh giao diện", Link = "/Admin/Interface/", Image = "menu.PNG", Order = 3 }
            //     );

            // //context.Roles.AddOrUpdate(x => x.ID,
            // //    new Role() { ID = "ACCESS_FACULTY", Name = "Truy cập trang QL Liên chi đoàn", FunctionID = 1 },
            // //    new Role() { ID = "ACCESS_CLASS", Name = "Xem Chi Đoàn", FunctionID = 2 },
            // //    new Role() { ID = "UPDATE_CLASS", Name = "Cập nhật Chi đoàn", FunctionID = 2 },
            // //     new Role() { ID = "ACCESS_MEMBER", Name = "Xem Đoàn viên", FunctionID = 3 },
            // //    new Role() { ID = "UPDATE_MEMBER", Name = "Cập nhật Đoàn viên", FunctionID = 3 },
            // //    new Role() { ID = "ADD_STUDENT", Name = "Thêm mới Đoàn viên Sinh viên", FunctionID = 3 },
            // //    new Role() { ID = "ADD_LECTURE", Name = "Thêm mới Đoàn viên Giảng viên", FunctionID = 3 },
            // //    new Role() { ID = "EDIT_MEMBER", Name = "Sửa thông tin Đoàn Viên", FunctionID = 3 },
            // //    new Role() { ID = "DELETE_MEMBER", Name = "Xóa Đoàn Viên", FunctionID = 3 },
            // //    new Role() { ID = "ACCESS_ACTIVITY", Name = "Xem Hoạt động", FunctionID = 4 },
            // //    new Role() { ID = "UPDATE_ACTIVITY", Name = "Cập nhật Hoạt động", FunctionID = 4 },
            // //    new Role() { ID = "ACCESS_REPORT", Name = "Xem báo cáo hoạt động của các LCĐ", FunctionID = 5 },
            // //    new Role() { ID = "SUBMIT_REPORT", Name = "Chấm điểm hoạt động", FunctionID = 5 },
            // //    new Role() { ID = "SEND_REPORT", Name = "Gửi báo cáo lên đoàn trường", FunctionID = 7},
            // //    new Role() { ID = "ADD_REPORT", Name = "Thêm báo cáo hoạt động", FunctionID = 7 },
            // //    new Role() { ID = "ACCESS_SCHEDULE", Name = "Xem Lịch tuần", FunctionID = 6 },
            // //    new Role() { ID = "ADD_SCHEDULE", Name = "Thêm lịch tuần", FunctionID = 6 },
            // //    new Role() { ID = "UPDATE_SCHEDULE", Name = "Cập nhật lich tuần", FunctionID = 6 },
            // //    new Role() { ID = "ACCESS_NEW", Name = "Xem Tin tức", FunctionID = 8 },
            // //    new Role() { ID = "UPDATE_NEW", Name = "Cập nhật Tin tức", FunctionID = 8 },
            // //    new Role() { ID = "ACCESS_AWARD", Name = "Quản lý Danh hiệu", FunctionID = 9 },
            // //    new Role() { ID = "ACCESS_ACHIEVEMENT", Name = "Xem Khen thưởng", FunctionID = 10 },
            // //    new Role() { ID = "ADD_ACHIEVEMENT_MEMBER", Name = "Thêm Khen thưởng đoàn viên", FunctionID = 10 },
            // //    new Role() { ID = "ADD_ACHIEVEMENT_FACULTY", Name = "Thêm Khen thưởng liên chi đoàn", FunctionID = 10 },
            // //    new Role() { ID = "DELETE_ACHIEVEMENT_MEMBER", Name = "Xóa Khen thưởng đoàn viên", FunctionID = 10 },
            // //    new Role() { ID = "DELETE_ACHIEVEMENT_FACULTY", Name = "Xóa Khen thưởng Liên chi đoàn", FunctionID = 10 },
            // //    new Role() { ID = "ACCESS_ACCOUNT", Name = "Truy cập QL Tài khoản", FunctionID = 11 },
            // //    new Role() { ID = "ACCESS_PERMISSION", Name = "Phân quyền", FunctionID = 12 }

            // //    );
            // context.Races.AddOrUpdate(x => x.ID,
            //     new Race() { ID = 1, Name = "Kinh" },
            //     new Race() { ID = 2, Name = "Thái" }
            // );

            // context.Religions.AddOrUpdate(x => x.ID,
            //     new Religion() { ID = 1, Name = "Không" },
            //     new Religion() { ID = 2, Name = "Thiên chúa giáo" }
            // );

            // context.AcademicYears.AddOrUpdate(x => x.ID,
            //     new AcademicYear() { ID = 1, Name = "K53" },
            //     new AcademicYear() { ID = 2, Name = "K54" },
            //     new AcademicYear() { ID = 3, Name = "K55" },
            //     new AcademicYear() { ID = 4, Name = "K56" },
            //     new AcademicYear() { ID = 5, Name = "K57" },
            //     new AcademicYear() { ID = 6, Name = "K58" },
            //     new AcademicYear() { ID = 7, Name = "K59" },
            //     new AcademicYear() { ID = 8, Name = "K60" }

            // );

            // context.Units.AddOrUpdate(x => x.ID, new Unit()
            // {
            //     ID = 1,
            //     Name = "Đoàn trường",
            //     Type = 0,
            //     UnitCode = "ĐT"
            // });

            context.Positions.AddOrUpdate(x => x.ID,
                new Position() { ID = 1, Name = "Bí thư LCĐ", Level = 1, Type = 1, UnitType= 1, Amount=1 },
                new Position() { ID = 2, Name = "Phó bí thư LCĐ", Level = 2, Type = 1, UnitType = 1, Amount = 2 },
                new Position() { ID = 3, Name = "Uỷ viên Ban thường trực LCĐ", Level = 3, Type = 1, UnitType=1 ,Amount=10 },
                new Position() { ID = 4, Name = "Uỷ viên Ban chấp hành LCĐ", Level = 4, Type = 1, UnitType=1, Amount = 10 },
                new Position() { ID = 5, Name = "Lớp trưởng", Level = 1, Type = 0,  UnitType=2, Amount=1},
                new Position() { ID = 6, Name = "Lớp phó", Level = 2, Type = 0, UnitType = 2, Amount = 3 },
                new Position() { ID = 7, Name = "Bí thư Chi đoàn", Level = 1, Type = 1, UnitType = 2, Amount = 1 },
                new Position() { ID = 8, Name = "Phó bí thư Chi đoàn", Level = 2, Type = 1,  UnitType=2, Amount=3},
                new Position() { ID = 9, Name = "Uỷ viên BCH Chi đoàn", Level = 3, Type = 1, UnitType = 2, Amount = 10 },
                 new Position() { ID = 10, Name = "Bí thư Đoàn trường", Level = 1, Type = 1, UnitType= 0, Amount=1 },
                new Position() { ID = 11, Name = "Phó bí thư Đoàn trường", Level = 2, Type = 1, UnitType = 0, Amount = 4 },
                new Position() { ID = 12, Name = "Uỷ viên Ban thường trực Đoàn trường", Level = 3, Type = 1, UnitType = 0, Amount = 50 },
                new Position() { ID = 13, Name = "Uỷ viên Ban chấp hành Đoàn trường", Level = 4, Type = 1, UnitType = 0, Amount = 50 }
               
                );

            //context.AwardLevels.AddOrUpdate(x => x.ID,
            //     new AwardLevel() { ID = 1, Name = "Khen thưởng cấp trường" },
            //     new AwardLevel() { ID = 2, Name = "Khen thưởng cấp Liên chi đoàn" }
            // );

            // context.Awards.AddOrUpdate(x => x.ID,
            //     new Award() { ID = 1, Name = "Sinh viên xuất sắc", Type = 3, AwardLevelID = 1 },
            //     new Award() { ID = 4, Name = "Sao tháng giêng", Type = 3, AwardLevelID = 1 },
            //     new Award() { ID = 2, Name = "Chi đoàn tiên tiến", Type = 2, AwardLevelID = 2 },
            //     new Award() { ID = 3, Name = "Liên chi đoàn năng động", Type = 1, AwardLevelID = 1 }
            // );

            // context.SchoolYears.AddOrUpdate(x => x.ID,
            //     new SchoolYear() { ID = 1, Name = "2017-2018", StartDate = new DateTime(2017, 9, 4), EndDate = new DateTime(2018, 7, 1) }
            // );
        }
    }
}
