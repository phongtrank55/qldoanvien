namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMonthlyReportExpireDate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Configs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Plugin = c.String(nullable: false),
                        Key = c.String(nullable: false),
                        Value = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MonthlyReportExpireDates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Month = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                        DueDate = c.DateTime(nullable: false),
                        CutOffDate = c.DateTime(),
                        IsEnabledCutOffDate = c.Boolean(nullable: false),
                        DeducatePoint = c.Single(nullable: false),
                        IsEnabledDecuteEveryExpireDay = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.MonthlyReports", "OtherPoint", c => c.Single(nullable: false));
            AddColumn("dbo.MonthlyReports", "Comment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MonthlyReports", "Comment");
            DropColumn("dbo.MonthlyReports", "OtherPoint");
            DropTable("dbo.MonthlyReportExpireDates");
            DropTable("dbo.Configs");
        }
    }
}
