namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditNew : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.News", "ModifiedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.News", "AttachmentFile", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.News", "AttachmentFile");
            DropColumn("dbo.News", "ModifiedDate");
        }
    }
}
