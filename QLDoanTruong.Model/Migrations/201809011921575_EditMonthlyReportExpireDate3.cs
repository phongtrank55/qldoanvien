namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMonthlyReportExpireDate3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MonthlyReports", "IsComposing", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MonthlyReports", "IsComposing");
        }
    }
}
