namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditAchievement2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Achievements", "DateReceive", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Achievements", "DateReceive");
        }
    }
}
