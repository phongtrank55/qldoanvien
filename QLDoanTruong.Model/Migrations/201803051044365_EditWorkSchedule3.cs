namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditWorkSchedule3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkSchedules", "StartDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WorkSchedules", "StartDate");
        }
    }
}
