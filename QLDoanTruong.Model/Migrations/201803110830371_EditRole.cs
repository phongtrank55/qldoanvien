namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditRole : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Menus", "GroupUserID", "dbo.GroupUsers");
            DropIndex("dbo.Menus", new[] { "GroupUserID" });
            CreateTable(
                "dbo.Functions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Link = c.String(),
                        Image = c.String(),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.YouthUnionClasses", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.Roles", "FunctionID", c => c.Int());
            CreateIndex("dbo.Roles", "FunctionID");
            AddForeignKey("dbo.Roles", "FunctionID", "dbo.Functions", "ID");
            DropTable("dbo.Menus");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Menus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Link = c.String(),
                        Image = c.String(),
                        Order = c.Int(nullable: false),
                        GroupUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            DropForeignKey("dbo.Roles", "FunctionID", "dbo.Functions");
            DropIndex("dbo.Roles", new[] { "FunctionID" });
            DropColumn("dbo.Roles", "FunctionID");
            DropColumn("dbo.YouthUnionClasses", "Type");
            DropTable("dbo.Functions");
            CreateIndex("dbo.Menus", "GroupUserID");
            AddForeignKey("dbo.Menus", "GroupUserID", "dbo.GroupUsers", "ID", cascadeDelete: true);
        }
    }
}
