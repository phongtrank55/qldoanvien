namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMember6 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.YouthUnionMembers", "PositionID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.YouthUnionMembers", "PositionID", c => c.Int());
        }
    }
}
