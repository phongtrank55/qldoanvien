namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMember4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.YouthUnionMembers", "RelativeAddress", c => c.String());
            DropColumn("dbo.YouthUnionMembers", "RelativeAddresss");
        }
        
        public override void Down()
        {
            AddColumn("dbo.YouthUnionMembers", "RelativeAddresss", c => c.String());
            DropColumn("dbo.YouthUnionMembers", "RelativeAddress");
        }
    }
}
