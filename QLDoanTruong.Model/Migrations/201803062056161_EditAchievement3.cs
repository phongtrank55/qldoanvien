namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditAchievement3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Achievements", "YouthUnionID", "dbo.Units");
            DropForeignKey("dbo.Achievements", "YouthUnionID", "dbo.YouthUnionMembers");
            DropForeignKey("dbo.Achievements", "YouthUnionID", "dbo.YouthUnionClasses");
            DropIndex("dbo.Achievements", new[] { "YouthUnionID" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Achievements", "YouthUnionID");
            AddForeignKey("dbo.Achievements", "YouthUnionID", "dbo.YouthUnionClasses", "ID");
            AddForeignKey("dbo.Achievements", "YouthUnionID", "dbo.YouthUnionMembers", "ID");
            AddForeignKey("dbo.Achievements", "YouthUnionID", "dbo.Units", "ID");
        }
    }
}
