namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditPosition : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Positions", "Level", c => c.Int(nullable: false));
            AlterColumn("dbo.Activities", "ActivityCode", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Activities", "ActivityCode", c => c.String(nullable: false));
            DropColumn("dbo.Positions", "Level");
        }
    }
}
