namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditWorkSchedule2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkSchedules", "Week", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WorkSchedules", "Week");
        }
    }
}
