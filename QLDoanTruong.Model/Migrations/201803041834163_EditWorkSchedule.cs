namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditWorkSchedule : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.WorkSchedules", "YouthUnionMemberID", "dbo.YouthUnionMembers");
            DropIndex("dbo.WorkSchedules", new[] { "YouthUnionMemberID" });
            CreateTable(
                "dbo.WorkScheduleDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        WorkScheduleID = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Time = c.Time(nullable: false, precision: 7),
                        ChairMan = c.String(),
                        Content = c.String(storeType: "ntext"),
                        Participants = c.String(),
                        Location = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.WorkSchedules", t => t.WorkScheduleID, cascadeDelete: true)
                .Index(t => t.WorkScheduleID);
            
            AddColumn("dbo.WorkSchedules", "UnitID", c => c.Int(nullable: false));
            AddColumn("dbo.WorkSchedules", "Name", c => c.String());
            AddColumn("dbo.SchoolYears", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.SchoolYears", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Activities", "Content", c => c.String(storeType: "ntext"));
            CreateIndex("dbo.WorkSchedules", "UnitID");
            AddForeignKey("dbo.WorkSchedules", "UnitID", "dbo.Units", "ID", cascadeDelete: true);
            DropColumn("dbo.WorkSchedules", "Date");
            DropColumn("dbo.WorkSchedules", "Time");
            DropColumn("dbo.WorkSchedules", "YouthUnionMemberID");
            DropColumn("dbo.WorkSchedules", "Location");
            DropColumn("dbo.WorkSchedules", "Participants");
            DropColumn("dbo.SchoolYears", "Status");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SchoolYears", "Status", c => c.Int());
            AddColumn("dbo.WorkSchedules", "Participants", c => c.String());
            AddColumn("dbo.WorkSchedules", "Location", c => c.String());
            AddColumn("dbo.WorkSchedules", "YouthUnionMemberID", c => c.Int(nullable: false));
            AddColumn("dbo.WorkSchedules", "Time", c => c.Time(nullable: false, precision: 7));
            AddColumn("dbo.WorkSchedules", "Date", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.WorkScheduleDetails", "WorkScheduleID", "dbo.WorkSchedules");
            DropForeignKey("dbo.WorkSchedules", "UnitID", "dbo.Units");
            DropIndex("dbo.WorkScheduleDetails", new[] { "WorkScheduleID" });
            DropIndex("dbo.WorkSchedules", new[] { "UnitID" });
            AlterColumn("dbo.Activities", "Content", c => c.String(unicode: false, storeType: "text"));
            DropColumn("dbo.SchoolYears", "EndDate");
            DropColumn("dbo.SchoolYears", "StartDate");
            DropColumn("dbo.WorkSchedules", "Name");
            DropColumn("dbo.WorkSchedules", "UnitID");
            DropTable("dbo.WorkScheduleDetails");
            CreateIndex("dbo.WorkSchedules", "YouthUnionMemberID");
            AddForeignKey("dbo.WorkSchedules", "YouthUnionMemberID", "dbo.YouthUnionMembers", "ID", cascadeDelete: true);
        }
    }
}
