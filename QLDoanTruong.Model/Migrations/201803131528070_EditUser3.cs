namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditUser3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.YouthUnionMembers", "ID", "dbo.Users");
            AddForeignKey("dbo.YouthUnionMembers", "ID", "dbo.Users", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.YouthUnionMembers", "ID", "dbo.Users");
            AddForeignKey("dbo.YouthUnionMembers", "ID", "dbo.Users", "ID");
        }
    }
}
