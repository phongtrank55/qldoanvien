namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMember3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.YouthUnionMembers", "Facebook", c => c.String());
            AddColumn("dbo.YouthUnionMembers", "Website", c => c.String());
            AddColumn("dbo.YouthUnionMembers", "RelativeAddresss", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.YouthUnionMembers", "RelativeAddresss");
            DropColumn("dbo.YouthUnionMembers", "Website");
            DropColumn("dbo.YouthUnionMembers", "Facebook");
        }
    }
}
