namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditAward2 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.AwardTypes", newName: "AwardLevels");
            RenameColumn(table: "dbo.Awards", name: "AwardTypeID", newName: "AwardLevelID");
            RenameIndex(table: "dbo.Awards", name: "IX_AwardTypeID", newName: "IX_AwardLevelID");
            AddColumn("dbo.Achievements", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.Awards", "Type", c => c.Int(nullable: false));
            DropColumn("dbo.Achievements", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Achievements", "Description", c => c.String(storeType: "ntext"));
            DropColumn("dbo.Awards", "Type");
            DropColumn("dbo.Achievements", "Type");
            RenameIndex(table: "dbo.Awards", name: "IX_AwardLevelID", newName: "IX_AwardTypeID");
            RenameColumn(table: "dbo.Awards", name: "AwardLevelID", newName: "AwardTypeID");
            RenameTable(name: "dbo.AwardLevels", newName: "AwardTypes");
        }
    }
}
