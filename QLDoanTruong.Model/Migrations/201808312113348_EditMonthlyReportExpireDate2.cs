namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMonthlyReportExpireDate2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MonthlyReportExpireDates", "IsEnabledDeducateEveryExpireDay", c => c.Boolean(nullable: false));
            DropColumn("dbo.MonthlyReportExpireDates", "IsEnabledDecuteEveryExpireDay");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MonthlyReportExpireDates", "IsEnabledDecuteEveryExpireDay", c => c.Boolean(nullable: false));
            DropColumn("dbo.MonthlyReportExpireDates", "IsEnabledDeducateEveryExpireDay");
        }
    }
}
