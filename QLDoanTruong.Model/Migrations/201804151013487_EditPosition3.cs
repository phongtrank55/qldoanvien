namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditPosition3 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Leaders", newName: "LeaderUnits");
            DropForeignKey("dbo.YouthUnionMembers", "PositionID", "dbo.Positions");
            DropIndex("dbo.YouthUnionMembers", new[] { "PositionID" });
            CreateTable(
                "dbo.LeaderClasses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PositionID = c.Int(nullable: false),
                        ClassID = c.Int(nullable: false),
                        YouthUnionMemberID = c.Int(nullable: false),
                        SchoolYearID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Positions", t => t.PositionID, cascadeDelete: true)
                .ForeignKey("dbo.YouthUnionMembers", t => t.YouthUnionMemberID, cascadeDelete: true)
                .ForeignKey("dbo.SchoolYears", t => t.SchoolYearID, cascadeDelete: true)
                .ForeignKey("dbo.YouthUnionClasses", t => t.ClassID, cascadeDelete: true)
                .Index(t => t.PositionID)
                .Index(t => t.ClassID)
                .Index(t => t.YouthUnionMemberID)
                .Index(t => t.SchoolYearID);
            
            AddColumn("dbo.Positions", "UnitType", c => c.Int(nullable: false));
            AddColumn("dbo.Positions", "Amount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LeaderClasses", "ClassID", "dbo.YouthUnionClasses");
            DropForeignKey("dbo.LeaderClasses", "SchoolYearID", "dbo.SchoolYears");
            DropForeignKey("dbo.LeaderClasses", "YouthUnionMemberID", "dbo.YouthUnionMembers");
            DropForeignKey("dbo.LeaderClasses", "PositionID", "dbo.Positions");
            DropIndex("dbo.LeaderClasses", new[] { "SchoolYearID" });
            DropIndex("dbo.LeaderClasses", new[] { "YouthUnionMemberID" });
            DropIndex("dbo.LeaderClasses", new[] { "ClassID" });
            DropIndex("dbo.LeaderClasses", new[] { "PositionID" });
            DropColumn("dbo.Positions", "Amount");
            DropColumn("dbo.Positions", "UnitType");
            DropTable("dbo.LeaderClasses");
            CreateIndex("dbo.YouthUnionMembers", "PositionID");
            AddForeignKey("dbo.YouthUnionMembers", "PositionID", "dbo.Positions", "ID");
            RenameTable(name: "dbo.LeaderUnits", newName: "Leaders");
        }
    }
}
