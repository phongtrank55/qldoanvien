namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditAchievement5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Achievements", "SchoolYearID", "dbo.SchoolYears");
            DropIndex("dbo.Achievements", new[] { "SchoolYearID" });
            AlterColumn("dbo.Achievements", "SchoolYearID", c => c.Int());
            CreateIndex("dbo.Achievements", "SchoolYearID");
            AddForeignKey("dbo.Achievements", "SchoolYearID", "dbo.SchoolYears", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Achievements", "SchoolYearID", "dbo.SchoolYears");
            DropIndex("dbo.Achievements", new[] { "SchoolYearID" });
            AlterColumn("dbo.Achievements", "SchoolYearID", c => c.Int(nullable: false));
            CreateIndex("dbo.Achievements", "SchoolYearID");
            AddForeignKey("dbo.Achievements", "SchoolYearID", "dbo.SchoolYears", "ID", cascadeDelete: true);
        }
    }
}
