namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteMenu : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Menus", "ParentMenuID", "dbo.Menus");
            DropIndex("dbo.Menus", new[] { "ParentMenuID" });
            DropTable("dbo.Menus");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Menus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(),
                        Url = c.String(nullable: false),
                        ParentMenuID = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateIndex("dbo.Menus", "ParentMenuID");
            AddForeignKey("dbo.Menus", "ParentMenuID", "dbo.Menus", "ID");
        }
    }
}
