namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditUser2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "ModifiedDate", c => c.DateTime());
            DropColumn("dbo.Users", "Email");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Email", c => c.String());
            AlterColumn("dbo.Users", "ModifiedDate", c => c.DateTime(nullable: false));
        }
    }
}
