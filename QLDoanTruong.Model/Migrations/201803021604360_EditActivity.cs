namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditActivity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Menus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Link = c.String(),
                        Image = c.String(),
                        Order = c.Int(nullable: false),
                        GroupUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.GroupUsers", t => t.GroupUserID, cascadeDelete: true)
                .Index(t => t.GroupUserID);
            
            AddColumn("dbo.Activities", "Score", c => c.Single());
            AddColumn("dbo.Activities", "Content", c => c.String(unicode: false, storeType: "ntext"));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Menus", "GroupUserID", "dbo.GroupUsers");
            DropIndex("dbo.Menus", new[] { "GroupUserID" });
            DropColumn("dbo.Activities", "Content");
            DropColumn("dbo.Activities", "Score");
            DropTable("dbo.Menus");
        }
    }
}
