namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditAward : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.News", "AuthorID", "dbo.YouthUnionMembers");
            DropForeignKey("dbo.Activities", "YouthUnionMemberStaffID", "dbo.YouthUnionMembers");
            DropIndex("dbo.News", new[] { "AuthorID" });
            CreateTable(
                "dbo.Awards",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        AwardTypeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AwardTypes", t => t.AwardTypeID, cascadeDelete: true)
                .Index(t => t.AwardTypeID);
            
            CreateTable(
                "dbo.AwardTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.News", "Author", c => c.String(nullable: false));
            AddColumn("dbo.News", "IsOutStanding", c => c.Boolean(nullable: false));
            AddColumn("dbo.News", "Image", c => c.String());
            AddColumn("dbo.Achievements", "AwardID", c => c.Int(nullable: false));
            AlterColumn("dbo.Achievements", "YouthUnionID", c => c.Int(nullable: false));
            CreateIndex("dbo.Achievements", "AwardID");
            AddForeignKey("dbo.Achievements", "AwardID", "dbo.Awards", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Activities", "YouthUnionMemberStaffID", "dbo.YouthUnionMembers", "ID", cascadeDelete: true);
            DropColumn("dbo.News", "AuthorID");
            DropColumn("dbo.News", "AttachmentFile");
            DropColumn("dbo.Achievements", "Type");
            DropColumn("dbo.Achievements", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Achievements", "Name", c => c.String(nullable: false));
            AddColumn("dbo.Achievements", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.News", "AttachmentFile", c => c.String());
            AddColumn("dbo.News", "AuthorID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Activities", "YouthUnionMemberStaffID", "dbo.YouthUnionMembers");
            DropForeignKey("dbo.Achievements", "AwardID", "dbo.Awards");
            DropForeignKey("dbo.Awards", "AwardTypeID", "dbo.AwardTypes");
            DropIndex("dbo.Awards", new[] { "AwardTypeID" });
            DropIndex("dbo.Achievements", new[] { "AwardID" });
            AlterColumn("dbo.Achievements", "YouthUnionID", c => c.Int());
            DropColumn("dbo.Achievements", "AwardID");
            DropColumn("dbo.News", "Image");
            DropColumn("dbo.News", "IsOutStanding");
            DropColumn("dbo.News", "Author");
            DropTable("dbo.AwardTypes");
            DropTable("dbo.Awards");
            CreateIndex("dbo.News", "AuthorID");
            AddForeignKey("dbo.Activities", "YouthUnionMemberStaffID", "dbo.YouthUnionMembers", "ID");
            AddForeignKey("dbo.News", "AuthorID", "dbo.YouthUnionMembers", "ID", cascadeDelete: true);
        }
    }
}
