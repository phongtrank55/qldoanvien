namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditActivity1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Activities", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Activities", "ModifiedDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Activities", "ModifiedDate");
            DropColumn("dbo.Activities", "CreatedDate");
        }
    }
}
