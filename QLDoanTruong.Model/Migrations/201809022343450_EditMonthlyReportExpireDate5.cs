namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMonthlyReportExpireDate5 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.MonthlyReports", "Rank");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MonthlyReports", "Rank", c => c.Int());
        }
    }
}
