namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMonthlyReportExpireDate6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MonthlyReports", "Rank", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MonthlyReports", "Rank");
        }
    }
}
