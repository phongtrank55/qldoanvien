namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditAchievement4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Achievements", "SchoolYearID", c => c.Int(nullable: false));
            CreateIndex("dbo.Achievements", "SchoolYearID");
            AddForeignKey("dbo.Achievements", "SchoolYearID", "dbo.SchoolYears", "ID", cascadeDelete: true);
            DropColumn("dbo.Achievements", "DateReceive");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Achievements", "DateReceive", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.Achievements", "SchoolYearID", "dbo.SchoolYears");
            DropIndex("dbo.Achievements", new[] { "SchoolYearID" });
            DropColumn("dbo.Achievements", "SchoolYearID");
        }
    }
}
