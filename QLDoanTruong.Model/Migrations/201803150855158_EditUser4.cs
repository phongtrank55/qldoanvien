namespace QLDoanTruong.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditUser4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.YouthUnionMembers", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.YouthUnionMembers", "ModifiedDate", c => c.DateTime());
            AddColumn("dbo.Users", "TokenForgetPass", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "TokenForgetPass");
            DropColumn("dbo.YouthUnionMembers", "ModifiedDate");
            DropColumn("dbo.YouthUnionMembers", "CreatedDate");
        }
    }
}
