﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("YouthUnionMembers")]
    public class YouthUnionMember
    {
        public YouthUnionMember()
        {
            Firstname = " ";
            RaceID = 1; ReligionID = 1;
        }

        [Key]
        public int ID { get; set; }

        [Display(Name = "Họ")]
        [Required(ErrorMessage = "Họ không thể để trống!")]
        public string Lastname { get; set; }

        [Display(Name = "Tên")]
        
        public string Firstname { get; set; }
        [Display(Name = "Chi đoàn")]

        //[ForeignKey("YouthUnionClass")]
        public int? YouthUnionClassID { get; set; }

        [Display(Name = "Loại đoàn viên")]
        public int Type { get; set; }

        [Display(Name = "Mã sinh viên/cán bộ:")]
        [Required(ErrorMessage = "Mã SV/CB không thể trống")]
        public string YouthUnionMemberCode { get; set; }

        [Display(Name = "Ngày sinh")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Giới tính")]
        [Required]
        public int Gender { get; set; }
        [Display(Name = "Dân tộc")]
        public int RaceID { get; set; }
        [Display(Name = "Tôn giáo")]
        public int ReligionID { get; set; }

        [Display(Name = "Số điện thoại")]
        [Phone(ErrorMessage = "Không đúng định dạng SĐT")]
        public string Phone { get; set; }

        [Display(Name = "Địa chỉ Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Không đúng định dạng Email!")]
        public string Email { get; set; }
        [Display(Name = "Quê quán")]
        public string HomeTown { get; set; }
        [Display(Name = "Địa chỉ hiện tại")]
        public string CurrentAddress { get; set; }
        [Display(Name = "Địa chỉ thường trú")]
        public string PermanentAddress { get; set; }
        [Display(Name = "Họ tên người thân")]
        public string RelativeName { get; set; }

        [Display(Name = "SĐT người thân")]
        [Phone(ErrorMessage = "Không đúng định dạng SĐT")]
        public string RelativePhone { get; set; }
        [Display(Name = "Ảnh")]
        public string Photo { get; set; }
        [Display(Name = "Số thẻ đoàn")]
        public string YouthUnionCardNo { get; set; }
        [Display(Name = "Ngày vào đoàn")]
        public string DateIntoYouthUnion { get; set; }
        [Display(Name = "Ngày vào đảng")]
        public string DateIntoParty { get; set; }
        [Display(Name = "Số CMND")]
        public string IDCardNo { get; set; }
        [Display(Name = "Ngày cấp")]
        public string DateOfIssue { get; set; }
        [Display(Name = "Nơi cấp")]
        public string PlaceOfIssue { get; set; }
        [Display(Name = "Facebook")]
        public string Facebook { get; set; }
        [Display(Name = "Website")]
        
        public string Website { get; set; }

        [Display(Name = "Địa chỉ người thân")]
        public string RelativeAddress { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy")]
        public System.DateTime CreatedDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy")]
        public System.DateTime? ModifiedDate { get; set; }

        [ForeignKey("YouthUnionClassID")]
        public virtual YouthUnionClass YouthUnionClass { get; set; }
        
        [ForeignKey("RaceID")]
        public virtual Race Race { get; set; }

        [ForeignKey("ReligionID")]
        public virtual Religion Religion { get; set; }

        [ForeignKey("ID")]
        public virtual User User { get; set; }
        public virtual ICollection<LeaderClass> LeaderClasses { get; set; }
        public virtual ICollection<LeaderUnit> LeaderUnits { get; set; }
    }
}
