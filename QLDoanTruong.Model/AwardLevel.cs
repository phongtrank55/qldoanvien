﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("AwardLevels")]
    public class AwardLevel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        
        [Display(Name = "Cấp khen thưởng")]
        [Required(ErrorMessage = "Tên cấp khen thưởng không thể để trống")]
        public string Name { get; set; }

        public virtual ICollection<Award> Awards { get; set; }
    }
}
