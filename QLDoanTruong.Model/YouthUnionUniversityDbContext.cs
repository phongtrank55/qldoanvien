﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace QLDoanTruong.Model
{
    public class YouthUnionUniversityDbContext : DbContext
    {
        public YouthUnionUniversityDbContext()
            : base("name=YouthUnionConnection")
        {

        }
        public virtual DbSet<AcademicYear> AcademicYears { get; set; }
        public virtual DbSet<Achievement> Achievements { get; set; }
        public virtual DbSet<Activity> Activities { get; set; }
        public virtual DbSet<Award> Awards { get; set; }
        public virtual DbSet<AwardLevel> AwardLevels { get; set; }
        public virtual DbSet<Config> Configs { get; set; }
        public virtual DbSet<Credential> Credentials { get; set; }
        public virtual DbSet<Function> Functions { get; set; }
        
        public virtual DbSet<GroupUser> GroupUsers { get; set; }
        public virtual DbSet<LeaderClass> LeaderClasses { get; set; }
        public virtual DbSet<LeaderUnit> LeaderUnits { get; set; }
       
        public virtual DbSet<MonthlyReport> MonthlyReports { get; set; }
        public virtual DbSet<MonthlyReportExpireDate> MonthlyReportExpireDates { get; set; }
        public virtual DbSet<MonthlyReportDetail> MonthlyReportDetails { get; set; }
        public virtual DbSet<New> News { get; set; }
        
        public virtual DbSet<Race> Races { get; set; }
        public virtual DbSet<TempMonthlyReportDetail> TempMonthlyReportDetails { get; set; }
        public virtual DbSet<Religion> Religions { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<SchoolYear> SchoolYears { get; set; }
        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        
        public virtual DbSet<User> Users { get; set; }
        
        public virtual DbSet<WorkSchedule> WorkSchedules { get; set; }
        public virtual DbSet<WorkScheduleDetail> WorkScheduleDetails { get; set; }
        public virtual DbSet<YouthUnionClass> YouthUnionClasses { get; set; }
        public virtual DbSet<YouthUnionMember> YouthUnionMembers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //lieen keest 1-1
            modelBuilder.Entity<YouthUnionMember>().HasRequired(m => m.User).WithRequiredDependent(m => m.YouthUnionMember).WillCascadeOnDelete(true);
            //bo casecade
          //  modelBuilder.Entity<Activity>().HasRequired(a => a.YouthUnionMember).WithMany().HasForeignKey(t => t.YouthUnionMemberStaffID).WillCascadeOnDelete(false);
            //modelBuilder.Entity<YouthUnionMember>().HasMany(c => c.Activities).WithRequired(m => m.YouthUnionMember).HasForeignKey<int>(m => m.YouthUnionMemberStaffID).WillCascadeOnDelete(false);
            
            //1-n
            modelBuilder.Entity<Unit>().HasMany(u => u.MonthlyReports).WithRequired(r => r.Unit).HasForeignKey<int>(s => s.FacultyID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Unit>().HasMany(u => u.YouthUnionClasses).WithRequired(r => r.Unit).HasForeignKey<int>(s => s.FacultyID);
           // modelBuilder.Entity<YouthUnionMember>().HasMany(c => c.Activity).WithRequired(m => m.YouthUnionMember).HasForeignKey<int>(m => m.YouthUnionMemberStaffID);
        
            //modelBuilder.Entity<YouthUnionClass>().HasMany(c => c.YouthUnionMember).WithRequired(m => m.YouthUnionClass).HasForeignKey<int?>(m => m.YouthUnionClassID);
            modelBuilder.Entity<YouthUnionClass>().HasMany(c => c.YouthUnionMembers).WithOptional(m => m.YouthUnionClass);
            modelBuilder.Entity<MonthlyReport>().HasMany(c => c.MonthlyReportDetails).WithRequired(m => m.MonthlyReport).HasForeignKey<int>(m => m.MonthReportID);
            modelBuilder.Entity<Activity>().HasMany(c => c.MonthlyReportDetailes).WithRequired(m => m.Activity).HasForeignKey<int>(m => m.ActivityID);
            

            modelBuilder.Entity<Unit>().HasMany(u => u.Activities).WithRequired(m => m.Unit).HasForeignKey<int>(m => m.UnitID);
            modelBuilder.Entity<YouthUnionMember>().HasMany(u => u.LeaderUnits).WithRequired(m => m.YouthUnionMember).HasForeignKey<int>(m => m.YouthUnionMemberID).WillCascadeOnDelete(false);
            modelBuilder.Entity<YouthUnionMember>().HasMany(u => u.LeaderClasses).WithRequired(m => m.YouthUnionMember).HasForeignKey<int>(m => m.YouthUnionMemberID).WillCascadeOnDelete(false);
            //modelBuilder.Entity<Unit>().HasMany(u => u.Leader).WithRequired(m => m.Unit).HasForeignKey<int>(m => m.UnitID);
            //modelBuilder.Entity<UnionPosition>().HasMany(u => u.Leader).WithRequired(m => m.UnionPosition).HasForeignKey<int>(m => m.PositionID);
            modelBuilder.Entity<Function>().HasMany(u => u.Roles).WithOptional(m => m.Function);

            modelBuilder.Entity<WorkSchedule>().HasMany(u => u.WorkScheduleDetails).WithRequired(m => m.WorkSchedule).HasForeignKey<int>(m => m.WorkScheduleID);

            
            base.OnModelCreating(modelBuilder);
        }



    }
}
