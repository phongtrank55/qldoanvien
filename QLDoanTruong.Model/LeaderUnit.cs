using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("LeaderUnits")]
    public class LeaderUnit
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int PositionID {get; set; }
        public int UnitID  {get; set; }
        public int YouthUnionMemberID { get; set; }

        [ForeignKey("PositionID")]
        public virtual Position Position  {get; set; }

        [ForeignKey("UnitID")]
        public virtual Unit Unit { get; set; }
        [ForeignKey("YouthUnionMemberID")]
        public virtual YouthUnionMember YouthUnionMember { get; set; }
    }
}
