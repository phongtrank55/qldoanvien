﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("Units")]
    public class Unit
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage = "Mã LCĐ không được để trống")]
        [Display(Name = "Mã LCĐ")]
        public string UnitCode { get; set; }

        [Required(ErrorMessage = "Tên LCĐ không được để trống")]
        [Display(Name = "Tên LCĐ")]
        public string Name { get; set; }
        [Display(Name = "Địa chỉ Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Không đúng định dạng Email!")]
       
        public string Email { get; set; }

        public int Type{get; set;}
        public virtual ICollection<MonthlyReport> MonthlyReports { get; set; }

        public virtual ICollection<YouthUnionClass> YouthUnionClasses { get; set; }
        public virtual ICollection<Activity> Activities { get; set; }
        public virtual ICollection<LeaderUnit> LeaderUnits { get; set; }
        public virtual ICollection<WorkSchedule> WorrkSchedules { get; set; }
    }
}