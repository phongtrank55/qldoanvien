﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("SchoolYears")]
    public class SchoolYear
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        public System.DateTime StartDate { get; set; }

        public System.DateTime EndDate { get; set; }
        
        public virtual ICollection<LeaderClass> LeaderClasses { get; set; }
        public virtual ICollection<WorkSchedule> WorkSchedules{get; set;}
        public virtual ICollection<Achievement> Achievements { get; set;}
    }
}
