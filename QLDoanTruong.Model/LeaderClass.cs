using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("LeaderClasses")]
    public class LeaderClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int PositionID {get; set; }
        public int ClassID  {get; set; }
        public int YouthUnionMemberID { get; set; }
        public int SchoolYearID {get; set;}

        [ForeignKey("PositionID")]
        public virtual Position Position  {get; set; }

        [ForeignKey("ClassID")]
        public virtual YouthUnionClass YouthUnionClass { get; set; }

        [ForeignKey("SchoolYearID")]
        public virtual SchoolYear SchoolYear { get; set; }

        [ForeignKey("YouthUnionMemberID")]
        public virtual YouthUnionMember YouthUnionMember { get; set; }
    }
}
