﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("News")]
    public class New
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID {get; set; }
       
        [Display(Name = "Tiêu đề")]
        [Required (ErrorMessage="Tiêu đề không thể để trống")]
        public string Title {get; set; }

        [Required(ErrorMessage="Nội dung không được để trống")]
        [Column(TypeName="ntext")]
        [Display(Name="Nội dung")]
        public string Content {get; set; }
        public System.DateTime CreatedDate { get; set; }

        [Display(Name = "Cập nhật")]
        [DisplayFormat(DataFormatString="{0:H:mm dd/MM/yyyy}")]
        public System.DateTime ModifiedDate { get; set; }

        [Display(Name = "Tác giả")]
        [Required(ErrorMessage="Tác giả không thể để trống")]
        public string Author { get; set; }
        public bool IsOutStanding { get; set; }
        public string Image { get; set; }
        //[Required]
        //public int AuthorID {get; set; }
        //[Display(FunctionName="File đính kèm")]
        //public string AttachmentFile { get; set; }

        //[ForeignKey("AuthorID")]
        //public virtual YouthUnionMember YouthUnionMember {get; set;}
    }
}