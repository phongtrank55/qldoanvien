﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("MonthlyReportDetails")]
    public class MonthlyReportDetail
    {
        public MonthlyReportDetail()
        {
            Submited = false;
            PointSubmit = 0;
        }
        [Key]
        [Column(Order = 1)]
        public int MonthReportID { get; set; }
        [Key]
        [Column(Order = 2)]
        public int ActivityID { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Ngày hoạt động")]
        public System.DateTime DateActive { get; set; }

        [Display(Name = "Điểm tự chấm")]
        public float? PointSelf { get; set; }

        [Display(Name = "Điểm xác nhận")]
        public float? PointSubmit { get; set; }

        [Display(Name = "Xác nhận")]
        public bool Submited { get; set; }

        [Display(Name = "Minh chứng")]
        public string FileUploadProof { get; set; }
        
        [Display(Name = "Minh chứng")]
        public string OutsideLinkProof { get; set; }

                [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]

        [Display(Name = "Ngày cập nhật")]
        public System.DateTime UpdatedDate { get; set; }

        [ForeignKey("MonthReportID")]
        public virtual MonthlyReport MonthlyReport { get; set; }

        [ForeignKey("ActivityID")]
        public virtual Activity Activity { get; set; }

    }
}
