﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("WorkScheduleDetails")]
    public class WorkScheduleDetail
    {
        public WorkScheduleDetail()
        {
            IsDayLong = false;
            IsEveryDay = false;
            IsImportant = false;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int WorkScheduleID { get; set; }

        [Display(Name = "Ngày")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public System.DateTime Date { get; set; }

        [Display(Name = "Thời gian")]
        [DisplayFormat(DataFormatString = "{0:H:mm}")]
        public System.TimeSpan Time { get; set; }

        [Display(Name = "Chủ trì")]
        public string ChairMan { get; set; }

        [Display(Name = "Nội dung")]
        [Column(TypeName = "ntext")]
        public string Content { get; set; }

        [Display(Name = "Thành phần tham dự")]
        public string Participants { get; set; }

        [Display(Name = "Địa điểm")]
        public string Location { get; set; }
        public string DayOfWeek { get; set; }
        public bool IsDayLong { get; set; }
        public bool IsEveryDay { get; set; }
        public bool IsImportant { get; set; }

        [ForeignKey("WorkScheduleID")]
        public virtual WorkSchedule WorkSchedule { get; set; }
    }
}
