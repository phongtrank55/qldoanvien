using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("Credentials")]
    public class Credential
    {
        [Key]
        [Column(Order=1)]
        public int GroupUserID { get; set; }
        
        [Key]
        [Column(Order=2)]
        [Required]
        public string RoleID { get; set; }

        [ForeignKey("GroupUserID")]
        public virtual GroupUser GroupUser{get; set;}

        [ForeignKey("RoleID")]
        public virtual Role Role {get; set;}
    }
}
