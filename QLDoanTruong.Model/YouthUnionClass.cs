﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("YouthUnionClasses")]
    public class YouthUnionClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage="Mã lớp không được trống")]
        [Display(Name="Mã lớp")]
        public string YouthUnionClassCode { get; set; }
        
        [Required(ErrorMessage = "Tên lớp không được trống")]
        [Display(Name = "Tên lớp")]
        public string Name { get; set; }
        public int? AcademicYearID { get; set; }

        public int FacultyID { get; set; }

        //[Display(Name="Bí thư")]
        //public int? SecretaryID { get; set; }

        public int Type { get; set; }

        //[ForeignKey("SecretaryID")]
        //public virtual YouthUnionMember Secretary { get; set; }
        public virtual ICollection<LeaderClass> LeaderClasses { get; set; }
        public virtual ICollection<YouthUnionMember> YouthUnionMembers { get; set; }

        [ForeignKey("FacultyID")]
        public virtual Unit Unit { get; set; }
        [ForeignKey("AcademicYearID")]
        public virtual AcademicYear AcademicYear { get; set; }
    }
}