﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("Activities")]
    public class Activity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Display(Name="Mã hoạt động")]
        //[Required(ErrorMessage="Mã hoạt động không thể để trống")]
        public string ActivityCode {get; set; }

        [Display(Name="Tên hoạt động")]
        [Required(ErrorMessage="Tên hoạt động không thể để trống")]
        public string Name {get; set; }
        
        [Required(ErrorMessage="Đơn vị không thể để trống")]
        [Display(Name="Đơn vị")]
        public int UnitID {get; set; }

        [Display(Name="Ngày bắt đầu")]
        [Required(ErrorMessage = "Ngày bắt đầu không thể để trống")]
        public System.DateTime StartDate {get; set; }

        [Display(Name = "Ngày kết thúc")]
        [Required(ErrorMessage = "Ngày kết thúc không thể để trống")]
        public System.DateTime EndDate {get; set; }
        
        [Required(ErrorMessage = "Cán bộ không thể để trống")]
        [Display(Name = "Cán bộ phụ trách")]
        public string YouthUnionMemberStaff {get; set;}
        
        [Display(Name="Tài liệu")]
        public string Document {get; set; }
        [Display(Name="Điểm hoạt động")]
        public float? Score { get; set; }

        [Display(Name="Mô tả")]
        [Column(TypeName="ntext")]
        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("UnitID")]
        public virtual Unit Unit {get; set; }

        public virtual ICollection<MonthlyReportDetail> MonthlyReportDetailes {get; set;}
    }
}
