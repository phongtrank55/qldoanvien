﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("MonthlyReports")]
    public class MonthlyReport
    {
        public MonthlyReport()
        {
            Submited = false;
            OtherPoint = 0;
            IsComposing = true;

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [Display(Name = "Mã báo cáo")]
        public string MonthlyReportCode { get; set; }

        [Display(Name = "Liên chi đoàn")]
        public int FacultyID { get; set; }

        public int Month { get; set; }
        public int Year { get; set; }

        [Display(Name = "Xác nhận")]
        [DefaultValue(false)]
        public bool Submited { get; set; }
        public bool IsComposing { get; set; }

        [Display(Name = "Ngày gửi")]
        [DisplayFormat(DataFormatString = "{0:H:mm dd/MM/yyyy}")]
        public System.DateTime? SentDate { get; set; }
      
        [DefaultValue(0)]
        [Display(Name = "Điểm khác")]
        public float OtherPoint { get; set; }

        [Display(Name = "Lý do")]
        public string Comment { get; set; }


        [Display(Name = "Xếp hạng")]
        public int? Rank { get; set; }

        [DisplayFormat(DataFormatString = "{0:H:mm dd/MM/yyyy}")]

        public System.DateTime CreatedDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:H:mm dd/MM/yyyy}")]

        public System.DateTime ModifiedDate { get; set; }


        //[ForeignKey("FacultyID")]
        public virtual Unit Unit { get; set; }

        public virtual ICollection<MonthlyReportDetail> MonthlyReportDetails { get; set; }


    }
}
