﻿using System.ComponentModel.DataAnnotations;

namespace QLDoanTruong.Model.ViewModel
{
    public class ResetPasswordViewModel
    {
        [Display(Name = "Mật khẩu")]
        [Required(ErrorMessage = "Mật khẩu không được để trống")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Mật khẩu không được để trống")]
        [Display(Name = "Nhập lại mật khẩu")]
        [Compare("Password", ErrorMessage = "Mật khẩu không trùng nhau!")]
        public string ConfirmPassword { get; set; }

    }
}
