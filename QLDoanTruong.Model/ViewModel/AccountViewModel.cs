﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Model.ViewModel
{
    public class AccountViewModel:ChangePasswordViewModel
    {
        public int ID { get; set; }
        [Display(Name = "Trạng thái")]
        public bool Status { get; set; }

        public int? FacultyID { get; set; }
        
        [Display(Name = "Họ")]
        [Required(ErrorMessage = "Họ và tên không thể để trống!")]
        public string Lastname { get; set; }
        
        [Display(Name = "Tên")]
        [Required(ErrorMessage = "Họ và tên không thể để trống!")]
        public string Firstname { get; set; }
        [Required(ErrorMessage="Email Không thể để trống")]
        [Display(Name = "Địa chỉ Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Không đúng định dạng Email!")]
        public string Email { get; set; }
        
        [Display(Name = "Nhóm quyền")]
        public int GroupUserID { get; set; }
        [Display(Name = "Nhóm quyền")]
        public string GroupName { get; set; }
        [Display(Name = "Loại đoàn viên")]
        public int? Type { get; set; }
        [Display(Name = "Đơn vị")]
        public string UnitName { get; set; }
       
        [Display(Name = "Ngày sinh")]
        public string DateOfBirth { get; set; }
        [Display(Name = "Giới tính")]
        public int Gender { get; set; }
        [Required(ErrorMessage = "MãSV/CB không được để trống")]

        [Display(Name = "Mã SV/CB")]
        public string YouthUnionMemberCode { get; set; }
        public int? YouthUnionClassID { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy")]
        public System.DateTime CreatedDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy")]
        public System.DateTime ModifiedDate { get; set; }
        [Display(Name = "Dân tộc")]
        public int RaceID { get; set; }
        [Display(Name = "Tôn giáo")]
        public int ReligionID { get; set; }
        [Phone(ErrorMessage="Không đúng định dạng SĐT")]
        
        [Display(Name = "Số điện thoại")]
        public string Phone { get; set; }
        [Display(Name = "Quê quán")]
        public string HomeTown { get; set; }
        [Display(Name = "Địa chỉ hiện tại")]
        public string CurrentAddress { get; set; }
        [Display(Name = "Địa chỉ thường trú")]
        public string PermanentAddress { get; set; }
        [Display(Name = "Họ tên người thân")]
        public string RelativeName { get; set; }
        [Display(Name = "SĐT người thân")]
        public string RelativePhone { get; set; }
        [Display(Name = "Ảnh")]
        public string Photo { get; set; }
        [Display(Name = "Số thẻ đoàn")]
        public string YouthUnionCardNo { get; set; }
        [Display(Name = "Ngày vào đoàn")]
        public string DateIntoYouthUnion { get; set; }
        [Display(Name = "Ngày vào đảng")]
        public string DateIntoParty { get; set; }
        [Display(Name = "Số CMND")]
        public string IDCardNo { get; set; }
        [Display(Name = "Ngày cấp")]
        public string DateOfIssue { get; set; }
        [Display(Name = "Nơi cấp")]
        public string PlaceOfIssue { get; set; }
        [Display(Name = "Facebook")]
        public string Facebook { get; set; }
        [Display(Name = "Website")]

        public string Website { get; set; }

        [Display(Name = "Địa chỉ người thân")]
        public string RelativeAddress { get; set; }


    }
}
