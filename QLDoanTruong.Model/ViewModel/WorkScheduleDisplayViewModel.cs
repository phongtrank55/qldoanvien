﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QLDoanTruong.Model.ViewModel
{
    public class WorkScheduleDisplayViewModel
    {
        public class DetailView
        {
            public DetailView()
            {
                Participants = new List<string>();
                Chairman = new List<string>();
                IsImportant = false;
                IsDayLong = false;
            }
            public int ID { get; set; }
            public int WorkScheduleID { get; set; }

            [Display(Name = "Thời gian")]
            [DisplayFormat(DataFormatString = "{0:H:mm}")]
            public System.TimeSpan Time { get; set; }

            [Display(Name = "Chủ trì")]
            public List<string> Chairman { get; set; }

            [Display(Name = "Nội dung")]
            public string Content { get; set; }

            [Display(Name = "Thành phần tham dự")]
            public List<string> Participants { get; set; }

            [Display(Name = "Địa điểm")]
            public string Location { get; set; }
            public bool IsDayLong { get; set; }
            public bool IsImportant { get; set; }
        }
  

        public WorkScheduleDisplayViewModel()
        {
            Details = new List<DetailView>();
        }
        public string DayOfWeek { get; set; }
        [Display(Name = "Ngày")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
            
        public System.DateTime Date { get; set; }
        public int Count { get; set; }
        public List<DetailView> Details { get; set; } 
    }
}
