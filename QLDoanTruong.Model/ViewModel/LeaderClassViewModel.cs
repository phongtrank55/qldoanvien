﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Model.ViewModel
{
    public class LeaderClassViewModel
    {
        public string PositionName { get; set; }
        public IEnumerable<string> Member { get; set; }
        public IEnumerable<int> ID { get; set; }

        public int PositionType { get; set; }
    }
}
