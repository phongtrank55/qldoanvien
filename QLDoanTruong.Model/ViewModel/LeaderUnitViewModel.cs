﻿using System.ComponentModel.DataAnnotations;

namespace QLDoanTruong.Model.ViewModel
{
    public class LeaderUnitViewModel
    {
        public int ID { get; set; }
        public int MemberID { get; set; }
         [Display(Name="Họ")]
        public string Lastname { get; set; }

        [Display(Name = "Tên")]
        public string Firstname { get; set; }
        [Display(Name = "Chi đoàn")]
        public string YouthUnionClass {get; set; }
        public string YouthUnionMemberCode {get; set; }
        [Display(Name="Ngày sinh")]
        public string DateOfBirth {get; set; }
        public string UnitName { get; set; }
       
        [Display(Name="Số điện thoại")]
        public string Phone {get; set; }
        [Display(Name = "Địa chỉ Email")]
        public string Email { get; set; }
        public int PositionID { get; set; }
        [Display(Name = "Chức vụ")]
        public string Position { get; set; }
       
    }
}
