﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Model.ViewModel
{
   public class YouthUnionMemberViewModel
    {
       public int ID { get; set; }
         public string Lastname { get; set; }
 
        
        public string Firstname { get; set; }
        

        //[ForeignKey("YouthUnionClass")]
        public int? YouthUnionClassID { get; set; }
 
        public int? PositionID { get; set; }
        
        public int Type { get; set; }

        public string YouthUnionMemberCode { get; set; }

        
        public string DateOfBirth { get; set; }

        public string Gender { get; set; }
       
        public string Race { get; set; }

        public string Religion { get; set; }
        public string Phone { get; set; }

        public string Email { get; set; }
        public List<string> Position { get; set; }
        
    }
}
