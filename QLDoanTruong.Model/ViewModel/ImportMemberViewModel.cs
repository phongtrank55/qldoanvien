﻿
namespace QLDoanTruong.Model.ViewModel
{
  public  class ImportMemberViewModel
    {
      public string StudentCode { get; set; }
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public string DateOfBirth { get; set; }
      public string Class { get; set; }
      public string Faculty { get; set; }
      public string Gender { get; set; }
      public bool IsSuccess { get; set; }
      public string Message { get; set; }

    }
}
