﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Model.ViewModel
{
    public class YouthUnionFacultyViewModel
    {
      public int ID { get; set; }

        [Display(Name = "Mã LCĐ")]
        public string YouthUnionFaculityCode { get; set; }

        [Display(Name = "Tên Liên chi đoàn")]
        public string Name { get; set; }

        [Display(Name = "Số lượng CĐ")]
        public int ClassAmount { get; set; }
              
        [Display(Name="Số lượng ĐV")]
        public int UnionAmount { get; set; }

        [Display(Name = "Nam")]
        public int MaleAmount { get; set; }
        
        [Display(Name = "Nữ")]
        public int FemaleAmount { get; set; }
        
        [Display(Name = "Tôn giáo")]
        public int ReligionAmount { get; set; }
        
        [Display(Name = "Dân tộc thiểu số")]
        public int EthnicAmount { get; set; }

        [Display(Name = "Bí thư")]
        public string SecretaryName { get; set; }
        
        [Display(Name = "Phó bí thư")]
        public string DeputySecretaryName { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
