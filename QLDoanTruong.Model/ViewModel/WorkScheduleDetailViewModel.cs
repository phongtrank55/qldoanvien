﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Model.ViewModel
{

    public class WorkScheduleDetailViewModel
    {
        public WorkScheduleDetailViewModel()
        {
            Participants = new List<string>();
            Chairman = new List<string>();
            IsImportant = false;
            IsDayLong = false;
        }
        public int ID { get; set; }
        public int WorkScheduleID { get; set; }

        [Display(Name = "Ngày")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public System.DateTime Date { get; set; }

        [Display(Name = "Thời gian")]
        [DisplayFormat(DataFormatString = "{0:H:mm}")]
        public System.TimeSpan Time { get; set; }

        [Display(Name = "Chủ trì")]
        public List<string> Chairman { get; set; }
        public string ChairManOther { get; set; }

        [Display(Name = "Nội dung")]
        public string Content { get; set; }
        public string DayOfWeek { get; set; }

        [Display(Name = "Thành phần tham dự")]
        public List<string> Participants { get; set; }
        public string ParticipantOther { get; set; }

        [Display(Name = "Địa điểm")]
        public string Location { get; set; }
        public bool IsDayLong { get; set; }
        public bool IsImportant { get; set; }
    }
}
