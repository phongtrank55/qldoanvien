﻿
namespace QLDoanTruong.Model.ViewModel
{
  public  class ImportAchievementStudentViewModel
    {
      public string StudentCode { get; set; }
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public string Result { get; set; }

    }
}
