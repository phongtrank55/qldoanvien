﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Model.ViewModel
{
    public class CredentialViewModel
    {
        public CredentialViewModel()
        {
            Roles = new List<RoleItem>();
        }
        public class RoleItem
        {
            public RoleItem()
            {
                Values = new List<bool>();
            }
            public string RoleID { get; set; }
            public string RoleName { get; set; }
            //public int Order { get; set; }
            public List<bool> Values { get; set; }    
        }
        public int ID { get; set; }
        public string FunctionName { get; set; }

        public List<RoleItem> Roles { get; set; }
    }
}
