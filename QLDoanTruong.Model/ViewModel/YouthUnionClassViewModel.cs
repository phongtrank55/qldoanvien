﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Model.ViewModel
{
    public class YouthUnionClassViewModel
    {
      public int ID { get; set; }

        [Display(Name = "Mã lớp")]
        public string YouthUnionClassCode { get; set; }

        [Display(Name = "Tên lớp")]
        public string Name { get; set; }

        public int FacultyID { get; set; }
        [Display(Name = "Liên chi đoàn")]
        public string FacultyName { get; set; }
        
        [Display(Name="Số lượng")]
        public int UnionAmount { get; set; }

        [Display(Name = "Nam")]
        public int MaleAmount { get; set; }
        
        [Display(Name = "Nữ")]
        public int FemaleAmount { get; set; }
        
        [Display(Name = "Tôn giáo")]
        public int ReligionAmount { get; set; }
        
        [Display(Name = "Dân tộc thiểu số")]
        public int EthnicAmount { get; set; }

        [Display(Name = "Bí thư CĐ")]
        public string SecretaryName { get; set; }

        [Display(Name = "SĐT")]
        public string Phone { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }


        public int Type { get; set; }
    }
}
