﻿
using System.ComponentModel.DataAnnotations;
namespace QLDoanTruong.Model.ViewModel
{
    public class ChangePasswordViewModel:ResetPasswordViewModel
    {
        public int ID { get; set; }

        [Display(Name = "Tên đăng nhập")]
        [Required(ErrorMessage = "Tên đăng nhập không được để trống")]
        public string Username { get; set; }
        
    }
}
