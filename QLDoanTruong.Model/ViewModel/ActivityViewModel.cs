﻿using System.ComponentModel.DataAnnotations;

namespace QLDoanTruong.Model.ViewModel
{
    public class ActivityViewModel
    {

        public int ID { get; set; }

        [Display(Name = "Mã hoạt động")]
        public string ActivityCode { get; set; }

        [Display(Name = "Tên hoạt động")]
        public string Name { get; set; }

        [Display(Name = "Đơn vị")]
        public string UnitName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Ngày bắt đầu")]
        public System.DateTime StartDate { get; set; }

        [Display(Name = "Ngày kết thúc")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public System.DateTime EndDate { get; set; }

        [Display(Name = "Cán bộ phụ trách")]
        public string YouthUnionStaff { get; set; }
        [Display(Name = "Tình trạng")]
        public bool IsEnd { get; set; }

        public int UnitType { get; set; }
        [Display(Name = "Tài liệu")]
        public string Document { get; set; }

        [Display(Name = "Điểm hoạt động")]
        public float? Score { get; set; }

        [Display(Name = "Mô tả")]
        public string Content { get; set; }



    }
}
