﻿
namespace QLDoanTruong.Model.ViewModel
{
    public class SearchMemberViewModel
    {
        public long? FacultyID { get; set; }
        public long? ClassID { get; set; }
        public long? ReligionID { get; set; }
        public int? AcademicYearID { get; set; }
        public string MemberCode { get; set; }
        public string Fullname { get; set; }
        public string Phone { get; set; }
        public int? Type { get; set; }

    }
}
