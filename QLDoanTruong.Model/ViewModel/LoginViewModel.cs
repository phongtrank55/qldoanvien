﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDoanTruong.Model.ViewModel
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Không được để trống")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Không được để trống")]
        public string Password { get; set; }

        public bool IsRemember { get; set; }
    }
}
