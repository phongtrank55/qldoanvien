﻿using System.ComponentModel.DataAnnotations;

namespace QLDoanTruong.Model.ViewModel
{
    public class AchievementViewModel
    {
        public int ID { get; set; }

        public int YouthUnionID { get; set; }
        public string YouthUnionName { get; set; }        

        [Display(Name = "Tên giải thưởng")]
        public string AwardName { get; set; }

        [Display(Name = "Cấp giải thưởng")]
        public string AwardLevelName { get; set; }
        [Display(Name = "Mô tả giải thưởng")]
        public string AwardDescription { get; set; }
        public int Type { get; set; }
        [Display(Name = "Văn bản quyết định")]
        public string Document { get; set; }

        [Display(Name = "Năm học")]
        public string SchoolYearName { get; set; }
    }
}
