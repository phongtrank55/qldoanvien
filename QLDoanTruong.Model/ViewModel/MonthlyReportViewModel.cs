﻿
using System.ComponentModel.DataAnnotations;
namespace QLDoanTruong.Model.ViewModel
{
    public class MonthlyReportViewModel
    {
        public MonthlyReportViewModel()
        {

            DeducatePoint = 0;

            Submited = false;
            PointSubmitSum = 0;
            PointSelfSum = 0;
        }
        public int ID { get; set; }
        [Display(Name = "Mã báo cáo")]
        public string MonthlyReportCode { get; set; }

        public int FacultyID { get; set; }

        [Display(Name = "Liên chi đoàn")]
        public string FacultyName { get; set; }
        [Display(Name = "Xác nhận")]
        public bool Submited { get; set; }
        [Display(Name = "Điểm tự chấm")]
        public float? PointSelfSum { get; set; }
        [Display(Name = "Điểm xác nhận")]
        public float? PointSubmitSum { get; set; }
        [Display(Name = "Xếp loại")]
        public int? Rank { get; set; }
      
        public System.TimeSpan ExpireDayAmount { get; set; }

        public float DeducatePoint { get; set; }
        public float PointSum { get {
            if (this.PointSubmitSum == null) this.PointSubmitSum = 0;
            if (this.DeducatePoint == null) this.DeducatePoint = 0;

            return this.PointSubmitSum.Value - this.DeducatePoint; 
        } }

        public int Month { get; set; }
        public int Year { get; set; }
        
        [Display(Name = "Ngày gửi")]
        [DisplayFormat(DataFormatString = "{0:HH:mm dd/MM/yyyy}")]
        public System.DateTime? SentDate { get; set; }
        public float? ComparePreviousMonth { get; set; }

    }
}
