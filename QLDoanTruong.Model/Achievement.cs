﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("Achievements")]
    public class Achievement
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int YouthUnionID { get; set; }
        public int AwardID { get; set; }

        public int Type { get; set; }
        [Display(Name = "Văn bản quyết định")]
        public string Document { get; set; }
        public int? SchoolYearID { get; set; }

        [ForeignKey("AwardID")]
        public virtual Award Award { get; set; }

        [ForeignKey("SchoolYearID")]
        public virtual SchoolYear SchoolYear { get; set; }
    }
}
