﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("Users")]
    public class User
    {
        public User()
        {
            Status = true;
            PasswordChangeTimeNo = 0;
            
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage = "Tên đăng nhập không được trống!")]
        [Display(Name="Tên đăng nhập:")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Mật khẩu không được trống!")]
        [Display(Name = "Mật khẩu:")]
        public string Password { get; set; }
        
        public int PasswordChangeTimeNo {get; set;}
        public bool Status { get; set; }
        public int GroupUserID {get; set;}

        public string TokenForgetPass { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy")]
        public System.DateTime CreatedDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy")]
        public System.DateTime? ModifiedDate { get; set; }
     

        [ForeignKey("GroupUserID")]
        public virtual GroupUser GroupUser {get; set;}

        public virtual YouthUnionMember YouthUnionMember {get; set;}

    }
}
