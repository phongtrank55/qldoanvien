﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("Religions")]
    public class Religion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [Display(Name = "Dân tộc")]
        public string Name { get; set; }

        [Display(Name = "Phổ biến")]
        public bool IsPopular { get; set; }


        public virtual ICollection<YouthUnionMember> YouthUnionMembers {get; set;}
    }
}
