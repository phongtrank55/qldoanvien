﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("Roles")]
    public class Role
    {
        [Key]
        [Required]
        public string ID { get; set; }

        [Required]
        public string Name { get; set; }
    
        public int? FunctionID { get; set; }
        public int Order { get; set; }
        [ForeignKey("FunctionID")]
        public virtual Function Function { get; set; }
        public virtual ICollection<Credential> Credentials{get; set;}
    }
}
