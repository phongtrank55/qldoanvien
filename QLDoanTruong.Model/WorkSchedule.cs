﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLDoanTruong.Model
{
    [Table("WorkSchedules")]
    public class WorkSchedule
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int UnitID { get; set; }
        public int SchoolYearID {get; set; }
        [Display(Name="Lịch tuần")]
        public string Name { get; set; }

        public System.DateTime StartDate { get; set; }

        public int Week { get; set; }

        [Display(Name = "Ngày tạo")]
        [DisplayFormat(DataFormatString = "{0:HH:mm dd/MM/yyyy}")]
        public System.DateTime CreatedDate { get; set; }
        
        [Display(Name = "Cập nhật")]
        [DisplayFormat(DataFormatString = "{0:HH:mm dd/MM/yyyy}")]
        public System.DateTime ModifiedDate { get; set; }
        
        [ForeignKey("SchoolYearID")]
        public virtual SchoolYear SchoolYear {get; set; }

        [ForeignKey("UnitID")]
        public virtual Unit Unit { get; set; }
        public virtual ICollection<WorkScheduleDetail> WorkScheduleDetails { get; set; }
    }
}
